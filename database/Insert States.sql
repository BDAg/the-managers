show databases;

use pimanager;

show tables;

describe States;

delete from States where idState = 1;
select * from States;

INSERT INTO `States` (`idState`, `name`, `createdAt`, `updatedAt`) VALUES
(1, 'Acre', NOW(), NOW()),
(2, 'Alagoas', NOW(), NOW()),
(3, 'Amazonas', NOW(), NOW()),
(4, 'Amapá', NOW(), NOW()),
(5, 'Bahia', NOW(), NOW()),
(6, 'Ceará', NOW(), NOW()),
(7, 'Distrito Federal', NOW(), NOW()),
(8, 'Espírito Santo', NOW(), NOW()),
(9, 'Goiás', NOW(), NOW()),
(10, 'Maranhão', NOW(), NOW()),
(11, 'Minas Gerais', NOW(), NOW()),
(12, 'Mato Grosso do Sul', NOW(), NOW()),
(13, 'Mato Grosso', NOW(), NOW()),
(14, 'Pará', NOW(), NOW()),
(15, 'Paraíba', NOW(), NOW()),
(16, 'Pernambuco', NOW(), NOW()),
(17, 'Piauí', NOW(), NOW()),
(18, 'Paraná', NOW(), NOW()),
(19, 'Rio de Janeiro', NOW(), NOW()),
(20, 'Rio Grande do Norte', NOW(), NOW()),
(21, 'Rondônia', NOW(), NOW()),
(22, 'Roraima', NOW(), NOW()),
(23, 'Rio Grande do Sul', NOW(), NOW()),
(24, 'Santa Catarina', NOW(), NOW()),
(25, 'Sergipe', NOW(), NOW()),
(26, 'São Paulo', NOW(), NOW()),
(27, 'Tocantins', NOW(), NOW());