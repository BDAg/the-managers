const express = require("express");
const cors = require("cors");

const app = express();

const NotificationSequelize = require('./sequelize').Notification;

let server = require('http').createServer(app).listen(4555)
    ,io = require('socket.io').listen(server);

const studentRouter = require("./routes/StudentR");
const professorRouter = require('./routes/ProfessorR');
const photosRouter = require('./routes/PhotoR');
const postsRouter = require('./routes/PostR');
const projectRouter = require("./routes/ProjectR");
const technologyRouter = require('./routes/TechnologyR');
const utilRouter = require('./routes/UtilsR');
const notificationRouter = require('./routes/NotificationR');
const matrixRouter = require('./routes/MatrixR');
const adminRouter = require('./routes/AdminR');
const tutorialRouter = require('./routes/TutorialR');
const cronogramaRouter = require('./routes/CronogramaR');

const serveStatic = require('serve-static');
const path = require('path');
const fs = require('fs');

let notifications = new Array();
// notifications.push(21);

app.use(cors());
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true}));

const dir = path.join(__dirname, 'photos');
let diretorios = [
                  __dirname + '/photos/',
                  __dirname + '/photos/student/',
                  __dirname + '/photos/professor/',
                  __dirname + '/photos/post/'
                ];
diretorios.forEach(
  (value) => {
    if (!fs.existsSync(value)) {
      fs.mkdirSync(value);
    }
  }
);

app.use('/photos', serveStatic(dir));
app.use("/student", studentRouter);
app.use("/professor", professorRouter);
app.use("/photo", photosRouter);
app.use("/post", postsRouter);
app.use("/project", projectRouter);
app.use("/technology", technologyRouter);
app.use("/notification", notificationRouter);
app.use("/util", utilRouter);
app.use("/matrix", matrixRouter);
app.use("/admin", adminRouter);
app.use("/tutorial", tutorialRouter);
app.use("/cronograma", cronogramaRouter);

app.get(
    "/", (req, res) => {
        res.send("API OF THE MANAGERS TO PI MANAGEMENT WEBSITE")
    }
);

NotificationSequelize.addHook('afterCreate', 'notify', (atributes, options) => {
  notifications.push(new Number(atributes.dataValues.StudentId).valueOf());
});

io.on('connection', (cliente) => {
  cliente.on('subscriptNotification', (interval, idStudent) => {
    setInterval(() => {
      cliente.emit('notification', notifications.includes(idStudent) ? 1 : 0);
    }, interval);
  });
});

app.get('/notification', (req, res, next) => {
  let idStudent = req.body.idStudent;
  if (idStudent == undefined) {
    res.status(406).json({
      message: 'Invalid Params!'
    });
  } else {
    let index = notifications.indexOf(idStudent);
    if (index > -1) {
      notifications.splice(index, 1);
      res.status(200).json({
        message: 'View Notification!'
      });
    } else {
      res.status(200).json({
        message: 'Ok!'
      });
    }
  }
});

app.listen(process.env.PORT  || 3000, '0.0.0.0', () => {
    console.log('Listening')
});
