const { Administrator, Tutorial } = require("../../sequelize");
const jwt = require("jsonwebtoken");
const generateUrl = require("../helpers/generateUrl");
const config = require("../helpers/configs");

exports.Publish = function (req, res) {
    let data = {
        title: null,
        video: null,
        description: null,
        content: null,
        url: null,
        AdministratorId: null
    }

    let token = req.body.token;

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (token === undefined) {
        return res.status(406).json({
            "response": null,
            "error": "Invalid token!"
        });
    }
    else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idAdmin = tokenDecoded["idAdmin"];

        if (idAdmin) {
            data.AdministratorId = idAdmin;
            Administrator.findByPk(idAdmin).then(admin => {
                if (!admin) {
                    res.status(401).json({
                        "response": null,
                        "error": "Unauthorized"
                    });
                } else {
                    if (data.title === undefined || (data.content === undefined && data.video === undefined)) {
                        return res.status(406).json({
                            "response": null,
                            "error": "Invalid params!"
                        });
                    } else {
                        if (data.url === undefined) {
                            generateUrl(data.title, (url) => {
                                data.url = url
                            });
                        }

                        data.url = data.url + `-${Math.floor(100000 + Math.random() * 900000).toString()}`

                        for (let key in data) {
                            if (data[key] === null || data[key] === undefined) {
                                delete data[key];
                            }
                        }

                        Tutorial.create(data).then(result => {
                            res.status(201).json({
                                "response": {
                                    "message": "Created",
                                    "data": result.get()
                                },
                                "error": null
                            });
                        }).catch(err => {
                            res.status(500).json({
                                "response": null,
                                "error": err.toString()
                            });
                        });
                    }
                }
            })
        } else {
            return res.status(406).json({
                "response": null,
                "error": "Invalid token!"
            });
        }
    }
}

exports.Edit = function (req, res) {

}

exports.List = function (req, res) {
    Tutorial.findAll({
        order: [
            ['createdAt', 'DESC']
        ]
    }).then(tutorials => {
        res.status(200).json({
            "response": tutorials,
            "error": null
        });
    }).catch(err => {
        res.status(500).json({
            "response": null,
            "error": err.toString()
        });
    });
}

exports.getOne = function(req, res) {
    let url = req.params.url;

    Tutorial.findOne({
        where: {
            url: url
        },
        include: {
            model: Administrator,
            attributes: ["name"]
        }
    }).then(response => {
        if(!response){
            res.status(404).json({
                "response": null,
                "error": "Not found!"
            });
        } else {
            res.status(200).json({
                "response": response.get(),
                "error": null
            });
        }
    })
}