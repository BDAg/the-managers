const { Comentary } = require("../../sequelize");

const jwt = require("jsonwebtoken"),
    config = require('../helpers/configs');

exports.Comment = function (req, res) {

    let token = req.body.token;
    let data = {
        PostId: req.body.postId,
        comment: req.body.comment,
        StudentId: undefined,
        ProfessorId: undefined
    }

    if (token === undefined || data.PostId === undefined || data.comment === undefined) {
        if (token === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        }
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);

        if ('idProfessor' in tokenDecoded) {
            data.ProfessorId = tokenDecoded['idProfessor'];

        } else if ('idStudent' in tokenDecoded) {
            data.StudentId = tokenDecoded['idStudent'];
        }

        if (data.StudentId === undefined && data.ProfessorId === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        } else {
            for (let key in data) {
                if (data[key] === undefined) {
                    delete data[key]
                }
            }

            Comentary.create(data).then(comment => {
                res.status(201).json({
                    "response": {
                        "message": "Comment added",
                        "data": comment.get()
                    }
                });
            }).catch(err => {
                res.status(500).json({
                    "error": err,
                    "response": null
                });
            });
        }
    }

}

exports.DeleteComment = function (req, res) {

    let token = req.body.token;
    let data = {
        PostId: req.body.postId,
        StudentId: undefined,
        ProfessorId: undefined
    }

    if (token === undefined || data.PostId === undefined) {
        if (token === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        }
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);

        if ('idProfessor' in tokenDecoded) {
            data.ProfessorId = tokenDecoded['idProfessor'];

        } else if ('idStudent' in tokenDecoded) {
            data.StudentId = tokenDecoded['idStudent'];
        }

        if (data.StudentId === undefined && data.ProfessorId === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        } else {
            for (let key in data) {
                if (data[key] === undefined) {
                    delete data[key]
                }
            }

            Comentary.findOne({
                where: data
            }).then(comment => {
                if (!comment) {
                    res.status(404).json({
                        "error": "Not found",
                        "response": null
                    });
                } else {
                    comment.destroy().then(() => {
                        res.status(200).json({
                            "response": "Comment removed",
                            "error": null
                        });
                    }).catch(err => {
                        res.status(500).json({
                            "error": err,
                            "response": null
                        });
                    });
                }
            }).catch(err => {
                res.status(500).json({
                    "error": err,
                    "response": null
                });
            });
        }
    }
}

exports.EditComment = function (req, res) {
    let token = req.body.token;
    let newComment = req.body.comment;
    let IdComentary = req.body.comentaryId;
    let unauthorized = true;

    let data = {
        StudentId: undefined,
        ProfessorId: undefined
    }

    if (token === undefined || IdComentary === undefined || newComment === undefined) {
        res.status(406).json({
            "error": "Invalid params",
            "response": null
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);

        if ('idProfessor' in tokenDecoded) {
            data.ProfessorId = tokenDecoded['idProfessor'];

        } else if ('idStudent' in tokenDecoded) {
            data.StudentId = tokenDecoded['idStudent'];
        }

        if (data.StudentId === undefined && data.ProfessorId === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        } else {
            for (let key in data) {
                if (data[key] === undefined) {
                    delete data[key]
                }
            }

            Comentary.findByPk(IdComentary).then(comment => {

                for (let key in data) {
                    if (data[key] === comment.get()[key]) {
                        unauthorized = false;
                        break;
                    }
                }

                if (unauthorized) {
                    res.status(401).json({
                        "response": "Unauthorized",
                        "error": null
                    })
                } else {
                    comment.update({
                        comment: newComment
                    }).then(() => {
                        res.status(200).json({
                            "response": "Comment updated",
                            "error": null
                        })
                    }).catch(err => {
                        res.status(500).json({
                            "error": err.toString(),
                            "response": null
                        });
                    });
                }
            }).catch(err => {
                res.status(500).json({
                    "error": err.toString(),
                    "response": null
                });
            });
        }
    }

}