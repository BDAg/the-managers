const { Student, City, State, Photo, Technology, Project, Member, sequelize } = require("../../sequelize");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../helpers/configs");
const roles = require("../helpers/roles");
const Sequelize = require('sequelize');
const transporter = require('../helpers/transporter');

const redis = require("redis");

exports.Register = function (req, res, next) {
    let data = {
        name: null,
        birthday: null,
        semester: null,
        phone: null,
        email: null,
        password: null,
        CityId: null,
        Role: roles.student
    }

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (data.name === null || data.semester === null || data.email === null || data.password === null || data.password.length < 6 || data.CityId === null) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params!"
        });
    } else {
        for (let key in data) {
            if (data[key] === null) {
                delete data[key];
            }
        }

        bcrypt.hash(data.password, 10, (err, hash) => {
            if (err) {
                res.status(406).json({
                    "response": null,
                    "error": err
                })
            } else {
                data.password = hash;

                Student.create(data).then(result => {
                    let token = jwt.sign(
                        {
                            email: result.email,
                            idStudent: result.id,
                            name: result.name,
                            role: result.role
                        },
                        config.secret,
                        { expiresIn: "3h" }
                    );
                    res.status(201).json(
                        {
                            "response": "Student registered",
                            "data": result.get(),
                            "token": token
                        }
                    );
                }).catch(err => {
                    res.status(406).json(
                        {
                            "response": "Student not registered",
                            "error": err
                        }
                    )
                });
            }
        });
    }
}

exports.Login = function (req, res, next) {
    let email = req.body.emailOrUsername;
    let password = req.body.password;

    if (email === undefined || password === undefined || password.length < 6) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {
        Student.findOne({
            include: [
                { all: true },
                { model: City, include: [State] },
                { model: Technology },
                { model: Photo },
                { model: Project }
            ],
            where: {
                email: email,
            }
        }).then(result => {
            if (!result) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "Student not found"
                    }
                )
            }
            else {
                bcrypt.compare(password, result.get().password, function (err, authenticated) {
                    if (err) {
                        res.status(406).json(
                            {
                                "response": null,
                                "error": err
                            }
                        )
                    } else {

                        if (!authenticated) {
                            res.status(401).json({
                                'error': null,
                                'response': {
                                    'auth': false,
                                    'message': 'Not authenticated!'
                                }
                            });
                        } else {
                            let token = jwt.sign({
                                email: result.email,
                                idStudent: result.id,
                                name: result.name,
                                role: result.role
                            }, config.secret, { expiresIn: "120d" });

                            res.status(201).json({
                                "error": null,
                                "response": {
                                    "auth": true,
                                    "message": "Authenticated!",
                                    "token": token,
                                    "data": {
                                        id: result.id,
                                        name: result.name,
                                        email: result.email,
                                        role: result.role,
                                        birthday: result.birthday,
                                        semester: result.semester,
                                        phone: result.phone,
                                        matrix: result.Technologies,
                                        projects: result.Projects,
                                        city: result.City,
                                        photo: result.Photo
                                    }
                                }
                            })
                        }
                    }
                });
            }
        }).catch(err => {
            res.status(406).json(
                {
                    "response": null,
                    "error": "error to list user infos: " + err
                }
            )
        });
    }
}

exports.GetOneStundentByEmail = function (req, res) {
    let email = req.params.email;

    if (email === undefined) {
        res.status(406).json({
            "error": "Invalid params!",
            "response": null
        });
    } else {
        Student.findOne({
            where: {
                "email": email
            },
            include: [
                { all: true },
                { model: City, include: [State] },
                { model: Technology },
                { model: Photo },
                { model: Project }
            ]
        }).then(result => {
            if (!result) {
                res.status(404).json({
                    "error": "Student not found!",
                    "response": null
                });
            } else {
                res.status(201).json({
                    "error": null,
                    "response": {
                        "data": {
                            id: result.id,
                            name: result.name,
                            email: result.email,
                            birthday: result.birthday,
                            semester: result.semester,
                            phone: result.phone,
                            matrix: result.Technologies,
                            projects: result.Projects,
                            city: result.City,
                            photo: result.Photo
                        }
                    }
                });
            }
        }).catch(err => {
            res.status(500).json({
                "error": err,
                "response": null
            });
        });
    }
}



exports.GetAllStudents = function (req, res) {

    Student.findAll({
        attributes: ['name', 'semester', 'email'],
        include: [
            { model: City, attributes: ['name'], include: {
                model: State,
                attributes: ['name']
            } },
            { model: Photo, attributes: ['path'] },
            { model: Project, attributes: ["idProject"] , through: { model: Member, attributes: ['job', 'isMember']}},
            
        ]
    }).then(result => {
        if (!result) {
            res.status(404).json({
                "error": "Student not found!",
                "response": null
            });
        } else {
            res.status(201).json({
                "error": null,
                "response": {
                    "data": result
                }
            });
        }
    }).catch(err => {
        res.status(500).json({
            "error": err,
            "response": null
        });
    });
}

exports.ForgotPassword = function (req, res, next) {

    let email = req.body.email;

    if (email == undefined) {
        res.status(406).json(
            {
                "error": "Invalid params!",
                "response": null
            }
        )
    } else {

        Student.findOne({
            where: {
                email: email
            }
        }).then((value) => {
            if (!value) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "Student not found"
                    }
                );
            } else {

                let code = Math.floor(100000 + Math.random() * 900000);

                let conn = redis.createClient('redis://redis-11872.c74.us-east-1-4.ec2.cloud.redislabs.com:11872');
                conn.auth("Fatec2019#", (err, reply) => {
                    if (err) {
                        conn.end(true);
                        res.status(500).json(
                            {
                                "response": null,
                                "error": err
                            }
                        );
                    } else {

                        conn.setex(`student:${value.get().id}`, 1200, code, (err, reply) => {
                            // conn.set(`student:${value.get().id}`, code, 'EX', 1200, (err, reply) => {

                            conn.end(true);

                            if (err) {
                                res.status(406).json(
                                    {
                                        "response": null,
                                        "error": err
                                    }
                                )
                            } else {

                                conteudo_email = `Oi ${value.name},<br><br>
                                Esqueceu sua senha? Tudo bem, não nos esquecemos de você! Copie o código para redefinir sua senha.<br><br>
                                Código: ${code} <br><br>

                                <b>Lembrando que o código para alterar senha funcionará por apenas 10 minutos!.</b>
                                `;

                                var mailOptions = {
                                    from: 'octopusprojectoficial@gmail.com',
                                    to: email,
                                    subject: 'Recuperação de Conta',
                                    html: conteudo_email
                                };

                                transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                        res.status(500).json(
                                            {
                                                'response': null,
                                                'error': error
                                            }
                                        );
                                    } else {
                                        res.status(200).json(
                                            {
                                                'error': null,
                                                'response': {
                                                    'message': 'Success!',
                                                    'info': info
                                                }
                                            }
                                        );
                                    }
                                });

                            }

                        });
                    }
                });
            }
        })
            .catch((err) => {
                if (err) {
                    res.status(500).json(
                        {
                            "response": null,
                            "error": err
                        }
                    )
                }
            });

    }

}

exports.CheckCode = function (req, res, next) {

    let email = req.body.email;
    let code = req.body.code;

    if (email == undefined || code == undefined) {
        res.status(406).json(
            {
                "error": "Invalid params!",
                "response": null
            }
        )
    } else {

        Student.findOne({
            where: {
                email: email
            }
        }).then((value) => {
            if (!value) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "Student not found"
                    }
                );
            } else {

                let conn = redis.createClient('redis://redis-11872.c74.us-east-1-4.ec2.cloud.redislabs.com:11872');
                conn.auth("Fatec2019#", (err, reply) => {
                    if (err) {
                        conn.end(true);
                        res.status(500).json(
                            {
                                "response": null,
                                "error": err
                            }
                        );
                    } else {

                        conn.get(`student:${value.get().id}`, (err, reply) => {

                            conn.end(true);

                            if (err) {
                                res.status(500).json(
                                    {
                                        "response": null,
                                        "error": err
                                    }
                                );
                            } else {

                                if (!reply) {
                                    res.status(404).json(
                                        {
                                            "response": null,
                                            "error": "Not found code!"
                                        }
                                    );
                                } else {

                                    res.status(200).json(
                                        {
                                            "error": null,
                                            "response": {
                                                "authorize": reply == code.toString()
                                            }
                                        }
                                    )

                                }

                            }
                        });
                    }
                });
            }
        });

    }

}

exports.ChangePasswordWithCode = function (req, res, next) {

    let email = req.body.email;
    let code = req.body.code;
    let password = req.body.password;

    if (email == undefined || code == undefined || password == undefined || password.length < 6) {
        res.status(406).json(
            {
                "error": "Invalid params!",
                "response": null
            }
        )
    } else {

        Student.findOne({
            where: {
                email: email
            }
        }).then((value) => {
            if (!value) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "Student not found"
                    }
                );
            } else {

                let conn = redis.createClient('redis://redis-11872.c74.us-east-1-4.ec2.cloud.redislabs.com:11872');
                conn.auth("Fatec2019#", (err, reply) => {
                    if (err) {
                        conn.end(true);
                        res.status(500).json(
                            {
                                "response": null,
                                "error": err
                            }
                        );
                    } else {
                        conn.get(`student:${value.get().id}`, (err, reply) => {

                            conn.end(true);

                            if (err) {
                                res.status(500).json(
                                    {
                                        "response": null,
                                        "error": err
                                    }
                                );
                            } else {

                                if (!reply) {
                                    res.status(404).json(
                                        {
                                            "response": null,
                                            "error": "Not found code!"
                                        }
                                    );
                                } else {

                                    if (reply != code.toString()) {
                                        res.status(406).json(
                                            {
                                                "error": null,
                                                "response": {
                                                    "authorize": false
                                                },
                                            }
                                        );
                                    } else {
                                        bcrypt.hash(password, 10, (err, hash) => {
                                            if (err) {
                                                res.status(406).json({
                                                    "response": null,
                                                    "error": err
                                                })
                                            } else {
                                                Student.update(
                                                    {
                                                        password: hash
                                                    },
                                                    {
                                                        where: {
                                                            email: email
                                                        }
                                                    }
                                                ).then((value) => {
                                                    res.status(200).json(
                                                        {
                                                            "error": null,
                                                            "response": {
                                                                "message": "Updated",
                                                                "value": value
                                                            }
                                                        }
                                                    );
                                                }).catch((err) => {
                                                    res.status(200).json(
                                                        {
                                                            "response": null,
                                                            "error": err
                                                        }
                                                    );
                                                });
                                            }
                                        });
                                    }

                                }

                            }
                        })
                    }
                });
            }
        });

    }

}

exports.ChangePassword = function (req, res, next) {
    let password = req.body.password;
    let newPassword = req.body.newPassword;
    let token = req.body.token;

    if (token === undefined || newPassword === undefined || password === undefined || password.length < 6) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {
        tokenDecoded = jwt.verify(token, config.secret);

        let idStudent = tokenDecoded["idStudent"];

        Student.findByPk(idStudent).then(result => {
            if (!result) {
                res.status(404).json({
                    'response': null,
                    'error': 'Student not found'
                });
            } else {
                bcrypt.compare(password, result.password, function (err, equals) {
                    if (err) {
                        res.status(406).json({
                            'response': null,
                            'error': err
                        });
                    } else {
                        if (!equals) {
                            res.status(406).json({
                                'response': null,
                                'error': 'Invalid password!'
                            });
                        } else {
                            bcrypt.hash(newPassword, 10, function (err, newPasswordHash) {
                                if (err) {
                                    res.status(406).json({
                                        'response': null,
                                        'error': err
                                    });
                                } else {
                                    result.update({ password: newPasswordHash }).then(studentUpdated => {
                                        res.status(200).json({
                                            'response': {
                                                "message": "Password updated",
                                                "data": studentUpdated.get()
                                            },
                                            'error': null
                                        })
                                    });
                                }
                            });
                        }
                    }
                });
            }
        }).catch(err => {
            res.status(406).json(
                {
                    "response": null,
                    "error": "error to change the password: " + err
                }
            )
        })
    }

}

exports.EditPefil = function (req, res, next) {
    let data = {
        name: null,
        birthday: null,
        semester: null,
        phone: null,
        email: null,
        CityId: null,
    }

    let token = req.body.token;

    if (token === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid token"
        })
    }

    let tokenDecoded = jwt.verify(token, config.secret);

    let idStudent = tokenDecoded["idStudent"];

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    for (let key in data) {
        if (data[key] === null) {
            delete data[key];
        }
    }

    if (data.name === undefined && data.birthday === undefined && data.semester === undefined && data.phone === undefined && data.email === undefined && data.CityId === undefined) {
        res.status(201).json({
            "response": "Everything is up to date",
            "error": null
        });
    } else {
        Student.findByPk(idStudent).then(result => {
            if (!result) {
                res.status(404).json({
                    'response': null,
                    'error': "Student perfil not found"
                });
            } else {
                result.update(data).then(studentUpdated => {
                    res.status(200).json({
                        'response': {
                            "message": "Perfil updated",
                            "data": {
                                name: studentUpdated.name,
                                birthday: studentUpdated.birthday,
                                semester: studentUpdated.semester,
                                phone: studentUpdated.phone,
                                email: studentUpdated.email,
                                CityId: studentUpdated.CityId,
                            }
                        },
                        'error': null
                    })
                });
            }
        }).catch(err => {
            res.status(406).json({
                'response': null,
                'error': err
            });
        })
    }

}

exports.EditPhoto = function (req, res, next) {

    let token = req.body.token;
    let idPhoto = req.body.idPhoto;

    if (token == undefined) {
        res.status(406).json(
            {
                "response": null,
                "error": "Invalid params!"
            }
        );
    } else {

        let tokenDecoded = jwt.verify(token, config.secret);

        Student.update(
            {
                PhotoId: idPhoto
            },
            {
                where: {
                    id: tokenDecoded.idStudent
                }
            }
        ).then((value) => {
            res.status(200).json(
                {
                    "error": null,
                    "response": value
                }
            )
        }).catch((err) => {
            res.status(500).json(
                {
                    "response": null,
                    "error": err
                }
            );
        });

    }
}

exports.sendProjectSolicitation = function (req, res, next) {
    let projectId = req.body.projectId;
    let studentId = req.body.studentId;

    let dataMember = {
        StudentId: studentId,
        ProjectId: projectId,
        invite: 0,
        job: 1,
        isMember: 0
    }

    if (projectId === undefined || studentId === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params"
        });
    } else {
        Member.create(dataMember).then(result => {
            if (!result) {
                res.status(406).json({
                    'response': null,
                    'error': "Solicitacion not send"
                });
            } else {
                res.status(201).json({
                    "response": "success",
                    "error": null
                });
            }
        }).catch(err => {
            res.status(500).json(
                {
                    "response": null,
                    "error": err
                }
            );
        })
    }
}

exports.acceptInvite = function (req, res) {
    let projectId = req.body.projectId;
    let studentId = req.body.studentId;

    if (projectId === undefined || studentId === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params"
        });
    } else {
        Member.findOne({
            where: {
                StudentId: studentId,
                ProjectId: projectId
            }
        }).then(result => {
            if (!result) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "Student not found"
                    }
                )
            } else {
                result.update({ isMember: 1 }).then(result => {
                    if (!result) {
                        res.status(406).json({
                            'response': null,
                            'error': "Solicitacion not send"
                        });
                    } else {
                        res.status(201).json({
                            "response": "success",
                            "error": null
                        });
                    }
                })
            }
        })
    }
}

exports.DenyInvite = function (req, res, next) {
    let projectId = req.body.projectId;
    let studentId = req.body.studentId;

    if (projectId === undefined || studentId === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid  params!"
        });
    } else {
        Member.findOne({
            where: {
                StudentId: studentId,
                ProjectId: projectId
            }
        }).then(member => {
            if (!member) {
                res.status(404).json({
                    "response": null,
                    "error": "Member not found"
                });
            } else {
                member.destroy().then(() => {
                    res.status(201).json({
                        "response": "Successfull canceled",
                        "error": null
                    });
                })
            }
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });
    }
}

exports.ListStudentsWithoutProject = function (req, res, next) {

    sequelize.query(`
    select stu.idStudent, stu.name, stu.email, stu.semester, stu.phone, stu.PhotoId, filtro.maximo from Students as stu 
        left join 
        (
            select st.idStudent, st.email, st.name, max(me.isMember) as "maximo" from Students as st
				left join Members as me
					on st.idStudent = me.StudentId
					group by st.idStudent
        ) as filtro
            on stu.idStudent = filtro.idStudent
    where filtro.maximo != 1 or filtro.maximo is null;
    `, { type: sequelize.QueryTypes.SELECT }).then(data => {
        res.status(200).json({ "response": data })
    })
        .catch(err => { res.status(500).json({ "error": err }); });

}