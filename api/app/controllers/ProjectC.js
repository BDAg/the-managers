const { Project, Member, Student, Technology, City, State, SkillMatrix, Photo, Notification, Project_has_Technology } = require("../../sequelize");
const jwt = require("jsonwebtoken");
const config = require("../helpers/configs");
const jobs = require("../helpers/jobs");
const op = require('sequelize').Op;

const Sequelize = require('sequelize');

exports.Create = function (req, res, next) {
    let data = {
        name: null,
        description: null,
        url: null,
        gitlab: null,
        expireAt: null,
    }

    let memberData = {
        StudentId: req.body.StudentId,
        job: new Number(req.body.job).valueOf(),
        ProjectId: null,
        invite: 0,
        isMember: 1
    }

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }
    if (data.name === null || data.description === null || data.gitlab === null || data.expireAt === null || memberData.StudentId === null || memberData.job === NaN) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params!"
        });
    } else {
        Project.findOne({
            where: {
                url: data.url
            }
        }).then(projects => {
            if (projects) {
                data.url += `-${Math.floor(100000 + Math.random() * 900000).toString()}`
            }

            Project.create(data).then(result => {
                if (!result) {
                    res.status(406).json({
                        "response": null,
                        "error": "Project not created"
                    });
                } else {
                    memberData.ProjectId = result.get().id;

                    Member.create(memberData).then(memberResult => {
                        if (!memberResult) {
                            result.destroy();
                            res.status(406).json({
                                "response": null,
                                "error": "Member not created"
                            });
                        } else {
                            res.status(201).json({
                                "response": "Project created",
                                "data": result.get(),
                                "error": null
                            });
                        }
                    }).catch(err => {
                        result.destroy();
                        res.status(406).json({
                            "response": null,
                            "error": err
                        });
                    })

                }
            }).catch(err => {
                res.status(406).json({
                    "response": null,
                    "error": err
                });
            });
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });
    }
}

exports.Edit = function (req, res) {
    let idProject = req.body.idProject;
    let token = req.body.token;

    let data = {
        name: null,
        description: null,
        gitlab: null,
        expireAt: null
    }

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (token === undefined || idProject === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params"
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idStudent = tokenDecoded["idStudent"];
        Member.findOne({
            where: {
                ProjectId: idProject,
                StudentId: idStudent,
                isMember: 1,
                [op.or]: [
                    {
                        job: jobs.PO
                    },
                    {
                        job: jobs.SM
                    }
                ]
            }
        }).then(user => {
            if (!user) {
                res.status(401).json({
                    "response": "Unauthorized",
                    "auth": false,
                    "error": null
                });
            } else {
                if (data.name === undefined && data.description === undefined && data.gitlab === undefined && data.expireAt) {
                    res.status(201).json({
                        "response": "Everything is up to date",
                        "error": null
                    });
                } else {

                    for (let key in data) {
                        if (data[key] === null || data[key] === undefined) {
                            delete data[key];
                        }
                    }

                    Project.findByPk(idProject).then(result => {
                        if (!result) {
                            res.status(404).json({
                                "response": null,
                                "error": "Project not found"
                            });
                        } else {
                            result.update(data).then(resultUpdated => {
                                res.status(201).json({
                                    "response": "Project updated",
                                    "data": resultUpdated.get(),
                                    "error": null
                                });
                            }).catch(err => {
                                res.status(406).json({
                                    "response": null,
                                    "error": err
                                });
                            });
                        }
                    }).catch(err => {
                        res.status(406).json({
                            "response": null,
                            "error": err
                        });
                    })
                }
            }
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });
    }
}

exports.ViewPerfil = function (req, res, next) {
    let url = req.params.url;

    if (url === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params"
        });
    } else {
        Project.findOne({
            where: {
                url: url
            },
            include: [{
                model: Technology,
            },
            {
                model: Student,
                attributes: ["id", "name", "email", "semester"],
                include: [{
                    model: City,
                    attributes: ["name"],
                    include: [{
                        model: State,
                        attributes: ["name"]
                    }]
                },
                {
                    model: Technology,
                    attributes: ["name"],
                    through: {
                        model: SkillMatrix,
                        attributes: ["level"]
                    }
                },
                {
                    model: Photo,
                    attributes: ["path"]
                }
                ],
                through: {
                    model: Member,
                    where: {
                        isMember: 1
                    },
                    attributes: ["job", "ProjectId", "StudentId"],
                },

            }],
        }).then(project => {
            if (!project) {
                res.status(404).json({
                    "response": null,
                    "error": "Project not found"
                });
            } else {
                res.status(201).json({
                    "response": "Success",
                    "data": project.get(),
                    "error": null
                });
            }
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });
    }
}

exports.InviteMember = function (req, res, next) {
    let projectId = req.body.projectId;
    let studentId = req.body.studentId;
    let job = req.body.job;

    let dataMember = {
        StudentId: studentId,
        ProjectId: projectId,
        invite: 1,
        job: job || 1
    }

    let dataNotification = {
        message: null,
        ProjectId: projectId,
        StudentId: studentId,
        type: 0
    }

    if (projectId === undefined || studentId === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid  params!"
        });
    } else {
        Project.findByPk(projectId).then(projectResult => {
            Student.findByPk(studentId).then(studentResult => {
                if (!studentResult) {
                    res.status(404).json({
                        "response": null,
                        "error": "Student not found"
                    });
                } else {
                    if (!projectResult) {
                        res.status(404).json({
                            "response": null,
                            "error": "Project not found"
                        });
                    } else {
                        Member.create(dataMember).then(memberResult => {
                            if (!memberResult) {
                                res.status(406).json({
                                    "response": null,
                                    "error": "Error to invite the member"
                                });
                            } else {
                                dataNotification.message = `Você foi convidado para participar do projeto ${projectResult.name}`
                                Notification.create(dataNotification).then(notificationResult => {
                                    if (!notificationResult) {
                                        res.status(406).json({
                                            "response": null,
                                            "error": "Error to invite the member"
                                        });
                                    } else {
                                        res.status(201).json({
                                            "response": "Student successfull invited",
                                            "error": null
                                        });
                                    }
                                });
                            }
                        }).catch(err => {
                            res.status(406).json({
                                "response": null,
                                "error": err
                            });
                        });
                    }
                }
            }).catch(err => {
                res.status(406).json({
                    "response": null,
                    "error": err
                });
            });
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });

    }
}

exports.CancelInvite = function (req, res, next) {
    let projectId = req.body.projectId;
    let studentId = req.body.studentId;

    if (projectId === undefined || studentId === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid  params!"
        });
    } else {
        Member.findOne({
            where: {
                StudentId: studentId,
                ProjectId: projectId
            }
        }).then(member => {
            if (!member) {
                res.status(404).json({
                    "response": null,
                    "error": "Member not found"
                });
            } else {
                member.destroy().then(() => {
                    res.status(201).json({
                        "response": "Successfull canceled",
                        "error": null
                    });
                })
            }
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });
    }
}

exports.AgreeSolicitation = function (req, res, next) {
    let studentId = req.body.studentId;
    let projectId = req.body.projectId;

    let dataNotification = {
        message: null,
        ProjectId: projectId,
        StudentId: studentId,
        type: 1
    }

    if (studentId === undefined || projectId === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params!"
        });
    } else {
        Member.findOne({
            where: {
                StudentId: studentId,
                ProjectId: projectId,
                isMember: 0
            }
        }).then(result => {
            if (!result) {
                res.status(404).json({
                    "response": null,
                    "error": "Solicitation not found"
                });
            } else {
                result.update({
                    isMember: 1
                }).then(member => {
                    if (!member) {
                        res.status(404).json({
                            "response": null,
                            "error": "Member not found"
                        });
                    } else {
                        Project.findByPk(projectId).then(project => {
                            if (!project) {
                                res.status(404).json({
                                    "response": null,
                                    "error": "Project not found"
                                });
                            } else {
                                dataNotification.message = `Sua solicitação para entrar em ${project.name} foi aceita!`

                                Notification.create(dataNotification).then(() => {
                                    res.status(201).json({
                                        "response": "Solicitation agreed",
                                        "error": null
                                    });
                                }).catch(err => {
                                    res.status(406).json({
                                        "response": null,
                                        "error": err
                                    });
                                });
                            }

                        }).catch(err => {
                            res.status(406).json({
                                "response": null,
                                "error": err
                            });
                        })
                    }
                }).catch(err => {
                    res.status(406).json({
                        "response": null,
                        "error": err
                    });
                });
            }
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });
    }
}

exports.DenySolicitation = function (req, res, next) {
    let studentId = req.body.studentId;
    let projectId = req.body.projectId;

    let dataNotification = {
        message: null,
        ProjectId: projectId,
        StudentId: studentId,
        type: 2
    }

    if (studentId === undefined || projectId === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params!"
        });
    } else {
        Member.findOne({
            where: {
                StudentId: studentId,
                ProjectId: projectId,
                isMember: 0
            }
        }).then(result => {
            if (!result) {
                res.status(404).json({
                    "response": null,
                    "error": "Solicitation not found"
                });
            } else {
                result.destroy().then(() => {
                    Project.findByPk(projectId).then(project => {
                        if (!project) {
                            res.status(404).json({
                                "response": null,
                                "error": "Project not found"
                            });
                        } else {
                            dataNotification.message = `Sua solicitação para entrar em ${project.name} foi recusada!`

                            Notification.create(dataNotification).then(() => {
                                res.status(201).json({
                                    "response": "Successfull denied",
                                    "error": null
                                });
                            }).catch(err => {
                                res.status(406).json({
                                    "response": null,
                                    "error": err
                                });
                            });
                        }
                    }).catch(err => {
                        res.status(406).json({
                            "response": null,
                            "error": err
                        });
                    });
                }).catch(err => {
                    res.status(406).json({
                        "response": null,
                        "error": err
                    });
                });
            }
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });
    }
}

exports.ListInvites = function (req, res, next) {

    let idProject = req.body.idProject;
    let token = req.body.token;
    let inviteOrSolicitation = new Number(req.params.invite).valueOf();
    if (idProject == undefined || token == undefined) {
        res.status(406).json(
            {
                "response": null,
                "error": "Invalid Params!"
            }
        );
    } else {

        let tokenDecoded = jwt.verify(token, config.secret);
        let idStudent = tokenDecoded["idStudent"];
        Member.findOne({
            where:
            {
                ProjectId: idProject,
                StudentId: idStudent,
                [op.or]: [{
                    job: jobs.PO
                },
                {
                    job: jobs.SM
                }]
            }
        })
            .then((data) => {
                if (!data) {
                    res.status(406).json(
                        {
                            "response": null,
                            "error": "Not Authorized"
                        }
                    );
                } else {
                    Member.findAll({
                        where: {
                            ProjectId: idProject,
                            invite: inviteOrSolicitation,
                            isMember: 0
                        },
                        include: [
                            {
                                model: Student,
                                attributes: [
                                    "id",
                                    "name",
                                    "email"
                                ]
                            }
                        ]
                    })
                        .then(members => {
                            res.status(200).json(
                                {
                                    "error": null,
                                    "response": members
                                }
                            );
                        })
                        .catch(err => {
                            res.status(500).json(
                                {
                                    "error": err,
                                    "response": null
                                }
                            );
                        });
                }
            })
            .catch((err) => {
                res.status(500).json(
                    {
                        "response": null,
                        "error": err
                    }
                );
            });
    }

}

exports.List = function (req, res) {
    Project.findAll(
        {
            order: [
                ['createdAt', 'DESC']
            ]
        }
    ).then(projects => {
        res.status(201).json({
            "response": {
                "data": projects
            },
            "error": null
        })
    }).catch(err => {
        res.status(406).json({
            "response": null,
            "error": err
        });
    });
}

exports.addTechnology = function (req, res) {
    let projectId = req.body.projectId;
    let technologyId = req.body.technologyId;

    let data = {
        ProjectId: projectId,
        TechnologyId: technologyId
    }

    if (projectId === undefined || technologyId === undefined) {
        res.status(406).json(
            {
                "response": null,
                "error": "Invalid Params!"
            }
        );
    } else {
        Project_has_Technology.create(data).then(response => {
            if (!response) {
                res.status(406).json({
                    "response": null,
                    "error": "Not added!"
                });
            } else {
                res.status(201).json({
                    "response": "Successfull added",
                    "error": null
                });
            }
        }).catch((err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        }));
    }
}

exports.removeTechnology = function (req, res) {
    let projectId = req.body.projectId;
    let technologyId = req.body.technologyId;
    if (projectId === undefined || technologyId === undefined) {
        res.status(406).json(
            {
                "response": null,
                "error": "Invalid Params!"
            }
        );
    } else {
        Project_has_Technology.findOne({
            where: {
                ProjectId: projectId,
                TechnologyId: technologyId
            }
        }).then(result => {
            if (!result) {
                res.status(404).json({
                    "response": null,
                    "error": "Not founde!"
                });
            } else {
                result.destroy().then(() => {
                    res.status(201).json({
                        "response": "Successfull removed",
                        "error": null
                    });
                }).catch(err => {
                    res.status(406).json({
                        "response": null,
                        "error": err
                    });
                });
            }
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });
    }
}

exports.EditMemberJob = function (req, res) {
    let idMember = req.body.idMember;
    let idProject = req.body.idProject;
    let newJob = req.body.newJob;
    let token = req.body.token;

    if (idMember === undefined || idProject === undefined || newJob === undefined || token === undefined) {
        res.status(406).json({
            "error": "Invalid params",
            "response": null
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idStudent = tokenDecoded["idStudent"];

        Member.findOne({
            where: {
                StudentId: idStudent,
                ProjectId: idProject
            }
        }).then(result => {
            if (!result) {
                res.status(404).json({
                    "response": null,
                    "error": "Member not found!"
                });
            } else {
                if (result.job === 1) {
                    res.status(401).json({
                        "reponse": "Unauthorized",
                        "error": null
                    });
                } else {
                    Member.findOne({
                        where: {
                            StudentId: idMember,
                            ProjectId: idProject
                        }
                    }).then(member => {
                        if (!member) {
                            res.status(404).json({
                                "response": null,
                                "error": "Member not found!"
                            });
                        } else {
                            member.update({
                                job: newJob
                            }).then(() => {
                                res.status(201).json({
                                    "response": "Member job updated",
                                    "error": null
                                });
                            }).catch(err => {
                                res.status(500).json({
                                    "error": err,
                                    "response": null
                                });
                            });
                        }
                    }).catch(err => {
                        res.status(500).json({
                            "error": err,
                            "response": null
                        });
                    });
                }
            }
        }).catch(err => {
            res.status(500).json({
                "error": err,
                "response": null
            });
        });
    }
}

exports.RemoveMember = function (req, res) {
    let idMember = req.body.idMember;
    let idProject = req.body.idProject;
    let token = req.body.token;

    if (idMember === undefined || idProject === undefined || token === undefined) {
        res.status(406).json({
            "error": "Invalid params",
            "response": null
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idStudent = tokenDecoded["idStudent"];

        Member.findOne({
            where: {
                StudentId: idStudent,
                ProjectId: idProject
            }
        }).then(result => {
            if (!result) {
                res.status(404).json({
                    "response": null,
                    "error": "Member not found!"
                });

            } else {
                if (result.job === 1) {
                    res.status(401).json({
                        "reponse": "Unauthorized",
                        "error": null
                    });
                } else {
                    Member.findOne({
                        where: {
                            StudentId: idMember,
                            ProjectId: idProject
                        }
                    }).then(member => {
                        if (!member) {
                            res.status(404).json({
                                "response": null,
                                "error": "Member not found!"
                            });
                        } else {
                            member.destroy().then(() => {
                                res.status(201).json({
                                    "response": "Member removed",
                                    "error": null
                                });
                            }).catch(err => {
                                res.status(500).json({
                                    "error": err,
                                    "response": null
                                });
                            });
                        }
                    }).catch(err => {
                        res.status(500).json({
                            "error": err,
                            "response": null
                        });
                    });
                }
            }
        }).catch(err => {
            res.status(500).json({
                "error": err,
                "response": null
            });
        });

    }
}
// function generateUrl(string, callback) {
//     url = string.replace(/[^\w]/gi, ' ').split(" ").join("-").toLowerCase()

//     while (url.includes("--")) {
//         url = url.split("--").join("-");
//     }

//     if (url[url.length - 1] == "-") {
//         url = url.slice(0, url.length - 1)
//     }

//     if (url[0] == "-") {
//         url = url.slice(1, url.length)
//     }

//     callback(url)
// }