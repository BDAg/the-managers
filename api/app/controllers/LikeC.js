const { Like } = require("../../sequelize");

const jwt = require("jsonwebtoken"),
    config = require('../helpers/configs');

exports.Like = function (req, res) {

    let token = req.body.token;
    let data = {
        PostId: req.body.postId,
        StudentId: undefined,
        ProfessorId: undefined
    }

    if (token === undefined || data.PostId === undefined) {
        if (token === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        }
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);

        if ('idProfessor' in tokenDecoded) {
            data.ProfessorId = tokenDecoded['idProfessor'];

        } else if ('idStudent' in tokenDecoded) {
            data.StudentId = tokenDecoded['idStudent'];
        }

        if (data.StudentId === undefined && data.ProfessorId === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        } else {
            for (let key in data) {
                if (data[key] === undefined) {
                    delete data[key]
                }
            }

            Like.findOne({
                where: data
            }). then(hasLike => {
                if(hasLike){
                    res.status(406).json({
                        "error": "Post already liked",
                        "response": null
                    });
                } else {
                    Like.create(data).then(like => {
                        res.status(201).json({
                            "response": {
                                "message": "Liked",
                                "data": like.get()
                            }
                        });
                    }).catch(err => {
                        res.status(500).json({
                            "error": err,
                            "response": null
                        });
                    });
                }
            }).catch(err => {
                res.status(500).json({
                    "error": err,
                    "response": null
                });
            });
        }
    }

}

exports.Unlike = function (req, res) {

    let token = req.body.token;
    let data = {
        PostId: req.body.postId,
        StudentId: undefined,
        ProfessorId: undefined
    }

    if (token === undefined || data.PostId === undefined) {
        if (token === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        }
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);

        if ('idProfessor' in tokenDecoded) {
            data.ProfessorId = tokenDecoded['idProfessor'];

        } else if ('idStudent' in tokenDecoded) {
            data.StudentId = tokenDecoded['idStudent'];
        }

        if (data.StudentId === undefined && data.ProfessorId === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        } else {
            for (let key in data) {
                if (data[key] === undefined) {
                    delete data[key]
                }
            }

            Like.findOne({
                where: data
            }).then(like => {
                if (!like) {
                    res.status(404).json({
                        "error": "Not found",
                        "response": null
                    });
                } else {
                    like.destroy().then(() => {
                        res.status(200).json({
                            "response": "Unliked",
                            "error": null
                        });
                    }).catch(err => {
                        res.status(500).json({
                            "error": err,
                            "response": null
                        });
                    });
                }
            }).catch(err => {
                res.status(500).json({
                    "error": err,
                    "response": null
                });
            });
        }
    }
}