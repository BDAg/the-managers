const { Technology } = require('../../sequelize');

exports.List = function (req, res) {
    Technology.findAll({attributes: ['name', 'id']}).then(result => {
        return res.status(201).json({
            "error": null,
            "response": {
                "data": result
            }
        })
    }).catch(err => {
        res.status(406).json({
            'response': null,
            'error': err
        });
    })
}