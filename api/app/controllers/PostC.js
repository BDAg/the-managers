const { Post, Student, Professor, Photo, Project, Comentary, Like } = require('../../sequelize');

const jwt = require("jsonwebtoken"),
    config = require('../helpers/configs');

exports.Create = function (req, res) {
    let token = req.body.token;
    let data = {
        description: undefined,
        StudentId: undefined,
        ProfessorId: undefined,
        ProjectId: undefined,
        PhotoId: undefined
    }

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (token === undefined) {
        res.status(406).json({
            "error": "Invalid params",
            "response": null
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);

        if ('idProfessor' in tokenDecoded) {
            data.ProfessorId = tokenDecoded['idProfessor'];

        } else if ('idStudent' in tokenDecoded) {
            data.StudentId = tokenDecoded['idStudent'];
        }

        if ((data.StudentId === undefined && data.ProfessorId === undefined) || (data.description === undefined && data.PhotoId === undefined)) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        } else {
            for (let key in data) {
                if (data[key] === undefined) {
                    delete data[key]
                }
            }
            Post.create(data).then(result => {
                res.status(201).json({
                    "response": {
                        "message": "Created",
                        "data": result.get()
                    },
                    "error": null
                });
            }).catch(err => {
                res.status(500).json({
                    "error": err,
                    "response": null
                });
            })
        }
    }
}

exports.Delete = function (req, res) {
    let token = req.body.token;

    let data = {
        id: req.body.postId,
        ProfessorId: undefined,
        StudentId: undefined
    }

    if (token === undefined || data.id === undefined) {
        res.status(406).json({
            "error": "Invalid params",
            "response": null
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);

        if ('idProfessor' in tokenDecoded) {
            data.ProfessorId = tokenDecoded['idProfessor'];

        } else if ('idStudent' in tokenDecoded) {
            data.StudentId = tokenDecoded['idStudent'];
        }
        if (data.StudentId === undefined && data.ProfessorId === undefined) {
            res.status(406).json({
                "error": "Invalid token",
                "response": null
            });
        } else {
            for (let key in data) {
                if (data[key] === undefined) {
                    delete data[key]
                }
            }

            Post.findOne({
                where: data
            }).then(result => {
                if (!result) {
                    res.status(404).json({
                        "error": "Post not found",
                        "response": null
                    });
                } else {
                    result.destroy().then(() => {
                        res.status(200).json({
                            "error": null,
                            "response": "Post deleted"
                        });
                    }).catch(err => {
                        res.status(500).json({
                            "error": err,
                            "response": null
                        });
                    });
                }
            }).catch(err => {
                res.status(500).json({
                    "error": err,
                    "response": null
                });
            });
        }
    }
}

exports.List = function (req, res) {
    Post.findAll(
        {
            order: [
                ["createdAt", "DESC"]
            ],
            attributes: ["id", "description"],
            include: [
                {
                    model: Photo,
                    attributes: ["path", "id"]
                },
                {
                    model: Student,
                    attributes: ["id", "name", "email"],
                    include: [
                        {
                            model: Photo,
                            attributes: ["path"]
                        }
                    ]
                },
                {
                    model: Professor,
                    attributes: ["id", "name", "email"],
                    include: [
                        {
                            model: Photo,
                            attributes: ["path"]
                        }
                    ]
                },
                {
                    model: Project,
                    attributes: ["name", "url"]
                },
                {
                    model: Like,
                    include: [
                        {
                            model: Student,
                            attributes: ["name", "email"],
                            include: [
                                {
                                    model: Photo,
                                    attributes: ["path"]
                                }
                            ]
                        },
                        {
                            model: Professor,
                            attributes: ["name"],
                            include: [
                                {
                                    model: Photo,
                                    attributes: ["path"]
                                }
                            ]
                        },
                    ]
                },
                {
                    model: Comentary,
                    attributes: ["id", "comment", "createdAt"],
                    include: [
                        {
                            model: Student,
                            attributes: ["id", "name", "email"],
                            include: [
                                {
                                    model: Photo,
                                    attributes: ["path"]
                                }
                            ]
                        },
                        {
                            model: Professor,
                            attributes: ["name"],
                            include: [
                                {
                                    model: Photo,
                                    attributes: ["path"]
                                }
                            ]
                        },
                    ]
                }
            ]
        }
    ).then(result => {
        res.status(200).json({
            "response": result,
            "error": null
        });
    }).catch(err => {
        res.status(500).json({
            "error": err.toString(),
            "response": null
        });
    });;
}

exports.Edit = function (req, res) {
    let token = req.body.token;

    let data = {
        description: req.body.description,
        ProjectId: req.body.ProjectId
    }

    let dataFind = {
        id: req.body.postId,
        ProfessorId: undefined,
        StudentId: undefined
    }

    if (data.description === undefined && data.ProjectId === undefined) {
        res.status(204).json({
            "response": "Nothing to update",
            "error": null
        });
    } else {
        if (token === undefined || dataFind.id === undefined) {
            res.status(406).json({
                "error": "Invalid params",
                "response": null
            });
        } else {
            let tokenDecoded = jwt.verify(token, config.secret);

            if ('idProfessor' in tokenDecoded) {
                dataFind.ProfessorId = tokenDecoded['idProfessor'];

            } else if ('idStudent' in tokenDecoded) {
                dataFind.StudentId = tokenDecoded['idStudent'];
            }

            if (dataFind.StudentId === undefined && dataFind.ProfessorId === undefined) {
                res.status(406).json({
                    "error": "Invalid token",
                    "response": null
                });
            } else {
                for (let key in dataFind) {
                    if (dataFind[key] === undefined) {
                        delete dataFind[key]
                    }
                }
                for (let key in data) {
                    if (data[key] === undefined) {
                        delete data[key]
                    }
                }

                Post.findOne({
                    where: dataFind
                }).then(result => {
                    if(!result) {
                        res.status(404).json({
                            "error": "Post not found",
                            "response": null
                        });
                    } else {
                        result.update(data).then(() => {
                            res.status(200).json({
                                "response": {
                                    "message": "Post updated",
                                    "error": null
                                }
                            })
                        })
                    }
                }).catch(err => {
                    res.status(500).json({
                        "error": err.toString(),
                        "response": null
                    });
                });
            }
        }
    }
}