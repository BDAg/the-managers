const { Administrator } = require("../../sequelize");
const bcrypt = require("bcryptjs");
const config = require("../helpers/configs");
const jwt = require("jsonwebtoken");

exports.Create = function (req, res) {
    let data = {
        name: null,
        email: null,
        password: null,
        role: 3
    }

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (data.name === null || data.email === null || data.password === null || data.password.length < 6) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params!"
        });
    } else {
        for (let key in data) {
            if (data[key] === null) {
                delete data[key];
            }
        }

        bcrypt.hash(data.password, 10, (err, hash) => {
            if (err) {
                res.status(406).json({
                    "response": null,
                    "error": err
                })
            } else {
                data.password = hash;
                Administrator.create(data).then(result => {
                    let token = jwt.sign(
                        {
                            email: result.email,
                            idAdmin: result.id,
                            name: result.name,
                            role: result.role
                        },
                        config.secret,
                        { expiresIn: "3h" }
                    );
                    res.status(201).json(
                        {
                            "response": "Admin created",
                            "data": result.get(),
                            "token": token
                        }
                    );
                }).catch(err => {
                    res.status(406).json(
                        {
                            "response": "Admin not created",
                            "error": err.toString()
                        }
                    )
                });
            }
        });
    }
}

exports.Login = function(req, res) {
    let email = req.body.email;
    let password = req.body.password;

    if (email === undefined || password === undefined || password.length < 6) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {
        Administrator.findOne({
            where: {
                email: email,
            }
        }).then(result => {
            if (!result) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "admin not found"
                    }
                )
            } else {
                bcrypt.compare(password, result.get().password, function (err, authenticated) {
                    if (err) {
                        res.status(406).json(
                            {
                                "response": null,
                                "error": err
                            }
                        )
                    } else {

                        if (!authenticated) {
                            res.status(401).json({
                                'error': null,
                                'response': {
                                    'auth': false,
                                    'message': 'Not authenticated!'
                                }
                            });
                        } else {
                            let token = jwt.sign({
                                email: result.email,
                                idAdmin: result.id,
                                name: result.name,
                                role: result.role
                            }, config.secret, { expiresIn: "120d" });

                            res.status(200).json({
                                "error": null,
                                "response": {
                                    "auth": true,
                                    "message": "Authenticated!",
                                    "token": token,
                                    "data": {
                                        name: result.name,
                                        email: result.email,
                                        role: result.role,
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }
}