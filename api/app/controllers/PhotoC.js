const { Photo } = require("../../sequelize");
const jwt = require("jsonwebtoken"),
    config = require('../helpers/configs'),
    fs = require('fs')
sizeOf = require('buffer-image-size');


exports.Upload = function (req, res, next) {
    let token = req.body.token;
    let nomeImg = req.body.nomeImage;
    let image = req.body.image;
    let type = req.body.type.split("/")[1];

    if (token == undefined || nomeImg == undefined || image == undefined) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid params'
        });
    } else {

        let tokenDecoded = jwt.verify(token, config.secret);

        let realImage = Buffer.from(image.replace(`data:image\/${type};base64,`, ''), "base64");
        let dimensions = sizeOf(realImage);

        let idToken;
        let data = {
            path: null,
            width: dimensions.width,
            heigth: dimensions.height
        }

        if ('idProfessor' in tokenDecoded) {
            idToken = 'professor/' + tokenDecoded['idProfessor'];

        } else if ('idStudent' in tokenDecoded) {
            idToken = 'student/' + tokenDecoded['idStudent'];

        } else if ('idPost' in tokenDecoded) {
            idToken = 'post/' + tokenDecoded['idPost'];
        } else {
            res.status('406').json({
                'response': null,
                'error': 'Invalid Token'
            });
        }

        let diretorio = __dirname + "/../../photos/" + idToken;

        if (!fs.existsSync(diretorio)) {
            fs.mkdirSync(diretorio, { recursive: true });
        }

        nomeImg = nomeImg + new Date().getTime().toString()
        data.path = idToken + '/' + nomeImg + '.' + type
        if (data.path === null || data.heigth === null || data.width === null) {
            res.status(406).json({
                'response': null,
                "error": "Null fields"
            });
        }
        else {
            fs.writeFile(diretorio + '/' + nomeImg + '.' + type, realImage, function (err) {
                if (err) {
                    res.status(406).json({
                        'response': null,
                        'error': err
                    });
                } else {
                    Photo.create(data).then(result => {
                        res.status(201).json({
                            'error': null,
                            'response': {
                                'message': 'Success',
                                'path': result.get()
                            }
                        });
                    }).catch(error => {
                        res.status(406).json({
                            'response': null,
                            'error': error
                        });
                    });
                }
            });
        }
    }
}

exports.Delete = function (req, res) {
    let idFoto = req.body.idFoto;

    if (idFoto === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params!"
        });
    } else {
        Photo.findByPk(idFoto).then(result => {
            if (!result) {
                res.status(404).json({
                    "response": null,
                    "error": "Photo not found"
                });
            }
            else {
                result.destroy().then(() => {

                    fs.unlinkSync(__dirname + "/../../photos/" + result.path);

                    res.status(201).json({
                        "response": "Photo success deleted",
                        "error": null
                    });

                }).catch(err => {
                    res.status(406).json({
                        "response": null,
                        "error": err
                    });
                });

            }
        }).catch(err => {
            res.status(406).json({
                "response": null,
                "error": err
            });
        });
    }
}