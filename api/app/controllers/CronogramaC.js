const { Professor, Cronograma, TopicCronograma } = require("../../sequelize");
const jwt = require("jsonwebtoken");
const config = require("../helpers/configs");

exports.CreateTopic = function (req, res) {
    let data = {
        description: null,
        semester: null,
        ProfessorId: null,
    }

    let token = req.body.token;

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (token === undefined) {
        return res.status(406).json({
            "response": null,
            "error": "Invalid token!"
        });
    }
    else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idProfessor = tokenDecoded["idProfessor"];

        if (idProfessor) {
            data.ProfessorId = idProfessor;
            Professor.findByPk(idProfessor).then(professor => {
                if (!professor) {
                    res.status(401).json({
                        "response": null,
                        "error": "Unauthorized"
                    });
                } else {
                    if (data.description === null || data.semester === null) {
                        return res.status(406).json({
                            "response": null,
                            "error": "Invalid params!"
                        });
                    } else {
                        for (let key in data) {
                            if (data[key] === null || data[key] === undefined) {
                                delete data[key];
                            }
                        }

                        TopicCronograma.create(data).then(result => {
                            res.status(201).json({
                                "response": {
                                    "message": "Created",
                                    "data": result.get()
                                },
                                "error": null
                            });
                        }).catch(err => {
                            res.status(500).json({
                                "response": null,
                                "error": err.toString()
                            });
                        });
                    }
                }
            })
        } else {
            return res.status(406).json({
                "response": null,
                "error": "Invalid token!"
            });
        }
    }
}

exports.Create = function (req, res) {
    let data = {
        description: null,
        expireAt: null,
        startsAt: null,
        semester: null,
        ProfessorId: null,
        TopicsCronogramaId: null
    }

    let token = req.body.token;

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (token === undefined) {
        return res.status(406).json({
            "response": null,
            "error": "Invalid token!"
        });
    }
    else {
        for (let key in data) {
            if (data[key] === null || data[key] === undefined) {
                delete data[key];
            }
        }

        let tokenDecoded = jwt.verify(token, config.secret);
        let idProfessor = tokenDecoded["idProfessor"];

        if (idProfessor) {
            data.ProfessorId = idProfessor;
            Professor.findByPk(idProfessor).then(professor => {
                if (!professor) {
                    res.status(401).json({
                        "response": null,
                        "error": "Unauthorized"
                    });
                } else {
                    if (data.description === null || data.semester === null || data.expireAt === null || data.TopicsCronogramaId === null || data.startsAt === null) {
                        return res.status(406).json({
                            "response": null,
                            "error": "Invalid params!"
                        });
                    } else {
                        for (let key in data) {
                            if (data[key] === null || data[key] === undefined) {
                                delete data[key];
                            }
                        }

                        Cronograma.create(data).then(result => {
                            res.status(201).json({
                                "response": {
                                    "message": "Created",
                                    "data": result.get()
                                },
                                "error": null
                            });
                        }).catch(err => {
                            res.status(500).json({
                                "response": null,
                                "error": err.toString()
                            });
                        });
                    }
                }
            })
        } else {
            return res.status(406).json({
                "response": null,
                "error": "Invalid token!"
            });
        }
    }
}

exports.EditItem = function (req, res) {
    let token = req.body.token;
    let itemId = req.body.itemId;

    let data = {
        description: null,
        expireAt: null,
        startsAt: null,
        ProfessorId: null,
        TopicsCronogramaId: null
    }

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (token === undefined) {
        return res.status(406).json({
            "response": null,
            "error": "Invalid token!"
        });
    } else {
        for (let key in data) {
            if (data[key] === null || data[key] === undefined) {
                delete data[key];
            }
        }
        if (Object.keys(data).length > 0) {
            let tokenDecoded = jwt.verify(token, config.secret);
            let idProfessor = tokenDecoded["idProfessor"];

            if (idProfessor) {
                Professor.findByPk(idProfessor).then(professor => {
                    if (!professor) {
                        res.status(401).json({
                            "response": null,
                            "error": "Unauthorized"
                        });
                    } else {
                        if (itemId === undefined) {
                            return res.status(406).json({
                                "response": null,
                                "error": "Invalid params!"
                            });
                        } else {
                            Cronograma.findByPk(itemId).then(item => {
                                if (!item) {
                                    res.status(404).json({
                                        "response": null,
                                        "error": "item not found"
                                    });
                                } else {
                                    item.update(data).then((resUpdated) => {
                                        res.status(200).json({
                                            "response": "Item updated",
                                            "error": null
                                        })
                                    })
                                }
                            })
                        }
                    }
                })
            } else {
                return res.status(406).json({
                    "response": null,
                    "error": "Invalid token!"
                });
            }
        } else {
            res.status(304).json({
                "response": "Everything is up to date!",
                "error": null
            });
        }
    }
}

exports.EditTopic = function (req, res) {
    let token = req.body.token;
    let itemId = req.body.itemId;

    let data = {
        description: null,
        ProfessorId: null,
    }

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (token === undefined) {
        return res.status(406).json({
            "response": null,
            "error": "Invalid token!"
        });
    } else {
        for (let key in data) {
            if (data[key] === null || data[key] === undefined) {
                delete data[key];
            }
        }
        if (Object.keys(data).length > 0) {
            let tokenDecoded = jwt.verify(token, config.secret);
            let idProfessor = tokenDecoded["idProfessor"];

            if (idProfessor) {
                Professor.findByPk(idProfessor).then(professor => {
                    if (!professor) {
                        res.status(401).json({
                            "response": null,
                            "error": "Unauthorized"
                        });
                    } else {
                        if (itemId === undefined) {
                            return res.status(406).json({
                                "response": null,
                                "error": "Invalid params!"
                            });
                        } else {
                            TopicCronograma.findByPk(itemId).then(item => {
                                if (!item) {
                                    res.status(404).json({
                                        "response": null,
                                        "error": "item not found"
                                    });
                                } else {
                                    item.update(data).then((resUpdated) => {
                                        res.status(200).json({
                                            "response": "Item updated",
                                            "error": null
                                        })
                                    })
                                }
                            })
                        }
                    }
                })
            } else {
                return res.status(406).json({
                    "response": null,
                    "error": "Invalid token!"
                });
            }
        } else {
            res.status(304).json({
                "response": "Everything is up to date!",
                "error": null
            });
        }
    }
}

exports.List = function (req, res) {
    let semester = req.params.semester;

    TopicCronograma.findAll({
        where: {
            semester: semester
        },
        include: {
            model: Cronograma,
        },
        order: [
            ['createdAt', 'ASC'],
            [{ model: Cronograma }, 'expireAt', 'ASC']
        ]
    }).then(tutorials => {
        res.status(200).json({
            "response": tutorials,
            "error": null
        });
    }).catch(err => {
        res.status(500).json({
            "response": null,
            "error": err.toString()
        });
    });
}

exports.DeleteItem = function (req, res) {
    let itemId = req.body.itemId;
    let token = req.body.token;

    if (token === undefined) {
        return res.status(406).json({
            "response": null,
            "error": "Invalid token!"
        });
    }

    else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idProfessor = tokenDecoded["idProfessor"];

        if (idProfessor) {
            Professor.findByPk(idProfessor).then(professor => {
                if (!professor) {
                    res.status(401).json({
                        "response": null,
                        "error": "Unauthorized"
                    });
                } else {
                    if (itemId === undefined) {
                        return res.status(406).json({
                            "response": null,
                            "error": "Invalid params!"
                        });
                    } else {
                        Cronograma.findByPk(itemId).then(item => {
                            if (!item) {
                                res.status(404).json({
                                    "response": null,
                                    "error": "item not found"
                                });
                            } else {
                                item.destroy().then(result => {
                                    res.status(200).json({
                                        "response": "deleted",
                                        "error": null
                                    });
                                }).catch(err => {
                                    res.status(500).json({
                                        "response": null,
                                        "error": err.toString()
                                    });
                                });
                            }
                        })
                    }
                }
            })
        } else {
            return res.status(406).json({
                "response": null,
                "error": "Invalid token!"
            });
        }
    }
}

exports.DeleteTopic = function (req, res) {
    let itemId = req.body.itemId;
    let token = req.body.token;

    if (token === undefined) {
        return res.status(406).json({
            "response": null,
            "error": "Invalid token!"
        });
    }

    else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idProfessor = tokenDecoded["idProfessor"];

        if (idProfessor) {

            Professor.findByPk(idProfessor).then(professor => {
                if (!professor) {
                    res.status(401).json({
                        "response": null,
                        "error": "Unauthorized"
                    });
                } else {
                    if (itemId === undefined) {
                        return res.status(406).json({
                            "response": null,
                            "error": "Invalid params!"
                        });
                    } else {
                        TopicCronograma.findByPk(itemId).then(item => {
                            if (!item) {
                                res.status(404).json({
                                    "response": null,
                                    "error": "item not found"
                                });
                            } else {
                                item.destroy().then(result => {
                                    res.status(200).json({
                                        "response": "deleted",
                                        "error": null
                                    });
                                }).catch(err => {
                                    res.status(500).json({
                                        "response": null,
                                        "error": err.toString()
                                    });
                                });
                            }
                        })
                    }
                }
            })
        } else {
            return res.status(406).json({
                "response": null,
                "error": "Invalid token!"
            });
        }
    }
}