const { State } = require("../../sequelize");

exports.list = function(req, res, next) {
    
    State.findAll()
    .then(data => {
        res.status(200).json(
            {
                "error": null,
                "response": data
            }
        );
    })
    .catch(err => {
        res.status(500).json(
            {
                "response": null,
                "error": err
            }
        );
    });

}