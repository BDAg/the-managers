const { City } = require("../../sequelize");

exports.list = function(req, res, next) {
    
    City.findAll({
        order: [
            ['name', 'ASC'],
        ],
    })
    .then(data => {
        res.status(200).json(
            {
                "error": null,
                "response": data
            }
        );
    })
    .catch(err => {
        res.status(500).json(
            {
                "response": null,
                "error": err
            }
        );
    });

}

exports.listWithState = function(req, res, next) {

    let idState = req.params.idState;

    if (idState == undefined) {
        res.status(406).json(
            {
                "response": null,
                "error": "Invalid Params!"
            }
        );
    } else {
        City.findAll(
            {
                where: {
                    StateId: idState
                }
            }
        )
        .then(data => {
            res.status(200).json(
                {
                    "error": null,
                    "response": data
                }
            );
        })
        .catch(err => {
            res.status(500).json(
                {
                    "response": null,
                    "error": err
                }
            );
        });
    }

}