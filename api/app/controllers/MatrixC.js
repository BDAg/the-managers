const { SkillMatrix } = require('../../sequelize');
const jwt = require("jsonwebtoken");
const config = require("../helpers/configs");

exports.addTechnology = function (req, res) {
    let token = req.body.token;
    let technology = req.body.technology;
    let level = req.body.level;

    if (token === undefined || technology === undefined || level === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params"
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idStudent = tokenDecoded["idStudent"];
        if (!idStudent) {
            res.status(406).json({
                "response": null,
                "error": "Invalid token"
            });
        }
        else {
            SkillMatrix.create({
                "level": level,
                "StudentId": idStudent,
                "TechnologyId": technology
            }).then((response) => {
                res.status(201).json({
                    "response": {
                        "message": "Technology added",
                        "data": response.get()
                    }
                });
            }).catch(err => {
                res.status(406).json({
                    "response": null,
                    "error": err
                });
            });
        }
    }
}

exports.removeTechnology = function (req, res) {
    let token = req.body.token;
    let technology = req.body.technology;

    if (token === undefined || technology === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params"
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idStudent = tokenDecoded["idStudent"];
        if (!idStudent) {
            res.status(406).json({
                "response": null,
                "error": "Invalid token"
            });
        } else {
            SkillMatrix.findOne({
                where:{
                    "StudentId": idStudent,
                    "TechnologyId": technology
                } 
            }).then((response) => {
                if (!response) {
                    res.status(404).json({
                        "response": null,
                        "error": "Not found"
                    });
                } else {
                    response.destroy().then(() => {
                        res.status(201).json({
                            "response": {
                                "message": "Technology removed",
                                "data": response.get()
                            }
                        });
                    }).catch(err => {
                        es.status(406).json({
                            "response": null,
                            "error": err
                        });
                    });
                }
            }).catch(err => {
                res.status(406).json({
                    "response": null,
                    "error": err
                });
            });
        }
    }
}

exports.editTechnology = function(req, res) {
    let token = req.body.token;
    let technology = req.body.technology;
    let level = req.body.level; 

    if (token === undefined || technology === undefined || level === undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params"
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idStudent = tokenDecoded["idStudent"];
        if (!idStudent) {
            res.status(406).json({
                "response": null,
                "error": "Invalid token"
            });
        } else {
            SkillMatrix.findOne({
                where:{
                    "StudentId": idStudent,
                    "TechnologyId": technology
                } 
            }).then((response) => {
                if (!response) {
                    res.status(404).json({
                        "response": null,
                        "error": "Not found"
                    });
                } else {
                    response.update({
                        "level": level
                    }).then(() => {
                        res.status(201).json({
                            "response": {
                                "message": "Technology updated",
                                "data": response.get()
                            }
                        });
                    }).catch(err => {
                        res.status(406).json({
                            "response": null,
                            "error": err
                        });
                    });
                }
            }).catch(err => {
                res.status(406).json({
                    "response": null,
                    "error": err
                });
            });
        }
    }
}