const { Notification } = require('../../sequelize');
const jwt = require("jsonwebtoken");
const config = require('../helpers/configs');

exports.ListUserNotifications = function (req, res, next ) {
    let token = req.headers.authorization;

    if (token === undefined) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idStudent = tokenDecoded["idStudent"];

        if (!idStudent) {
            res.status(401).json({
                'error': null,
                'response': "Unauthorized"
            });
        } else {
            Notification.findAll({
                where: {
                    StudentId: idStudent,
                    hidden: 0
                },
            }).then(result => {
                res.status(201).json({
                    "error": null,
                    "response": result
                });
            }).catch(err => {
                res.status(500).json({
                    "error": err,
                    "response": null
                });
            });
        }
    }
}

exports.HideNotifications = function (req, res) {
    let token = req.body.token;
    let idNotification = req.body.idNotification;

    if (token === undefined || idNotification === undefined) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {
        let tokenDecoded = jwt.verify(token, config.secret);
        let idStudent = tokenDecoded["idStudent"];
        
        if (!idStudent) {
            res.status(401).json({
                'error': null,
                'response': "Unauthorized"
            });
        } else {
            Notification.findByPk(idNotification).then(result => {
                if (!result) {
                    res.status(404).json({
                        "error": "Not found",
                        "response": null
                    });
                } else {
                    result.update({ hidden: 1 }).then(() => {
                        res.status(201).json({
                            "response": "Hidded",
                            "error": null
                        });
                    });
                }
            });
        }
    }
}