
const { Professor, City, State, Photo } = require("../../sequelize");

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../helpers/configs");
const roles = require("../helpers/roles");

const transporter = require('../helpers/transporter');

const redis = require("redis");

exports.Register = function (req, res, next) {
    let data = {
        name: null,
        birthday: null,
        phone: null,
        email: null,
        password: null,
        CityId: null,
        Role: roles.professor
    }

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    if (data.name === null || data.email === null || data.password === null || data.password.length < 6 || data.CityId === null) {
        res.status(406).json({
            "response": null,
            "error": "Invalid params!"
        });
    } else {
        for (let key in data) {
            if (data[key] === null) {
                delete data[key];
            }
        }

        bcrypt.hash(data.password, 10, (err, hash) => {
            if (err) {
                res.status(406).json({
                    "response": null,
                    "error": err
                })
            } else {
                data.password = hash;

                Professor.create(data).then(result => {
                    let token = jwt.sign(
                        {
                            email: result.email,
                            idProfessor: result.id,
                            name: result.name,
                            role: result.role
                        },
                        config.secret,
                        { expiresIn: "3h" }
                    );
                    res.status(201).json(
                        {
                            "response": "Professor registered",
                            "data": result.get(),
                            "token": token
                        }
                    );
                }).catch(err => {
                    res.status(406).json(
                        {
                            "response": "Professor not registered",
                            "error": err
                        }
                    )
                });
            }
        });
    }
}

exports.Login = function (req, res, next) {
    let email = req.body.emailOrUsername;
    let password = req.body.password;

    if (email === undefined || password === undefined || password.length < 6) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid params!'
        });
    } else {
        Professor.findOne({
            include: [
                { all: true },
                { model: City, include: [State] },
                { model: Photo },
            ],
            where: {
                email: email,
                activated: 1
            }
        }).then(result => {
            if (!result) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "Professor not found"
                    }
                )
            }
            else {
                bcrypt.compare(password, result.get().password, function (err, authenticated) {
                    if (err) {
                        res.status(406).json(
                            {
                                "response": null,
                                "error": err
                            }
                        )
                    } else {
                        if (!authenticated) {
                            res.status(401).json({
                                'error': null,
                                'response': {
                                    'auth': false,
                                    'message': 'Not authenticated!'
                                }
                            });
                        } else {
                            let token = jwt.sign({
                                email: result.email,
                                idProfessor: result.id,
                                name: result.name,
                                role: result.role
                            }, config.secret, { expiresIn: "30d" });

                            res.status(201).json({
                                "error": null,
                                "response": {
                                    "auth": true,
                                    "message": "Authenticated!",
                                    "token": token,
                                    "data": {
                                        id: result.id,
                                        name: result.name,
                                        email: result.email,
                                        role: result.role,
                                        birthday: result.birthday,
                                        semester: result.semester,
                                        phone: result.phone,
                                        city: result.City,
                                        photo: result.Photo
                                    }
                                }
                            })
                        }
                    }
                });
            }
        }).catch(err => {
            res.status(406).json(
                {
                    "response": null,
                    "error": "error to list user infos: " + err
                }
            )
        });
    }
}

exports.ForgotPassword = function (req, res, next) {

    let email = req.body.email;

    if (email == undefined) {
        res.status(406).json(
            {
                "error": "Invalid params!",
                "response": null
            }
        )
    } else {

        Professor.findOne({
            where: {
                email: email
            }
        }).then((value) => {
            if (!value) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "Professor not found"
                    }
                );
            } else {

                let code = Math.floor(100000 + Math.random() * 900000);

                let conn = redis.createClient('redis://redis-11872.c74.us-east-1-4.ec2.cloud.redislabs.com:11872');
                conn.auth("Fatec2019#", (err, reply) => {
                    if (err) {
                        conn.end(true);
                        res.status(500).json(
                            {
                                "response": null,
                                "error": err
                            }
                        );
                    } else {

                        conn.setex(`professor:${value.get().id}`, 1200, code, (err, reply) => {
                            
                            conn.end(true);

                            if (err) {
                                res.status(406).json(
                                    {
                                        "response": null,
                                        "error": err
                                    }
                                )
                            } else {

                                conteudo_email = `Oi ${value.name},<br><br>
                                Esqueceu sua senha? Tudo bem, não nos esquecemos de você! Copie o código para redefinir sua senha.<br><br>
                                Código: ${code} <br><br>

                                <b>Lembrando que o código para alterar senha funcionará por apenas 20 minutos! Grato.</b>
                                `;

                                var mailOptions = {
                                    from: 'octopusprojectoficial@gmail.com',
                                    to: email,
                                    subject: 'Recuperação de Conta',
                                    html: conteudo_email
                                };

                                transporter.sendMail(mailOptions, function (error, info) {
                                    if (error) {
                                        res.status(500).json(
                                            {
                                            'response': null,
                                            'error': error
                                            }
                                        );
                                    } else {
                                        res.status(200).json(
                                            {
                                                'error': null,
                                                'response': {
                                                    'message': 'Success!',
                                                    'info': info
                                                }
                                            }
                                        );
                                    }
                                });

                            }

                        });
                    }
                });
            }
        })
        .catch((err) => {
            if (err) {
                res.status(500).json(
                    {
                        "response": null,
                        "error": err
                    }
                )
            }
        });

    }

}

exports.CheckCode = function(req, res, next) {

    let email = req.body.email;
    let code = req.body.code;

    if (email == undefined || code == undefined) {
        res.status(406).json(
            {
                "error": "Invalid params!",
                "response": null
            }
        )
    } else {

        Professor.findOne({
            where: {
                email: email
            }
        }).then((value) => {
            if (!value) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "Professor not found"
                    }
                );
            } else {

                let conn = redis.createClient('redis://redis-11872.c74.us-east-1-4.ec2.cloud.redislabs.com:11872');
                conn.auth("Fatec2019#", (err, reply) => {
                    if (err) {
                        conn.end(true);
                        res.status(500).json(
                            {
                                "response": null,
                                "error": err
                            }
                        );
                    } else {

                        conn.get(`professor:${value.get().id}`, (err, reply) => {
                            
                            conn.end(true);
                            
                            if (err) {
                                res.status(500).json(
                                    {
                                        "response": null,
                                        "error": err
                                    }
                                );
                            } else {

                                if (!reply) {
                                    res.status(404).json(
                                        {
                                            "response": null,
                                            "error": "Not found code!"
                                        }
                                    );
                                } else {

                                    res.status(200).json(
                                        {
                                            "error": null,
                                            "response": {
                                                "authorize": reply == code.toString()
                                            }
                                        }
                                    )

                                }

                            }
                        });
                    }
                });
            }
        });

    }

}

exports.ChangePasswordWithCode = function(req, res, next) {

    let email = req.body.email;
    let code = req.body.code;
    let password = req.body.password;

    if (email == undefined || code == undefined || password == undefined || password.length < 6) {
        res.status(406).json(
            {
                "error": "Invalid params!",
                "response": null
            }
        )
    } else {

        Professor.findOne({
            where: {
                email: email
            }
        }).then((value) => {
            if (!value) {
                res.status(404).json(
                    {
                        "response": null,
                        "error": "Professor not found"
                    }
                );
            } else {

                let conn = redis.createClient('redis://redis-11872.c74.us-east-1-4.ec2.cloud.redislabs.com:11872');
                conn.auth("Fatec2019#", (err, reply) => {
                    if (err) {
                        conn.end(true);
                        res.status(500).json(
                            {
                                "response": null,
                                "error": err
                            }
                        );
                    } else {

                        conn.get(`professor:${value.get().id}`, (err, reply) => {
                            
                            conn.end(true);
                            
                            if (err) {
                                res.status(500).json(
                                    {
                                        "response": null,
                                        "error": err
                                    }
                                );
                            } else {

                                if (!reply) {
                                    res.status(404).json(
                                        {
                                            "response": null,
                                            "error": "Not found code!"
                                        }
                                    );
                                } else {

                                    if (reply != code.toString()) {
                                        res.status(406).json(
                                            {
                                                "error": null,
                                                "response": {
                                                    "authorize": false
                                                },
                                            }
                                        );
                                    } else {
                                        bcrypt.hash(password, 10, (err, hash) => {
                                            if (err) {
                                                res.status(406).json({
                                                    "response": null,
                                                    "error": err
                                                })
                                            } else {
                                                Professor.update(
                                                    {
                                                        password: hash
                                                    },
                                                    {
                                                        where: {
                                                            email: email
                                                        }
                                                    }
                                                ).then((value) => {
                                                    res.status(200).json(
                                                        {
                                                            "error": null,
                                                            "response": {
                                                                "message": "Updated",
                                                                "value": value
                                                            }
                                                        }
                                                    );
                                                }).catch((err) => {
                                                    res.status(200).json(
                                                        {
                                                            "response": null,
                                                            "error": err
                                                        }
                                                    );
                                                });
                                            }
                                        });
                                    }

                                }

                            }
                        });
                    }
                });
            }
        });

    }

}

exports.ChangePassword = function (req, res, next) {
    let password = req.body.password;
    let newPassword = req.body.newPassword;
    let token = req.body.token;

    if (token === undefined || newPassword === undefined || password === undefined || password.length < 6) {
        res.status(406).json({
            'response': null,
            'error': 'Invalid Params!'
        });
    } else {
        tokenDecoded = jwt.verify(token, config.secret);

        let idProfessor = tokenDecoded["idProfessor"];

        Professor.findByPk(idProfessor).then(result => {
            if (!result) {
                res.status(404).json({
                    'response': null,
                    'error': 'Professor not found'
                });
            } else {
                bcrypt.compare(password, result.password, function (err, equals) {
                    if (err) {
                        res.status(406).json({
                            'response': null,
                            'error': err
                        });
                    } else {
                        if (!equals) {
                            res.status(406).json({
                                'response': null,
                                'error': 'Invalid password!'
                            });
                        } else {
                            bcrypt.hash(newPassword, 10, function (err, newPasswordHash) {
                                if (err) {
                                    res.status(406).json({
                                        'response': null,
                                        'error': err
                                    });
                                } else {
                                    result.update({ password: newPasswordHash }).then(professorUpdated => {
                                        res.status(200).json({
                                            'response': {
                                                "message": "Password updated",
                                                "data": professorUpdated.get()
                                            },
                                            'error': null
                                        })
                                    });
                                }
                            });
                        }
                    }
                });
            }
        }).catch(err => {
            res.status(406).json(
                {
                    "response": null,
                    "error": "error to change the password: " + err
                }
            )
        })
    }

}

exports.EditPefil = function (req, res, next) {
    let data = {
        name: null,
        birthday: null,
        semester: null,
        phone: null,
        email: null,
        CityId: null,
    }

    let token = req.body.token;
    
    if (token === undefined) {
        res.status(406).json({
            "response":null,
            "error": "Invalid token"
        })
    }

    let tokenDecoded = jwt.verify(token, config.secret);

    let idProfessor = tokenDecoded["idProfessor"];

    for (let key in req.body) {
        if (data.hasOwnProperty(key)) {
            data[key] = req.body[key];
        }
    }

    for (let key in data) {
        if (data[key] === null) {
            delete data[key];
        }
    }

    if (data.name === undefined && data.birthday === undefined && data.semester === undefined && data.phone === undefined && data.email === undefined && data.CityId === undefined) {
        res.status(201).json({
            "response": "Everything is up to date",
            "error": null
        });
    } else {
        Professor.findByPk(idProfessor).then(result => {
            if (!result) {
                res.status(414).json({
                    'response': null,
                    'error': "Professor perfil not found"
                });
            } else {
                result.update(data).then(professorUpdated => {
                    res.status(200).json({
                        'response': {
                            "message": "Perfil updated",
                            "data": {
                                name: professorUpdated.name,
                                birthday: professorUpdated.birthday,
                                semester: professorUpdated.semester,
                                phone: professorUpdated.phone,
                                email: professorUpdated.email,
                                CityId: professorUpdated.CityId,
                            }
                        },
                        'error': null
                    })
                });
            }
        }).catch(err => {
            res.status(406).json({
                'response': null,
                'error': err
            });
        })
    }

}

exports.EditPhoto = function(req, res, next) {

    let token = req.body.token;
    let idPhoto = req.body.idPhoto;

    if (token == undefined) {
        res.status(406).json(
            {
                "response": null,
                "error": "Invalid params!"
            }
        );
    } else {

        let tokenDecoded = jwt.verify(token, config.secret);

        Professor.update(
            {
                PhotoId: idPhoto
            },
            {
                where: {
                    id: tokenDecoded.idProfessor
                }
            }
        ).then((value) => {
            res.status(200).json(
                {
                    "error": null,
                    "response": value
                }
            )
        }).catch((err) => {
            res.status(500).json(
                {
                    "response": null,
                    "error": err
                }
            );
        });

    }

}