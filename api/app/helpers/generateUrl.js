exports.generateUrl = function(string, callback) {
    url = string.replace(/[^\w]/gi, ' ').split(" ").join("-").toLowerCase()

    while (url.includes("--")) {
        url = url.split("--").join("-");
    }

    if (url[url.length - 1] == "-") {
        url = url.slice(0, url.length - 1)
    }

    if (url[0] == "-") {
        url = url.slice(1, url.length)
    }

    callback(url)
}