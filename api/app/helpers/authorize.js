const expressJwt = require('express-jwt');
const secret = require('../_helpers/config').secret;

function authorize(roles = []){
    if(typeof roles === 'string'){
        roles = [roles];
    }
    return [
        expressJwt({secret}),
        (req, res, next) => {
            if(roles.length && !roles.includes(req.user.role)){
                return res.status(401).json({message: 'Unauthorized'});
            }
            if(req.params.userId.toString() != req.user.userId.toString()){
                return res.status(401).json({message: 'Unauthorized'});
            }
            next();
        }
    ];
}


module.exports = authorize;
