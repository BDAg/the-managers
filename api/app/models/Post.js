module.exports = (Types) => {
    return {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            field: "idPost"
        },
        description: Types.STRING,
    }
}