module.exports = (Types) => {
    return {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            allowNull: false,
            field: "idProject"
        },
        name: {
            allowNull: false,
            type: Types.STRING
        },
        description: {
            allowNull: false,
            type: Types.STRING
        },
        url: {
            allowNull: false,
            type: Types.STRING,
            unique: true
        },
        gitlab: {
            allowNull: false,
            type: Types.STRING
        },
        expireAt: {
            allowNull: false,
            type: Types.DATE
        }
    }
}