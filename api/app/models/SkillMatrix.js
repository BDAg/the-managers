module.exports = (Types) => {
    return {
        level: {
            defaultValue: 1,
            type: Types.INTEGER
        }
    }
}