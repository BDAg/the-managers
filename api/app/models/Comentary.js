module.exports = (Types) => {
    return {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            field: "idComentary"
        },
        comment: {
            type: Types.STRING,
            allowNull: false
        }
    };
}