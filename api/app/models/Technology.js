module.exports = (Types) => {
    return {
        id: {
            allowNull: false,
            type: Types.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            field: "idTechnology"
        },
        name: {
            type: Types.STRING,
            allowNull: false
        },
    }
}