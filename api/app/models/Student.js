module.exports = (Types) => {
    return {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            field: "idStudent"
        },
        name: {
            allowNull: false,
            type: Types.STRING
        },
        birthday: Types.DATE,
        semester: {
            allowNull: false,
            type: Types.INTEGER
        },
        phone: Types.STRING,
        email: {
            allowNull: false,
            type: Types.STRING,
            unique: true,
            validator: {
                isEmail: true
            }
        },
        role: {
            type: Types.INTEGER(1),
            defaultValue: 1
        },
        password: {
            allowNull: false,
            type: Types.STRING
        },
    };
}