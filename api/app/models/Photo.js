module.exports = (Types) => {
    return {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            allowNull: false,
            field: "idPhoto"
        },
        path: {
            allowNull: false,
            type: Types.STRING
        },
        width: {
            type: Types.DOUBLE,
            allowNull: false
        },
        heigth: {
            type: Types.DOUBLE,
            allowNull: false
        }
    }
}