module.exports = (Types) => {
    return {
        isMember: {
            defaultValue: 0,
            type: Types.TINYINT,
            allowNull: false
        },
        job: {
            defaultValue: 1,
            type: Types.INTEGER(1),
            allowNull: false
        },
        invite: {
            defaultValue: 0,
            type: Types.TINYINT,
            allowNull: false
        }
    }
}