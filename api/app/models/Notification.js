module.exports = (Types) => {
    return {
        id: {
            allowNull: false,
            autoIncrement: true,
            type: Types.INTEGER(11),
            primaryKey: true,
            field: 'idNotification'
        },
        message: {
            allowNull: false,
            type: Types.STRING(1000)
        },
        hidden: {
            allowNull: false,
            defaultValue: 0,
            type: Types.TINYINT
        },
        type: {
            allowNull: false,
            type: Types.TINYINT
        }
    }
}