module.exports = (Types) => {
    return {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            field: "idProfessor"
        },
        name: {
            allowNull: false,
            type: Types.STRING
        },
        birthday: Types.DATE,
        phone: Types.STRING,
        email: {
            allowNull: false,
            type: Types.STRING,
            unique: true,
            validator: {
                isEmail: true
            }
        },
        role: {
            type: Types.INTEGER(1),
            defaultValue: 2
        },
        activated: {
            defaultValue: 0,
            type: Types.TINYINT,
            allowNull: false
        },
        password: {
            allowNull: false,
            type: Types.STRING
        },
    }
}