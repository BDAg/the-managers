module.exports = (Types) => {
    return {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            allowNull: false,
            field: "idTopicCronograma"
        },
        description: {
            allowNull: false,
            type: Types.STRING
        },
        semester: {
            allowNull: false,
            type: Types.INTEGER
        }
    }
}