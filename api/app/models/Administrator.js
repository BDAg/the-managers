module.exports = (Types) => {
    return  {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            field: "idAdmin"
        },
        name: {
            allowNull: false,
            type: Types.STRING
        },
        email: {
            allowNull: false,
            type: Types.STRING,
            unique: true,
            validator: {
                isEmail: true
            }
        },
        password: {
            allowNull: false,
            type: Types.STRING
        },
        role: {
            type: Types.INTEGER(1),
            defaultValue: 3
        },
    };
}