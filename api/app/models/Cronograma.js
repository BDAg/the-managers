module.exports = (Types) => {
    return {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            allowNull: false,
            field: "idCronograma"
        },
        description: {
            allowNull: false,
            type: Types.STRING
        },
        semester: {
            allowNull: false,
            type: Types.INTEGER
        },
        startsAt: {
            type: Types.DATE
        },
        expireAt: {
            type: Types.DATE
        }
    }
}