module.exports = (Types) => {
    return {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Types.INTEGER,
            allowNull: false,
            field: "idTutorial"
        },
        title: {
            allowNull: false,
            type: Types.STRING
        },
        video: {
            type: Types.STRING
        },
        description: {
            allowNull: false,
            type: Types.STRING
        },
        content: {
            type: Types.TEXT("LONG")
        },
        url: {
            allowNull: false,
            type: Types.STRING
        }
    }
}