'use strict';

const Sequelize = require('sequelize');;
const config = require('./config/database');;
const Model = Sequelize.Model;

const AdministratorModel = require('./app/models/Administrator');
const StateModel = require('./app/models/State');
const CityModel = require('./app/models/City');
const PhotoModel = require('./app/models/Photo');
const TechnologyModel = require('./app/models/Technology');
const StudentModel = require('./app/models/Student');
const SkillMatrixModel = require('./app/models/SkillMatrix');
const ProjectModel = require('./app/models/Project');
const MemberModel = require('./app/models/Member');
const PostModel = require('./app/models/Post');
const ProfessorModel = require('./app/models/Professor');
const Project_has_TechnologyModel = require('./app/models/Project_has_Technology');
const ComentaryModel = require('./app/models/Comentary');
const LikeModel = require('./app/models/Like');
const TutorialModel = require('./app/models/Tutorial');
const NotificationModel = require('./app/models/Notification');
const CronogramaModel = require('./app/models/Cronograma');
const TopicCronogramaModel = require('./app/models/TopicCronograma');
const sequelize = new Sequelize(config.database, config.username, config.password, config);

sequelize.authenticate().then(() => {
  console.log("Connected to Database")
}).catch(err => {
  console.log("Error to connect to the database: " + err)
});

class State extends Model { };
State.init(StateModel(Sequelize), {
  sequelize,
  modelName: "States"
});

class City extends Model { }
City.init(CityModel(Sequelize), {
  sequelize,
  modelName: "Cities"
});

class Student extends Model { }
Student.init(StudentModel(Sequelize), {
  sequelize,
  modelName: "Students",
});

class Project extends Model { };
Project.init(ProjectModel(Sequelize), {
  sequelize,
  modelName: "Projects"
});

class SkillMatrix extends Model { };
SkillMatrix.init(SkillMatrixModel(Sequelize), {
  sequelize,
  modelName: "SkillsMatrices"
});

class Technology extends Model { };
Technology.init( TechnologyModel(Sequelize), {
  sequelize,
  modelName: "Technologies"
});

class Project_has_Technology extends Model { };
Project_has_Technology.init(Project_has_TechnologyModel(), {
  sequelize,
  modelName: "Projects_has_Technologies"
});

class Photo extends Model { };
Photo.init(PhotoModel(Sequelize), {
  sequelize,
  modelName: "Photos"
});

class Professor extends Model { };
Professor.init(ProfessorModel(Sequelize), {
  sequelize,
  modelName: "Professors"
});

class Member extends Model { };
Member.init(MemberModel(Sequelize), {
  sequelize,
  modelName: "Members"
});

class Like extends Model { };
Like.init(LikeModel(Sequelize), {
  sequelize,
  modelName: "Likes"
});

class Post extends Model { };
Post.init(PostModel(Sequelize), {
  sequelize,
  modelName: "Posts"
});

class Comentary extends Model { };
Comentary.init(ComentaryModel(Sequelize), {
  sequelize,
  modelName: "Comentaries"
});

class Administrator extends Model { };
Administrator.init(AdministratorModel(Sequelize), {
  sequelize,
  modelName: "Administrators"
});

class Tutorial extends Model { };
Tutorial.init(TutorialModel(Sequelize), {
  sequelize,
  modelName: "Tutorials"
});

class Notification extends Model{ };
Notification.init(NotificationModel(Sequelize), {
  sequelize,
  modelName: 'Notifications'
})

class Cronograma extends Model { };
Cronograma.init(CronogramaModel(Sequelize), {
  sequelize,
  modelName: 'Cronogramas'
});

class TopicCronograma extends Model { };
TopicCronograma.init(TopicCronogramaModel(Sequelize), {
  sequelize,
  modelName: 'TopicsCronograma'
});

//1:N States x Cities
City.belongsTo(State);

//N:N Students x Technologies
Student.belongsToMany(Technology, { through: 'SkillsMatrices' });
SkillMatrix.belongsTo(Student);
SkillMatrix.belongsTo(Technology);
Technology.belongsToMany(Student, { through: 'SkillsMatrices', });

//N:N Project x Technologies
Project.belongsToMany(Technology, { through: "Projects_has_Technologies" });
Project_has_Technology.belongsTo(Project);
Project_has_Technology.belongsTo(Technology);
Technology.belongsToMany(Project, { through: "Projects_has_Technologies" });


//N:N Students x Projects
Student.belongsToMany(Project, { through: "Members" });
Member.belongsTo(Student);
Member.belongsTo(Project);
Project.belongsToMany(Student, { through: "Members" });

//1:N Cities x Students && Professor
Student.belongsTo(City);
Professor.belongsTo(City);

//1:1 Post x Project && Student && Professor
Post.belongsTo(Student);
Post.belongsTo(Professor);
Post.belongsTo(Project);

//1:1 Photo x Post && Student && Professor
Post.belongsTo(Photo);
Student.belongsTo(Photo);
Professor.belongsTo(Photo);

//1:N Like x Post
Post.hasMany(Like);

//1:1 Like x Post && Student && Professor
Like.belongsTo(Student);
Like.belongsTo(Professor);

//1:N Comentary x Post
Post.hasMany(Comentary);

//1:1 Comentary x Post && Student && Professor
Comentary.belongsTo(Student);
Comentary.belongsTo(Professor);

//1:N Tutorial x Admnistrator
Tutorial.belongsTo(Administrator);

//1:N Notification x Posts && Projects && Student
Post.hasMany(Notification);
Project.hasMany(Notification);
Student.hasMany(Notification);

//N:1 Cronograma && TopicCronograma x Professors
Cronograma.belongsTo(Professor);
TopicCronograma.belongsTo(Professor);

//1:N TopicCronograma x Cronograma
TopicCronograma.hasMany(Cronograma, { onDelete: "CASCADE" });

sequelize.sync().then(() => {
  console.log("Synchronized to Database")
}).catch(err => {
  console.log("Error to sync the database: " + err)
});

module.exports = {
  Student,
  City,
  State,
  Photo,
  Administrator,
  Technology,
  SkillMatrix,
  Project,
  Project_has_Technology,
  Member,
  Professor,
  Post,
  Like,
  Comentary,
  Tutorial,
  Notification,
  Cronograma,
  TopicCronograma,
  sequelize
};