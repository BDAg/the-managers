const router = require("express").Router()
const studentController = require("../app/controllers/StudentC")

router.post("/register", studentController.Register);
router.post("/login", studentController.Login);
router.get("/list/:email", studentController.GetOneStundentByEmail);
router.get("/get-all", studentController.GetAllStudents);
router.post("/forgot-password", studentController.ForgotPassword);
router.post("/check-code", studentController.CheckCode);
router.post("/change-password-code", studentController.ChangePasswordWithCode);
router.post("/change-password", studentController.ChangePassword);
router.post("/edit-perfil", studentController.EditPefil);
router.post("/edit-photo", studentController.EditPhoto);
router.post("/send-solicitation", studentController.sendProjectSolicitation);
router.post("/accept-invite", studentController.acceptInvite);
router.post("/deny-invite", studentController.DenyInvite);
router.get("/list-students-without-project", studentController.ListStudentsWithoutProject);

module.exports = router;