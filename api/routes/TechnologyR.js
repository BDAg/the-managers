const router = require("express").Router();

const technologyController = require("../app/controllers/TechnologiesC")

router.get("/list", technologyController.List);

module.exports = router;