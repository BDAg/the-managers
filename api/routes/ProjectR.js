const router = require("express").Router();

const projectController = require("../app/controllers/ProjectC")

router.post("/create", projectController.Create);
router.post("/edit", projectController.Edit);
router.get("/list", projectController.List);
router.get("/view-perfil/:url", projectController.ViewPerfil);

router.post("/invite-member", projectController.InviteMember);
router.post("/cancel-invite", projectController.CancelInvite);
router.post("/list-invite/:invite", projectController.ListInvites);

router.post("/deny-solicitation", projectController.DenySolicitation);
router.post("/agree-solicitation", projectController.AgreeSolicitation);

router.post("/add-technology", projectController.addTechnology);
router.post("/remove-technology", projectController.removeTechnology);

router.post("/remove-member", projectController.RemoveMember);
router.post("/edit-member-job", projectController.EditMemberJob);

module.exports = router;