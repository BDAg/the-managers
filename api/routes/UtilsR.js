const router = require("express").Router();

const stateController = require("../app/controllers/StateC");
const cityController = require("../app/controllers/CityC");

router.get("/states", stateController.list);

router.get("/cities", cityController.list);
router.get("/cities/:idState", cityController.listWithState)

module.exports = router;