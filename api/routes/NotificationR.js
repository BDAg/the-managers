const router = require("express").Router();
const NotificationController = require("../app/controllers/NotificationC");

router.get("/list", NotificationController.ListUserNotifications);
router.post("/hide", NotificationController.HideNotifications);

module.exports = router;
