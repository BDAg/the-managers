const router = require("express").Router();
const postController = require("../app/controllers/PostC");
const likeController = require("../app/controllers/LikeC");
const commentController = require("../app/controllers/CommentC");

router.post("/create", postController.Create);
router.post("/delete", postController.Delete);
router.get("/list", postController.List);
router.post("/edit", postController.Edit);

router.post("/like", likeController.Like);
router.post("/unlike", likeController.Unlike);

router.post("/comment", commentController.Comment);
router.post("/delete-comment", commentController.DeleteComment);
router.post("/edit-comment", commentController.EditComment);

module.exports = router;
