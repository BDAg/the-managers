const router = require("express").Router();

const tutorialController = require("../app/controllers/TutorialC")

router.post("/publish", tutorialController.Publish);
router.get("/list/:url", tutorialController.getOne);
router.get("/list", tutorialController.List);

module.exports = router;