const router = require("express").Router();

const cronogramaController = require("../app/controllers/CronogramaC")

router.post("/create", cronogramaController.Create);
router.post("/create-topic", cronogramaController.CreateTopic);
router.get("/:semester", cronogramaController.List);
router.post("/delete", cronogramaController.DeleteItem);
router.post("/delete-topic", cronogramaController.DeleteTopic);
router.post("/edit", cronogramaController.EditItem);
router.post("/edit-topic", cronogramaController.EditTopic);

module.exports = router;