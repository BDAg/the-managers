const router = require("express").Router();
const adminController = require("../app/controllers/AdminC");

router.post("/create", adminController.Create);
router.post("/login", adminController.Login);

module.exports = router;
