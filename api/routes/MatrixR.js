const router = require("express").Router();
const matrixController = require("../app/controllers/MatrixC");

router.post("/add", matrixController.addTechnology);
router.post("/remove", matrixController.removeTechnology);
router.post("/edit", matrixController.editTechnology);

module.exports = router;
