const router = require("express").Router();
const professorController = require("../app/controllers/ProfessorC");

router.post("/register", professorController.Register);
router.post("/login", professorController.Login);
router.post("/forgot-password", professorController.ForgotPassword);
router.post("/check-code", professorController.CheckCode);
router.post("/change-password-code", professorController.ChangePasswordWithCode);
router.post("/change-password", professorController.ChangePassword);
router.post("/edit-perfil", professorController.EditPefil);
router.post("/edit-photo", professorController.EditPhoto);

module.exports = router;