const router = require("express").Router();
const photoController = require("../app/controllers/PhotoC");

router.post("/upload", photoController.Upload);
router.post("/delete", photoController.Delete);

module.exports = router;
