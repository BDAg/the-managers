module.exports = {
    username: 'root',
    password: 'admin123',
    database: 'pimanager',
    host: '127.0.0.1',
    dialect: 'mysql',
    operatorAliases: false,
    logging: false,
    define: {
      timestamps: true,
      underscored: false,
    }
  }