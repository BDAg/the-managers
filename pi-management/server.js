const express = require('express');

const app = express();

const baseDir = `${__dirname}/build/`;

app.use(express.static(`${baseDir}`))

app.get('*', (req,res) => res.sendFile('index.html' , { root : baseDir }))

const port = 3001;

app.listen(port, '0.0.0.0', () => console.log(`Servidor subiu com sucesso em http://localhost:${port}`));