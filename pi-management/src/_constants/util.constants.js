export const utilConstants = {
    REQUEST: 'UTILS_REQUEST',
    SUCCESS: 'UTILS_SUCCESS',
    FAILURE: 'UTILS_FAILURE',
};
