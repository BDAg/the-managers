import axios from 'axios';
import { toast } from '../_helpers';
import { apiUrl } from '../_helpers/api-url';
// const url = "http://35.231.220.66:3000/admin";
const url = apiUrl+"/admin"

export const AdminServices = {
    login
}

async function login(email, password){
    axios({method: "POST", url: `${url}/login`, data: {
        "password": password,
        "email": email
    }}).then(response => {
        if(response.status === 200){
            localStorage.setItem("admin", JSON.stringify(response.data.response));
            return toast("Logado com sucesso, redirecionando", null, ()=>{
                return window.location.reload();
            });
        } else {
            return toast("Erro ao logar, verique seus dados e tente novamente", "red");
        }
    }).catch(err => {
        return toast("Erro ao logar, verique seus dados e tente novamente", "red");
    });
}