import axios from 'axios';
import { toast } from '../_helpers';
import { apiUrl } from '../_helpers/api-url';
// const url = "http://35.231.220.66:3000/tutorial";

const url = apiUrl+"/tutorial";

export const tutorialServices = {
    publish,
    list,
    view
}

async function publish(data) {
    axios({ method: "POST", url: `${url}/publish`, data: data }).then(response => {
        if (response.status === 201) {
            return toast("Tutorial criado com sucesso");
        } else {
            return toast("Erro ao postar, tente novamente", "red");
        }
    }).catch(err => {
        return toast("Erro ao postar, tente novamente", "red");
    });
}

async function list() {
    return new Promise((resolve, reject) => {
        axios({ method: "GET", url: `${url}/list` }).then(response => {
            if (response.status === 200) {
                resolve(response.data.response);
            } else {
                toast("Erro ao carregar tutoriais, tente novamente mais tarde", "red");
                throw new Error(response.data.error);
            }
        }).catch(err => {
            toast("Erro ao carregar tutoriais, tente novamente mais tarde", "red");
            reject(err)
        });
    });
}

async function view(urlProject) {
    return new Promise((resolve, reject) => {
        axios({ method: "GET", url: `${url}/list/${urlProject}` }).then(response => {
            if (response.status === 200) {
                resolve(response.data.response);
            } else {
                toast("Erro ao carregar tutorial, tente novamente mais tarde", "red");
                throw new Error(response.data.error);
            }
        }).catch(err => {
            switch (err.message) {
                case ("Request failed with status code 404"):
                    window.location.href = "/not-found";
                    break;
                default:
                    toast("Erro ao carregar tutorial, tente novamente mais tarde", "red");
                    break;
            }
            reject(err)
        });
    });
}