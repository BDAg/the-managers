import axios from 'axios';
import { toast } from '../_helpers';
import { apiUrl } from '../_helpers/api-url';
// const url = "http://35.231.220.66:3000/cronograma";
const url = apiUrl+"/cronograma";

export const cronogramaServices = {
    listar,
    criarCronograma,
    criarTopico,
    deletarTopico,
    deletarItem,
    editItem,
    editTopic
}

async function listar(semester) {
    return new Promise((resolve, reject) => {
        return axios({ method: "GET", url: `${url}/${semester}` }).then(response => {
            if (response.status === 200) {
                resolve(response.data.response);
            } else {
                return toast("Erro ao listar o cronograma", "red");
            }
        }).catch(err => {
            reject(err)
        });
    })
}

async function criarCronograma(data) {
    return new Promise((resolve, reject) => {
        return axios({ method: "POST", url: `${url}/create`, data: data }).then(response => {
            if (response.status === 201) {
                resolve(response);
            } else {
                toast("Ocorreu um erro ao adicionar item", "red")

                throw new Error("Cronograma não criado com status " + response.status);
            }
        }).catch(err => {
            reject(err);
        })
    })
}
async function criarTopico(data) {
    return new Promise((resolve, reject) => {
        return axios({ method: "POST", url: `${url}/create-topic`, data: data }).then(response => {
            if (response.status === 201) {
                resolve(response);
            } else {
                toast("Ocorreu um erro ao adicionar tópico", "red")

                throw new Error("Cronograma não criado com status " + response.status);
            }
        }).catch(err => {
            reject(err);
        })
    })
}

async function deletarTopico(topicId, token) {
    return new Promise((resolve, reject) => {
        return axios({ method: "POST", url: `${url}/delete-topic`, data: {itemId: topicId, token: token} }).then(response => {
            if (response.status === 200) {
                resolve(response);
            } else {
                toast("Ocorreu um erro ao deletar tópico", "red")

                throw new Error("Tópico não deletado com status " + response.status);
            }
        }).catch(err => {
            reject(err);
        })
    })
}

async function deletarItem(itemId, token) {
    return new Promise((resolve, reject) => {
        return axios({ method: "POST", url: `${url}/delete`, data: {itemId: itemId, token: token} }).then(response => {
            if (response.status === 200) {
                resolve(response);
            } else {
                toast("Ocorreu um erro ao deletar item", "red")

                throw new Error("Item não deletado com status " + response.status);
            }
        }).catch(err => {
            reject(err);
        })
    })
}

async function editItem(data) {
    return new Promise((resolve, reject) => {
        return axios({ method: "POST", url: `${url}/edit`, data: data }).then(response => {
            switch(response.status) {
                case 200:
                    resolve(response);
                    break;
                case 304:
                    toast("Você não alterou nenhum parâmetro!");
                    break;
                default:
                    toast("Ocorreu um erro ao editar item", "red");
                    throw new Error("Item não editado com status " + response.status);
            }
        }).catch(err => {
            reject(err);
        })
    })
}

async function editTopic(itemId, token, description) {
    return new Promise((resolve, reject) => {
        return axios({ method: "POST", url: `${url}/edit-topic`, data: {itemId: itemId, token: token, description: description} }).then(response => {
            switch(response.status) {
                case 200:
                    resolve(response);
                    break;
                case 304:
                    toast("Você não alterou nenhum parâmetro!");
                    break;
                default:
                    toast("Ocorreu um erro ao editar tópico", "red");
                    throw new Error("Item não editado com status " + response.status);
            }
        }).catch(err => {
            reject(err);
        })
    })
}