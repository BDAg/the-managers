import { authHeader, toast } from '../_helpers';
import axios from 'axios';
import { history } from '../_helpers';
import { projectServices } from './project.service';
import { cronogramaServices } from './cronograma.service';
import { apiUrl } from '../_helpers/api-url';

// const url = "http://35.231.220.66:3000/student";
// const urlPhoto = "http://35.231.220.66:3000/photo";
// const urlNotification = 'http://35.231.220.66:3000/notification';
// const urlMatrix = 'http://35.231.220.66:3000/matrix'

const url = apiUrl+"/student";
const urlPhoto = apiUrl+"/photo";
const urlNotification = apiUrl+"/notification";
const urlMatrix = apiUrl+"/matrix";

console.log(url);

export const studentServices = {
    login,
    viewStudentPerfil,
    logout,
    register,
    getAll,
    listUsersWithoutProjects,
    updateUser,
    changePassword,
    changePhoto,
    denyInvite,
    acceptInvite,
    sendSolicitation,
    forgotPassowrd,
    changePasswordWithCode,
    getAllStudents,
    addTechnology,
    removeTechnology,
    editTechnology,
    hideNotification,
    delete: _delete,
    header: authHeader()
    // getById,
};

async function login(email, senha) {
    let res;
    try {

        res = await axios.post(url + "/login", { "emailOrUsername": email, "password": senha });

        if (res.data.error) {
            return { "response": { "message": res.data.error } };
        }
        else {
            if (res.status === 201) {
                let user = res.data.response;

                localStorage.setItem('user', JSON.stringify(user));
                await projectServices.getAll();
                await cronogramaServices.listar(user.data.semester).then(response => {
                    localStorage.setItem("cronograma", JSON.stringify(response));
                });
                return user;
            }
            else
                return { "error": "Unknown error!" }
        }
    } catch (e) {
        return { "response": { "message": "Not authenticated!" } };
    }
}

function logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('users');
    localStorage.removeItem("cronograma");
    history.push("/");
    window.location.reload();
}

async function getAll() {

    const requestOptions = {
        headers: authHeader()
    };

    let res = await axios.get(url + `/list`, requestOptions);
    if (res.data.error) {
        return { "response": { "message": res.data.error } };
    } else {
        localStorage.setItem('users', JSON.stringify(res.data.response));
        return await axios.get(url + `/list`, requestOptions);
    }
}


async function register(user, toast) {
    console.log(user);
    let response;
    try {
        response = await axios({ method: "POST", url: url + "/register", data: user });
        console.log(response);
        if (response.data.error) {
            return { "response": { "message": response.data.error } };
        } else {
            if (response.status === 201) {
                let user = response.data.response;
                toast("Cadastro realizado com sucesso!", "teal", () => {
                    window.location.href = '/login/aluno'
                });
                return user;
            } else {
                toast("Unknown error!", "red");
                return { "error": "Unknown error!" };
            }
        }
    } catch (e) {
        console.log(e);
        switch (e.response.data.error.name) {
            case "SequelizeUniqueConstraintError":
                return toast("Email já em uso", "red");
            default:
                return toast(e.message, "red");
        }
    }

}

async function viewStudentPerfil(email, toast) {
    let response;
    try {
        response = await axios({ method: "GET", url: `${url}/list/${email}` });

        if (response.status === 201) {
            let user = JSON.parse(localStorage.getItem("users") || "{}");
            user[email] = response.data.response;
            localStorage.setItem("users", JSON.stringify(user));

            return user[email];
        } else {
            if (response.status === 404) {
                return { "error": "Usuário não encontrado" }
            }
        }
    } catch (e) {
        switch (e.message) {
            case ("Request failed with status code 404"):
                window.location.href = "/not-found";
                break
            default:
                return toast(e, "red", () => {
                    history.goBack();
                });
        }
    }
}

async function getAllStudents() {
    let response;

    try {
        response = await axios({ method: "GET", url: `${url}/get-all` });

        if (response.status === 201) {
            console.log(response.data.response.data)
            localStorage.setItem("allUsers", JSON.stringify(response.data.response.data));
            return response.data.response;
        } else {
            return { "error": "Unknown error!" };
        }
    } catch (e) {
        return { "error": e };
    }
}

async function updateUser(data, token) {
    let response;
    data["token"] = token
    try {
        response = await axios({ method: "POST", url: `${url}/edit-perfil`, data: data });

        if (response.data.error) {
            return { "error": response.data.error };
        } else {
            if (response.status === 200) {
                let user = JSON.parse(localStorage.getItem("user"));
                for (let key in response.data.response.data) {
                    user.data[key] = response.data.response.data[key];
                }
                localStorage.setItem("user", JSON.stringify(user));

                return response.data.response;
            } else {
                return { "error": "Unknown error!" };
            }
        }

    } catch (e) {
        return { "error": e };
    }
}

async function _delete(id, header) {

    let res = await axios({ method: "POST", url: `${url}/remove/${id}`, headers: header });
    if (res.data.error) {
        return { "response": { "message": res.data.error } };
    } else {
        if (res.status === 201) {
            let resp = res.data.response;
            return resp;
        } else {
            return { "error": "Unknown error!" };
        }
    }
}

async function listUsersWithoutProjects() {
    let response = null;

    try {
        response = await axios({ method: "GET", url: `${url}/list-students-without-project` });

        if (response.data.error) {
            return { "response": { "message": response.data.error } };
        } else {
            if (response.status === 200) {
                localStorage.setItem("usersWProject", JSON.stringify(response.data.response));
            } else {
                return { "error": "Unknown error!" };
            }
        }
    } catch (err) {
        return { "error": err };
    }
}

async function changePassword(oldPassword, newPassword, token, toast) {
    let response = null;
    try {
        response = await axios({ method: "POST", url: `${url}/change-password`, data: { "token": token, "password": oldPassword, "newPassword": newPassword } });
        if (response.data.error) {
            toast(response.data.error, "red")
            return { "error": response.data.error };
        } else {
            if (response.status === 200) {
                toast("Senha alterada com sucesso");
                return response.data;
            } else {
                toast("Ocorreu um erro ao alterar a senha", "red");
                return { "error": "Unknown error!" };
            }
        }

    } catch (err) {
        toast("Ocorreu um erro ao alterar a senha, verifique sua senha e tente novamente", "red");
        return { "error": err };
    }
}

async function changePhoto(photo, token, toast) {
    let response = null;
    try {
        response = await axios({
            method: "POST", url: `${urlPhoto}/upload`,

            data: {
                "token": token,
                "nomeImage": "student",
                "image": photo.base64,
                "type": photo.type
            }
        });

        if (response.data.error) {
            toast(response.data.error, "red")
            return { "error": response.data.error };
        } else {
            if (response.status === 201) {
                axios({ method: "POST", url: `${url}/edit-photo`, data: { "idPhoto": response.data.response.path.id, "token": token } }).then(res => {
                    if (res.status === 200) {
                        toast("Foto alterada com sucesso", null, () => {
                            let user = JSON.parse(localStorage.getItem("user"));
                            user.data.photo = response.data.response.path;
                            localStorage.setItem("user", JSON.stringify(user));
                        });
                    } else {
                        toast("Ocorreu um erro ao alterar a foto, tente novamente mais tarde", "red");
                        return { "error": "Unknown error" };
                    }
                }).catch(err => {
                    toast("Ocorreu um erro ao alterar a foto, tente novamente mais tarde", "red");
                    return { "error": err };
                })
                return response.data;
            } else {
                toast("Ocorreu um erro ao alterar a foto", "red");
                return { "error": "Unknown error!" };
            }
        }
    } catch (e) {
        toast("Ocorreu um erro ao alterar a foto, tente novamente mais tarde", "red");
        return { "error": e };
    }
}

async function acceptInvite(projectId, studentId, notificationId, token) {
    await axios({ method: "POST", url: `${url}/accept-invite`, data: { "projectId": projectId, "studentId": studentId } }).then(async () => {
        await axios({ method: "POST", url: `${urlNotification}/hide`, data: { "idNotification": notificationId, "token": token } }).then(async () => {
        }
        ).catch(error => {
            console.log(error);
        });
    }).catch(err => {
        console.log(err);
    });
}

async function denyInvite(projectId, studentId, notificationId, token) {
    await axios({ method: "POST", url: `${url}/deny-invite`, data: { "projectId": projectId, "studentId": studentId } }).then(async () => {
        await axios({ method: "POST", url: `${urlNotification}/hide`, data: { "idNotification": notificationId, "token": token } }).then(async () => {
        }).catch(error => {
            console.log(error);
        });
    }).catch(error => {
        console.log(error);
    });
}

async function sendSolicitation(projectId, studentId, toast) {
    await axios({ method: "POST", url: `${url}/send-solicitation`, data: { "projectId": projectId, "studentId": studentId } }).then(async () => {
        toast("Solicitação enviada")
    }).catch(error => {
        toast(error, "red");
    });
}

async function forgotPassowrd(email, toast) {
    let response;
    try {
        response = await axios({ method: "POST", url: `${url}/forgot-password`, data: { "email": email } })
        if (response.status === 200) {
            toast("Email enviado", null, () => {
                localStorage.setItem("emailForgot", email);
                window.location.href = "/password-redefine";
            });

            return { "response": response.data }
        } else {
            toast(response.error, "red");
            return { "error": response.error }
        }
    } catch (e) {
        toast(e, "red");
        return { "error": e };
    }
}

async function changePasswordWithCode(email, code, password, toast) {
    let response;

    try {
        response = await axios({ method: "POST", url: `${url}/check-code`, data: { "email": email, "code": code } });
        if (response.status === 200) {
            let responsePassowrd = await axios({ method: "POST", url: `${url}/change-password-code`, data: { "email": email, "code": code, "password": password } });
            if (responsePassowrd.status === 200) {
                toast("Senha alterada com sucesso", null, () => {
                    window.location.href = "/login/aluno";
                    localStorage.removeItem("emailForgot");
                });

            } else {
                toast(responsePassowrd.error, "red");
                return { "error": responsePassowrd.error }
            }
        } else {
            toast(response.error, "red");
            return { "error": response.error }
        }

    } catch (e) {
        toast(e, "red");
        return { "error": e };
    }
}

async function addTechnology(level, idTechnology, technologyName, toast) {
    let response;
    let user = JSON.parse(localStorage.getItem("user"));
    try {
        response = await axios({ method: "POST", url: `${urlMatrix}/add`, data: { "level": level, "technology": idTechnology, "token": user.token } });
        if (response.status === 201) {
            toast(`Tecnologia ${technologyName} adicionada!`);
            user.data.matrix.push(
                {
                    id: response.data.response.data.TechnologyId,
                    name: technologyName,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                    SkillsMatrices: {
                        StudentId: user.data.id,
                        TechnologyId: idTechnology,
                        level: level,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    }
                }
            );

            localStorage.setItem("user", JSON.stringify(user));

            return response.data;
        } else {
            toast("Erro ao adicionar " + technologyName, "red");
            return { "error": response.error };
        }
    } catch (e) {
        toast("Erro ao adicionar " + technologyName, "red");
        return { "error": e };
    }
}

async function removeTechnology(idTechnology, technologyName, toast) {
    let user = JSON.parse(localStorage.getItem("user"));
    let response;
    try {
        response = await axios({ method: "POST", url: `${urlMatrix}/remove`, data: { "technology": idTechnology, "token": user.token } });
        if (response.status === 201) {

            toast(`Tecnologia ${technologyName} removida!`);
            for (let key in user.data.matrix) {
                if (user.data.matrix[key].name === technologyName) {
                    user.data.matrix.splice(key, 1);
                    break;
                }
            }

            localStorage.setItem("user", JSON.stringify(user));
            return response.data;
        } else {
            toast("Erro ao deletar " + technologyName, "red");
            return { "error": response.error };
        }
    } catch (e) {
        toast("Erro ao deletar " + technologyName, "red");
        return { "error": e };
    }
}

async function editTechnology(idTechnology, level, toast) {
    let response;
    let user = JSON.parse(localStorage.getItem("user"));
    try {
        response = await axios({ method: "POST", url: `${urlMatrix}/edit`, data: { "technology": idTechnology, "token": user.token, "level": level } });
        if (response.status === 201) {
            toast("Editado com sucesso");
            for (let key in user.data.matrix) {
                if (user.data.matrix[key].SkillsMatrices.TechnologyId === idTechnology) {
                    user.data.matrix[key].SkillsMatrices.level = level;
                    break;
                }
            }
            localStorage.setItem("user", JSON.stringify(user));
        } else {
            toast("Erro ao editar", "red");
        }
    } catch (e) {
        toast("Erro ao editar", "red");
        return { "error": e };
    }
}

async function hideNotification(idNotification, token){
    return new Promise((resolve, reject) => {
        return axios({
            method: "POST",
            url: `${urlNotification}/hide`,
            data: {
                idNotification: idNotification,
                token: token
            }
        }).then(response => {
            resolve(response);
        }).catch(err => {
            toast("Ocorreu um erro ao marcar notificação como lida. Tente mais tarde", "red");
            reject(err)
        })
    })
}