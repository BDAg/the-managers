import axios from 'axios';
import { toast } from '../_helpers';
import { apiUrl } from '../_helpers/api-url';
// const url = "http://35.231.220.66:3000/post";
// const photoUrl = "http://35.231.220.66:3000/photo"

const url = apiUrl+"/post";
const photoUrl = apiUrl+"/photo";

export const postServices = {
    getAll,
    likePost,
    unlikePost,
    comment,
    createPost,
    editPost,
    deletePost,
    Polling
}

async function Polling() {
    let haveNews = false;
    let newData = await axios({ method: "GET", url: `${url}/list` });
    if (newData.status === 200) {
        haveNews = newData.data.response.length > JSON.parse(localStorage.getItem("posts")).length;

        if (haveNews) {
            localStorage.setItem("posts", JSON.stringify(newData.data.response));
        }

        return haveNews;
    }
    return false;
}

async function getAll(callback = null) {
    return new Promise((resolve, reject) =>
        axios({ method: "GET", url: `${url}/list` }).then(response => {
            localStorage.setItem("posts", JSON.stringify(response.data.response));

            if (callback) {
                callback(true);
            }
            resolve(response);
        }).catch(err => {
            reject(err)
            toast(err.toString(), "red");
        })
    );
}

async function likePost(token, postId, data) {
    axios({
        method: "POST", url: `${url}/like`, data: {
            "token": token,
            "postId": postId
        }
    }).then(response => {
        if (response.status === 201) {
            let posts = JSON.parse(localStorage.getItem("posts"));
            data.Likes[0].id = response.data.response.data.id;
            for (let key in posts) {
                let post = posts[key];
                if (post.id === data.id) {
                    posts[key] = data
                }
            }

            localStorage.setItem("posts", JSON.stringify(posts));
            return true
        } else {
            return false
        }
    }).catch(err => {
        toast(err.toString(), "red");
        return false
    });
}

async function unlikePost(token, postId, dataId) {
    axios({
        method: "POST", url: `${url}/unlike`, data: {
            "token": token,
            "postId": postId
        }
    }).then(response => {
        if (response.status === 200) {
            let posts = JSON.parse(localStorage.getItem("posts"));

            for (let key in posts) {
                let post = posts[key];
                if (post.id === postId) {
                    for (let likeKey in post.Likes) {
                        let postLike = post.Likes[likeKey];
                        if (dataId.isStudent) {
                            if (postLike.StudentId === dataId.StudentId) {
                                posts[key].Likes.splice(likeKey, 1);
                            }
                        } else {
                            if (postLike.ProfessorId === dataId.ProfessorId) {
                                posts[key].Likes.splice(likeKey, 1);
                            }
                        }
                    }
                }
            }
            localStorage.setItem("posts", JSON.stringify(posts));
            return true
        } else {
            return false
        }
    }).catch(err => {
        toast(err.toString(), "red");
        return false
    });
}

async function comment(token, comment, postId, data) {
    axios({
        method: "POST", url: `${url}/comment`, data: {
            "token": token,
            "postId": postId,
            "comment": comment
        }
    }).then(response => {

        if (response.status === 201) {
            data.Comentaries[0].id = response.data.response.data.id;
            let posts = JSON.parse(localStorage.getItem("posts"));

            for (let key in posts) {
                let post = posts[key];
                if (post.id === data.id) {
                    posts[key] = data
                }
            }

            localStorage.setItem("posts", JSON.stringify(posts));

            return true
        } else {
            return false
        }
    }).catch(err => {
        toast(err.toString(), "red");
        return false
    });
}

async function createPost(token, description, projectId, photo, callback) {
    if (photo) {
        axios({
            method: "POST", url: `${photoUrl}/upload`, data: {
                "nomeImage": "post",
                "token": token,
                "image": photo,
                "type": photo.split(";base64")[0]
            }
        }).then(response => {
            if (response.status === 201) {
                axios({
                    method: "POST", url: `${url}/create`, data: {
                        "token": token,
                        "PhotoId": response.data.response.path.id,
                        "description": description,
                        "ProjectId": projectId
                    }
                }).then(postResponse => {
                    if (postResponse.status === 201) {
                        getAll(callback);

                        return true
                    } else {
                        return false
                    }
                }).catch(err => {
                    toast(err.toString(), "red");
                    return false
                });
            }
        }).catch(err => {
            toast(err.toString(), "red");
            return false
        });
    }
    else {
        axios({
            method: "POST", url: `${url}/create`, data: {
                "token": token,
                "description": description,
                "ProjectId": projectId
            }
        }).then(postResponse => {
            if (postResponse.status === 201) {
                getAll(callback);

                return true
            } else {
                return false
            }
        }).catch(err => {
            toast(err.toString(), "red");
            return false
        });
    }
}

async function editPost(token, PostId, description, projectId) {
    axios({
        method: "POST", url: `${url}/edit`, data: {
            "token": token,
            "postId": PostId,
            "description": description,
            "ProjectId": projectId
        }
    }).then(response => {
        if (response.status === 200) {

            toast("Post editado com sucesso", null, getAll(() => {
                if (window.location.pathname !== "/perfil")
                    window.location.replace("/perfil#posts");
                else {
                    window.location.reload()
                }
            }));
            return true
        } else {
            toast("Ocorreu um erro ao editar o post", "red");
            return false
        }
    }).catch(err => {
        toast(err.toString(), "red");
        return false
    });
}

async function deletePost(token, PostId) {
    axios({
        method: "POST", url: `${url}/delete`, data: {
            "token": token,
            "postId": PostId
        }
    }).then(response => {
        if (response.status === 200) {
            toast("Post deletado com sucesso", null, getAll(() => {
                if (window.location.pathname !== "/perfil")
                    window.location.replace("/perfil#posts");
                else {
                    window.location.reload()
                }
            }));
            return true
        } else {
            toast("Ocorreu um erro ao deletar o post", "red");
            return false
        }
    }).catch(err => {
        toast(err.toString(), "red");
        return false
    });
}