import axios from 'axios';
import { history } from '../_helpers';
import { studentServices } from '.';
import { toast } from '../_helpers';
import { apiUrl } from '../_helpers/api-url';
// const url = "http://35.231.220.66:3000/project";

const url = apiUrl +"/project";

export const projectServices = {
    getAll,
    viewPerfil: getProject,
    createProject,
    editProject,
    inviteMember,
    addTechnology,
    removeTechnology,
    editMember,
    removeMember
}

async function getAll() {
    return new Promise((resolve, reject) =>
        axios({ method: "GET", url: `${url}/list` }).then(response => {
            let projects = response.data.response.data;
                if (JSON.stringify(projects) !== localStorage.getItem("projects")) {
                    localStorage.setItem("projects", JSON.stringify(projects));
                }
            resolve(projects);

        }).catch(err => {
            reject(err)
            toast(err.toString(), "red");
        })
    );
    // let response = null;

    // try {
    //     response = await axios({ method: 'GET', url: url + '/list' })
    //     if (response.data.error) {
    //         return { "error": response.data.error };
    //     } else {
    //         if (response.status === 201) {
    //             let projects = response.data.response.data;
    //             if (JSON.stringify(projects) !== localStorage.getItem("projects")) {
    //                 localStorage.setItem("projects", JSON.stringify(projects));
    //             }
    //             return projects
    //         } else {
    //             return { "error": "Unknown error" }
    //         }
    //     }
    // } catch (e) {
    //     return { "error": e }
    // }
}

async function getProject(projectUrl) {
    let response = null;
    let data = localStorage.getItem("projectsPerfil") ? JSON.parse(localStorage.getItem("projectsPerfil")) : {}
    try {
        response = await axios({ method: 'GET', url: `${url}/view-perfil/${projectUrl}` });
        if (response.data.error) {
            return { "error": response.data.error }
        } else {
            if (response.status === 201) {
                let dataProject = response.data.data;
                data[projectUrl] = dataProject
                localStorage.setItem("projectsPerfil", JSON.stringify(data));
                return dataProject
            } else {
                return { "error": "Unknown error" }
            }
        }
    } catch (e) {
        window.location.href = "/not-found";
        switch (e.message) {
            case ("Request failed with status code 404"):
                window.location.href = "/not-found";
                break
            default:
                return toast(e, "red", () => {
                    history.goBack();
                });
        }
    }
}

async function createProject(data) {
    let response = null;
    try {
        response = await axios({ method: "POST", url: `${url}/create`, data: data });

        if (response.status === 201) {
            let project = response.data.data;
            project["Members"] = {
                isMember: 1,
                job: data.job,
                invite: 0
            };
            let user = JSON.parse(localStorage.getItem("user"));
            user["data"]["projects"].push(project);
            localStorage.setItem("user", JSON.stringify(user));
            return { "response": "Project created", "error": null }
        } else {
            if (response.status === 401) {
                return { "error": "Unauthorized", "response": null }
            }
        }

    } catch (e) {
        return { "error": e }
    }
}

async function editProject(data) {
    let response = null;
    data["token"] = JSON.parse(localStorage.getItem("user"))["token"];

    try {
        response = await axios({ method: "POST", url: `${url}/edit`, data: data });
        if (response.status === 201) {
            localStorage.removeItem('projectsPerfil');
            return { "response": "Project edited", "error": null }
        } else {
            if (response.status === 401) {
                return { "error": "Unauthorized", "response": null }
            }
        }

    } catch (e) {
        return { "error": e }
    }
}

async function inviteMember(projectId, studentId, job) {
    let response = null;

    try {
        response = await axios({ method: "POST", url: `${url}/invite-member`, data: { studentId: studentId, projectId: projectId, job: job } });
        if (response.status === 201) {
            await studentServices.listUsersWithoutProjects();
            return response.data.response;
        } else {
            if (response.error) {
                return response
            } else {
                return { "error": "Unknown error" }
            }
        }

    } catch (e) {
        return { "error": e }
    }
}

async function addTechnology(idProject, technologyId, technologyName, toast) {
    let project = window.location.pathname.replace("/projects/", "");
    let projects = JSON.parse(localStorage.getItem("projectsPerfil"));
    let response;
    try {
        response = await axios({ method: "POST", url: `${url}/add-technology`, data: { "projectId": idProject, "technologyId": technologyId } });
        if(response.status === 201){
            toast(`Tecnologia Adicionada!`);
            if (projects && project) {
                projects[project].Technologies.push({
                    "id": technologyId,
                    "name": technologyName,
                    "Projects_has_Technologies": {
                        "ProjectId": idProject,
                        "TechnologyId": technologyId
                    }
                });
                localStorage.setItem("projectsPerfil", JSON.stringify(projects));
            }
        } else {
            toast("Erro ao adicionar", "red");
            return { "error": response.error }
        }
    } catch (e) {
        toast("Erro ao adicionar", "red");
        return { "error": e }
    }
}

async function removeTechnology(idProject, technologyId, toast) {
    let project = window.location.pathname.replace("/projects/", "");
    let projects = JSON.parse(localStorage.getItem("projectsPerfil"));

    let response;

    try {
        response = await axios({ method: "POST", url: `${url}/remove-technology`, data: { "projectId": idProject, "technologyId": technologyId } });
        if (response.status === 201) {
            if (projects && project) {
                let technologies = projects[project].Technologies
                for (let key in technologies) {
                    if (technologies[key] === technologyId) {
                        projects[project].Technologies.splice(key, 1);
                        break
                    }
                }
                localStorage.setItem("projectsPerfil", JSON.stringify(projects));
                toast(`Tecnologia removida!`);
                return response;
            }
        } else {
            toast("Erro ao deletar", "red");
            return { "error": response.error }
        }

    } catch (e) {
        toast("Erro ao deletar", "red");
        return { "error": e }
    }

}

async function editMember(idMember, idProject, newJob, toast, projectName) {
    let response;
    let token = JSON.parse(localStorage.getItem("user")).token;
    let data = localStorage.getItem("projectsPerfil") ? JSON.parse(localStorage.getItem("projectsPerfil")) : {}
    data = data[projectName] ? data : data[projectName] = {}
    let students = data[projectName].Students;

    try {
        response = await axios({method: "POST", url:`${url}/edit-member-job`, data: {
            "idMember": idMember, "idProject": idProject, "newJob": newJob, "token": token
        } });
        if(response.status === 201){
            toast(`Membro editado com sucesso!`);
            for(let key in students) {
                if(students[key].id === idMember) {
                    data[projectName].Students[key].Members.job = newJob;
                    break
                }
            };

            localStorage.setItem("projectsPerfil", JSON.stringify(data));

            return response;
        } else{
            toast(`Erro ao editar membro!`, "red");
        }

    } catch(e){
        toast(`Erro ao editar membro! <br/>`+e, "red");
    }
}

async function removeMember(idMember, idProject, toast, projectName){

    let response;
    let token = JSON.parse(localStorage.getItem("user")).token;
    let data = localStorage.getItem("projectsPerfil") ? JSON.parse(localStorage.getItem("projectsPerfil")) : {}
    data = data[projectName] ? data : data[projectName] = {}
    let students = data[projectName].Students;

    try {
        response = await axios({method: "POST", url:`${url}/remove-member`, data: {
            "idMember": idMember, "idProject": idProject, "token": token
        } });
        if(response.status === 201){
            toast(`Membro removido com sucesso!`);
            for(let key in students) {
                if(students[key].id === idMember) {
                    data[projectName].Students.splice(key, 1);
                    break
                }
            };
            localStorage.setItem("projectsPerfil", JSON.stringify(data));
            return response;
        } else{
            toast(`Erro ao remover membro!`, "red");
        }

    } catch(e){
        toast(`Erro ao remover membro! <br/>`+e, "red");
    }
}