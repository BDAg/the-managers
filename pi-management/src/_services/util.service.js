import axios from 'axios';
import { apiUrl } from '../_helpers/api-url';

// const url = "http://35.231.220.66:3000/util";

const url = apiUrl+"/util";

export const utilServices = {
    states,
    cities,
    citiesByState
}

async function states() {
    let res;
    try {

        res = await axios.get(url + "/states");

        if (res.data.error) {
            return { "response": { "message": res.data.error } };
        }
        else {
            if (res.status === 200) {
                let states = res.data.response;

                // localStorage.setItem('states', JSON.stringify(states));
                return JSON.stringify(states);
            }
            else
                return { "error": "Unknown error!" }
        }
    } catch (e) {
        return { "response": { "message": e } };
    }
}

async function cities() {
    let res;
    try {

        res = await axios.get(url + "/cities");

        if (res.data.error) {
            return { "response": { "message": res.data.error } };
        }
        else {
            if (res.status === 200) {
                let cities = res.data.response;

                // localStorage.setItem('cities', JSON.stringify(cities));
                return JSON.stringify(cities);
            }
            else
                return { "error": "Unknown error!" }
        }
    } catch (e) {
        return { "response": { "message": e } };
    }
}

async function citiesByState(IdState) {
    let res;
    try {

        res = await axios.get(url + "/cities/" + IdState.toString());

        if (res.data.error) {
            return { "response": { "message": res.data.error } };
        }
        else {
            if (res.status === 200) {
                let cities = res.data.response;

                // localStorage.setItem('cities', JSON.stringify(cities));
                return JSON.stringify(cities);
            }
            else
                return { "error": "Unknown error!" }
        }
    } catch (e) {
        return { "response": { "message": e } };
    }
}