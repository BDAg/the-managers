import React from 'react';
import { Row, Textarea, Icon, Col, Button, Select } from 'react-materialize';
import { postServices } from '../../_services';
import { Loading } from '../General';
const reader = new FileReader();

export class NewPost extends React.Component {
    constructor(props) {
        super(props);
        this.user = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : JSON.parse(localStorage.getItem("professor"));
        this.token = this.user.token;
        this.userProjects = this.user.data.projects || JSON.parse(localStorage.getItem("projects")) || [];
        this.role = this.user.data.role;

        this.state = {
            image: null,
            description: "",
            project: this.props.project ? this.props.project.id : "",
            validForm: false,
            submited: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleFile = this.handleFile.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleFile(e) {
        let file = e.target.files[0];
        if (file) {
            reader.readAsDataURL(file);
        }
        reader.onloadend = () => {
            this.setState({
                image: reader.result,
                validForm: true
            });
        }
    }

    handleChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;
        this.setState(data, () => {
            let { description, image } = this.state;
            let isValid = description.length > 5 || image !== null
            this.setState({
                validForm: isValid
            });
        });
    }

    async handleSubmit(event) {
        event.preventDefault();
        this.setState({ submited: true, validForm: false });
        const { project, image, description } = this.state;

        await postServices.createPost(this.token, description, project.toString().length > 0 ? project : null, image, this.props.reloadThis || window.location.reload());
        this.setState({ description: "", project: this.props.project ? this.props.project.id : null });
    }

    render() {
        const Options = () => {
            if (this.role === 1) {
                return Object.keys(this.userProjects).map((key) => {
                    if (this.userProjects[key].Members.isMember === 1) {
                        return (
                            <option key={key} value={this.userProjects[key].id}>
                                {this.userProjects[key].name}
                            </option>
                        )
                    }
                    return <React.Fragment key={key}></React.Fragment>
                })
            } else {
                return Object.keys(this.userProjects).map((key) => {
                    return (
                        <option key={key} value={this.userProjects[key].id}>
                            {this.userProjects[key].name}
                        </option>
                    )
                })
            }

        }
        const { image, validForm, description, submited } = this.state;
        // let defaultValue = this.props.project ? this.props.project.id : undefined;
        return (
            <Row s={12} m={12} l={12} xl={12} className="z-depth-1" >
                <Col s={12} m={12} l={12} xl={12}>
                    <form onSubmit={this.handleSubmit}>
                        <Row s={12} m={12} l={12} xl={12} >
                            <Select s={12} l={6} xl={6} value={this.state.project ? this.state.project.toString() : ""} name="project" label="Selecionar projeto" onChange={this.handleChange} disabled={this.props.project !== undefined}>
                                <option value={""}>
                                    Não publicar em nenhum projeto
                                </option>
                                {
                                    this.props.project ?
                                        <option key={0} value={this.props.project.id}>
                                            {this.props.project.name}
                                        </option>
                                        :
                                        <Options />
                                }
                            </Select>

                            <Textarea label="Escrever publicação" data-length={255} value={description} onChange={this.handleChange} name="description" style={{ height: "4em" }} s={12} m={12} l={12} xl={12} />

                            {image && <Col s={4} m={4} className="z-depth-1" style={{ marginLeft: "10px" }} l={4} xl={4}>
                                <Button className="right red-text" flat onClick={() => { this.setState({ image: null, validForm: description.length > 5 }) }}><Icon>close</Icon></Button>
                                <img style={{ marginTop: "5px" }} width="100%" src={image} alt="Foto" />
                                {submited && <Loading />}
                            </Col>

                            }
                        </Row>
                        <Row s={12} m={12} l={12} xl={12}>
                            <Col className="file-field input-field  z-depth-1" style={{ marginLeft: "5px" }}>
                                <Button flat>
                                    <span>
                                        Foto<Icon className="teal-text right">image</Icon>
                                    </span>
                                    <input type="file" onChange={this.handleFile} />
                                    <div className="file-path-wrapper">
                                        <input className="file-path validate" name="image" type="hidden"></input>
                                    </div>
                                </Button>
                            </Col>
                            <Col className="file-field input-field right z-depth-1 right" style={{ marginRight: "5px" }}>
                                <Button flat disabled={!validForm}>Publicar <Icon className="teal-text right">send</Icon></Button>
                            </Col>
                            {submited && <Loading />}
                        </Row>
                    </form>
                </Col>
            </Row>
        )
    }
}