import React from 'react';
import { Row, Icon, Col, Button, Modal, Textarea } from 'react-materialize';
import ReactTooltip from 'react-tooltip';
import { apiUrl } from '../../_helpers/api-url';

export class Comments extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            comments: this.props.comments,
        }
    }

    componentDidCatch() {
        setInterval(() => {
            console.log("Olá")
        }, 1000);
    }

    render() {
        const { comments } = this.state;
        let basePhoto = apiUrl+"/photos/";
        return (
            <Modal bottomSheet trigger={
                <Button waves={"teal"} flat className={`left`}>
                    <span>
                        <Icon className="right">comment</Icon>
                        comentar
                </span>
                </Button>
            }
                fixedFooter
                options={{
                    dismissible: true
                }}
                actions={[<Button flat modal={"close"} className="right"><Icon >close</Icon></Button>]}
            >
                <Row s={12} m={12} l={12} xl={12} >
                    <h3 className="left">Comentários</h3>
                </Row>
                <Row className="z-depth-1">
                    <form onSubmit={this.props.commentPost}>
                        <Textarea value={this.props.comment} label="Escrever comentário" s={12} m={12} l={12} xl={12} onChange={this.props.changeComment} />
                        <Col className="file-field input-field right z-depth-1 right" style={{ marginRight: "5px" }}>
                            <Button flat disabled={!this.props.validCommentForm}>Comentar <Icon className="teal-text right" >send</Icon></Button>
                        </Col>
                    </form>
                </Row>
                {
                    comments.length > 0 &&
                    Object.keys(comments).map(key => {
                        let comment = comments[key];
                        var photo = "";
                        if (comment.Student) {
                            if (comment.Student.Photo) {
                                photo = comment.Student.Photo.path
                            }
                        } else {
                            if (comment.Professor) {
                                if (comment.Professor.Photo) {
                                    photo = comment.Professor.Photo.path
                                }
                            }
                        }
                        return (
                            <Row className="z-depth-1" key={key}>
                                
                                <Col s={12} m={12} l={12} xl={12} className="valign-wrapper" style={{ marginTop: "5px", marginLeft: "5px" }}>
                                    {
                                        photo.length > 1 && <img data-tip="Professor" alt="" className="circle responsive img-user" style={comment.Professor && {border: "2px gold solid", boxShadow: "0 0 10px black"}}src={basePhoto + photo}></img>
                                    }
                                    <a href={comment.Student ? `/perfil/${comment.Student.email}` : `/perfil/professor/${comment.Professor.email}`} className="left">
                                        <span className="teal-text">
                                            {comment.Student ? comment.Student.name : comment.Professor.name}
                                        </span>
                                    </a>
                                </Col>
                                {comment.Professor && <ReactTooltip />}
                                <Row s={12} m={12} l={12} xl={12} style={{ marginTop: "5px", marginLeft: "15px" }}>
                                    <span className="center flow-text">{comment.comment}</span>
                                </Row>
                            </Row>
                        )

                    })
                }
            </Modal>
        )
    }
}