import React from 'react';
import { Row, Icon, Col, Button, CardPanel, Divider, Select } from 'react-materialize';
import './index.css';
import { Comments } from './Comments';
import { postServices } from '../../_services';
import { toast } from '../../_helpers';
import Textarea from 'react-materialize/lib/Textarea';
import ReactTooltip from 'react-tooltip';
import { AlertSubmit } from '../General/AlertSubmit';
import { apiUrl } from '../../_helpers/api-url';

export class ListPost extends React.Component {
    constructor(props) {
        super(props);
        this.user = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : localStorage.getItem("professor") ? JSON.parse(localStorage.getItem("professor")) : false;
        this.userProjects = this.user ? this.user.data.projects : [];
        let defaultValue = this.props.postData.Project ? this.props.postData.Project.id : 0;
        this.state = {
            data: this.props.postData,
            commentsLen: this.props.postData.Comentaries.length,
            likesLen: this.props.postData.Likes.length,
            liked: false,
            havePhoto: this.props.postData.Photo !== null,
            comment: "",
            validCommentForm: false,
            postOwner: false,
            editingPost: false,
            description: this.props.postData.description || "",
            defaultValue: this.props.postData.Project ? this.props.postData.Project.id : 0,
            project: defaultValue,
            dataTip: ""
        }

        this.likePost = this.likePost.bind(this);
        this.commentPost = this.commentPost.bind(this);
        this.changeComment = this.changeComment.bind(this);
        this.editPost = this.editPost.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.deletePost = this.deletePost.bind(this);
        this.fixImages = this.fixImages.bind(this);
    }

    componentDidMount() {
        var { data } = this.state;
        new Promise(resolve => setTimeout(resolve, 1500)).then(() => this.fixImages());
        if (this.user) {
            for (let key in data.Likes) {
                if (this.user.data.id === data.Likes[key].StudentId || this.user.data.id === data.Likes[key].ProfessorId) {
                    this.setState({
                        liked: true
                    });
                    break;
                }
            }

            this.setState({
                dataTip: mountTip(data)
            });

            if (this.user.data.role === 1) {
                if (data.Student) {
                    if (data.Student.id === this.user.data.id) {
                        this.setState({
                            postOwner: true
                        });
                    }
                }

            } else {
                if (data.Professor) {
                    if (data.Professor.id === this.user.data.id) {
                        this.setState({
                            postOwner: true
                        });
                    }
                }
            }
        } else {
            this.setState({
                dataTip: "Faça login para ver quem reagiu a essa publicação."
            })
        }
    }

    async likePost() {
        if (this.user) {
            const { liked, data, likesLen } = this.state;

            this.setState({ liked: !liked });

            if (liked) {
                this.setState({ likesLen: likesLen - 1 });
                let dataId = {
                    isStudent: false,
                    StudentId: null,
                    ProfessorId: null
                };
                let role = this.user.data.role;
                if (role === 1) {
                    dataId.isStudent = true;
                    dataId.StudentId = this.user.data.id;
                } else {
                    dataId.ProfessorId = this.user.data.id;
                }

                await postServices.unlikePost(this.user.token, data.id, dataId);

            } else {
                this.setState({ likesLen: likesLen + 1 });
                let role = this.user.data.role;
                let likeData;
                if (role === 1) {
                    likeData = {
                        "id": Infinity,
                        "createdAt": Date.now(),
                        "StudentId": this.user.data.id,
                        "ProfessorId": null,
                        "Student": {
                            "name": this.user.data.name,
                            "email": this.user.data.email,
                            "Photo": this.user.data.photo ? this.user.data.photo : null
                        },
                        "Professor": null
                    }
                } else {
                    likeData = {
                        "id": Infinity,
                        "createdAt": Date.now(),
                        "Student": null,
                        "StudentId": null,
                        "ProfessorId": this.user.data.id,
                        "Professor": {
                            "name": this.user.data.name,
                            "email": this.user.data.email,
                            "Photo": this.user.data.photo ? this.user.data.photo : null
                        }
                    }
                }

                data.Likes.unshift(likeData);
                await postServices.likePost(this.user.token, data.id, data);
            }
        } else {
            toast("Faça login para reagir a uma publicação", "red");
        }
    }

    async commentPost(e) {
        e.preventDefault();
        const { comment, commentsLen, data } = this.state
        this.setState({ commentsLen: commentsLen + 1, validCommentForm: false });

        let role = this.user.data.role;
        let commentData;
        if (role === 1) {
            commentData = {
                "id": Infinity,
                "comment": comment,
                "createdAt": Date.now(),
                "Student": {
                    "id": this.user.data.id,
                    "name": this.user.data.name,
                    "email": this.user.data.email,
                    "Photo": this.user.data.photo ? this.user.data.photo : null
                },
                "Professor": null
            }
        } else {
            commentData = {
                "id": Infinity,
                "comment": comment,
                "createdAt": Date.now(),
                "Student": null,
                "Professor": {
                    "id": this.user.data.id,
                    "name": this.user.data.name,
                    "email": this.user.data.email,
                    "Photo": this.user.data.photo ? this.user.data.photo : null
                }
            }
        }

        data.Comentaries.unshift(commentData);

        await postServices.comment(this.user.token, comment, data.id, data);

        this.setState({
            data: data,
            comment: ""
        });

    }

    changeComment(e) {
        this.setState({
            comment: e.target.value,
            validCommentForm: e.target.value.length > 5
        });
    }

    async editPost() {
        const { editingPost, description, data, project, defaultValue } = this.state;
        if (editingPost) {
            if (description !== data.description || project !== defaultValue) {
                await postServices.editPost(this.user.token, data.id, description, project)
            }
        }
        this.setState({
            editingPost: !editingPost
        });
    }

    handleChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;
        this.setState(data);
    }

    deletePost() {
        const { data } = this.state;
        postServices.deletePost(this.user.token, data.id);
    }

    componentWillUpdate() {
        this.fixImages();
    }

    componentDidUpdate(){
        this.fixImages();
    }    
    
    fixImages(){
        const { data, havePhoto } = this.state;
        let photo = havePhoto ? document.getElementById(`photo${data.Photo.id}`) : false;
        if (photo) {

            if (photo.width < photo.height) {
                photo.style.width = "auto";
                photo.style.maxWidth = "100%";
                photo.style.height = "auto";
                photo.style.maxHeight = "70vh";
                document.getElementById(`colPhoto${data.Photo.id}`).style.backgroundColor = "grey";
                document.getElementById(`colPhoto${data.Photo.id}`).style.maxHeight = "70vh";
            }
        }
    }

    componentWillReceiveProps(){
        this.fixImages();
    }

    render() {
        const { data, havePhoto, liked, commentsLen, likesLen, comment, postOwner, editingPost, description, defaultValue } = this.state;
        let basePhotoUrl = apiUrl+"/photos/";
        
        const EditButton = () => {
            return (
                editingPost
                    ?
                    <Button onClick={this.editPost} style={{ marginRight: "3px" }} floating waves={"teal"}><Icon>save</Icon></Button>
                    :
                    <Button onClick={this.editPost} style={{ marginRight: "3px" }} floating waves={"teal"}><Icon>edit</Icon></Button>
            )
        }
        if(document.readyState === "complete")
            this.fixImages();

        return (
            <Row className="z-depth-1">
                <Row s={12} m={12} l={12} xl={12} >
                    <Col s={8} m={8} l={8} xl={8} className="valign-wrapper" style={{ marginTop: "5px", marginLeft: "5px" }}>
                        {data.Student ?
                            data.Student.Photo && <img alt="" className="circle responsive img-user" src={basePhotoUrl + data.Student.Photo.path}></img>
                            :
                            data.Professor.Photo && <img data-tip="Professor" alt="" className="circle responsive img-user" style={{ border: "2px gold solid", boxShadow: "0 0 10px black" }} src={basePhotoUrl + data.Professor.Photo.path}></img>
                        }
                        {data.Professor && <ReactTooltip />}
                        <a href={data.Student ? `/perfil/${data.Student.email}` : `/perfil/professor/${data.Professor.email}`} className="left">
                            <span className="teal-text">
                                {data.Student ? data.Student.name : data.Professor.name}
                            </span>
                        </a>
                        {data.Project && <React.Fragment>
                            <Icon className="center">chevron_right</Icon>
                            <a href={`/projects/${data.Project.url}`} className="left">
                                <span className="teal-text">
                                    {data.Project.name}
                                </span>
                            </a>
                        </React.Fragment>
                        }

                    </Col>
                    {postOwner &&
                        <Col style={{ marginTop: "3px" }} className="valign-wrapper right">
                            {havePhoto && <EditButton />}
                            <Button style={{ marginRight: "3px" }} floating waves={"teal"} className="red" onClick={editingPost ? () => this.setState({ editingPost: false }) : ()=>AlertSubmit(this.deletePost)}><Icon>close</Icon></Button>
                        </Col>
                    }
                </Row>
                {
                    havePhoto
                        ?
                        <Col s={12} m={12} l={12} xl={12}>
                            <Col s={12} m={12} l={12} xl={12}>
                                {editingPost ?
                                    <form>
                                        <Select s={12} l={6} xl={6} name="project" label="Selecionar projeto" onChange={this.handleChange} defaultValue={defaultValue} disabled={this.props.project !== undefined}>
                                            <option value={null}>
                                                Não publicar em nenhum projeto
                                            </option>
                                            {
                                                this.props.project ?
                                                    <option key={0} value={this.props.project.id}>
                                                        {this.props.project.name}
                                                    </option>
                                                    :
                                                    Object.keys(this.userProjects).map((key) => {

                                                        if (this.userProjects[key].Members.isMember === 1) {

                                                            return (
                                                                <option key={key} value={this.userProjects[key].id}>
                                                                    {this.userProjects[key].name}
                                                                </option>
                                                            )
                                                        }
                                                        return <React.Fragment key={key}></React.Fragment>
                                                    })
                                            }
                                        </Select>
                                        <Textarea s={12} m={12} l={12} xl={12} value={description} name="description" onChange={this.handleChange} />
                                    </form>
                                    :

                                    <span className="left" style={{ marginLeft: "13px" }}>{data.description}</span>
                                }
                            </Col>
                            <Col s={12} m={12} l={12} xl={12} id={`colPhoto${data.Photo.id}`} className="center">
                                <img width="100%" src={basePhotoUrl + data.Photo.path} id={`photo${data.Photo.id}`} alt="Foto" />
                            </Col>
                        </Col>

                        :
                        <Col s={12} m={12} l={12} xl={12} className="center">
                            <CardPanel className="teal" s={12} m={12} l={12} xl={12}>
                                <Row s={12} m={12} l={12} xl={12}>
                                    <span className="center white-text flow-text">{data.description}</span>
                                </Row>
                            </CardPanel>
                        </Col>
                }
                <Col s={12} m={12} l={12} xl={12}>
                    <Divider />
                    {
                        liked && <div>
                            <span className="left grey-text">
                                Esse post motivou você
                            </span>
                            <br />
                        </div>
                    }
                    <span className="left grey-text" data-tip={this.state.dataTip}> {
                        commentsAndLikes(commentsLen, likesLen)

                    }
                        <ReactTooltip />
                    </span>
                    <br />
                    <Button waves={"teal"} onClick={this.likePost} flat className={`left ${liked && "teal accent-3 white-text"}`} style={{ marginRight: "5px" }}>
                        <span>
                            <Icon className="right">tag_faces</Icon>
                            Me motivou
                        </span>
                    </Button>
                    {this.user && <Comments comments={data.Comentaries.sort((a, b) => {
                        return b.id - a.id
                    })} commentPost={this.commentPost} comment={comment} changeComment={this.changeComment} validCommentForm={this.state.validCommentForm} />}
                    <br />

                    <br />
                </Col>

            </Row>
        )
    }
}

function commentsAndLikes(commentsLen, likesLen) {
    let commentsAndLikes = "";

    if (likesLen >= 1 || commentsLen >= 1) {
        commentsAndLikes += "Esse post"
    }
    if (likesLen >= 1) {
        commentsAndLikes += ` já motivou ${likesLen.toString()} pessoa`
        if (likesLen > 1) {
            commentsAndLikes += "s"
        }
    }
    if (likesLen >= 1 && commentsLen >= 1) {
        commentsAndLikes += " e"
    }
    if (commentsLen >= 1) {
        commentsAndLikes += " recebeu " + commentsLen.toString() + " comentário"
        if (commentsLen > 1) {
            commentsAndLikes += "s"
        }
    }

    return commentsAndLikes
}

function mountTip(data) {

    let dataTip = ""
    for (let key in data.Likes) {
        if (key <= 1) {
            if (data.Likes[key].Student) {
                dataTip += `${data.Likes[key].Student.name}`
            } else {
                dataTip += `${data.Likes[key].Professor.name}`
            }

            if (key < 1) {
                if (data.Likes.length > 2) {
                    dataTip += ", "
                } else {
                    if (data.Likes.length > 1)
                        dataTip += " e "
                }
            } else {
                if (data.Likes.length > 2) {
                    dataTip += ` e mais ${data.Likes.length - 2}`
                }
            }
        }
    }
    dataTip += `${data.Likes.length > 1 ? " foram motivados" : " foi motivado"} por essa publicação`
    return dataTip;
}