import React from 'react';
import { Loading } from '..'
import { projectServices } from '../../_services';
import M from 'materialize-css';


export class SearchUsers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: JSON.parse(localStorage.getItem("usersWProject")),
            invited: false
        }
        this.projectId = this.props.idProject;
        this.action = this.action.bind(this);
    }

    getData() {

        if (!this.state.users) {
            this.setState({
                users: JSON.parse(localStorage.getItem("usersWProject"))
            })
        } else {
            clearInterval(this.intervalData);
        }
    }

    componentDidMount() {
        this.intervalData = setInterval(
            () => this.getData(), 500
        );
    }

    async action(e) {
        e.preventDefault();
        this.setState({
            invited: true
        });

        let idProject = e.target.projectId.value;
        let idStudent = e.target.studentId.value;
        let response = await projectServices.inviteMember(idProject, idStudent, 1);
        let msg = '';
        if (response.error) {
            msg = "Ocorreu um erro ao enviar convite"
        }
        else
            msg = "Convite enviado"
        M.toast({
            html: `
                <span>${msg}</span>
            `,
            displayLength: 5000
        });

        this.setState({
            invited: false,
            users: JSON.parse(localStorage.getItem("usersWProject"))
        })
    }
    render() {
        const { users, invited } = this.state;
        if (users) {
            return (
                <div id="searchUsers" className="modal bottom-sheet">
                    <div className="modal-content">
                        {users.length > 0 ? Object.keys(users).map(user => {
                            return (
                                <div className="card darken-1" key={users[user].idStudent}>
                                    <div className="card-content">
                                        <b>{users[user].name} </b>
                                        <br />
                                        <span>{users[user].email}</span>
                                    </div>
                                    <div className="card-action">
                                        <form onSubmit={this.action}>
                                            <input type="hidden" value={this.projectId} name="projectId" />
                                            <input type="hidden" value={users[user].idStudent} name="studentId" />
                                            <button type="submit" className="btn-flat teal-text" disabled={invited} >{invited ? "Aguarde" : "Convidar"}</button>
                                        </form>
                                    </div>
                                </div>
                            )
                        }) : <span>Não há alunos sem projeto no momento</span>}
                    </div>
                    <div className="modal-footer">
                        <a href="#!" className="modal-close waves-effect waves-green btn-flat">Fechar</a>
                    </div>
                </div>
            )
        } else {
            return (
                <div id="searchUsers" className="modal bottom-sheet">
                    <div className="modal-content">
                        <Loading />
                    </div>
                </div>
            )
        }
    }


}