import React from 'react';
import M from 'materialize-css';
import './index.css';
import axios from 'axios';
import { Loading } from '../';
import { apiUrl } from '../../_helpers/api-url';

export const ProjectsTabs = ({ invites, solicitations }) => {
    document.addEventListener('DOMContentLoaded', function () {
        var el = document.querySelectorAll('.tabs')
        M.Tabs.init(el);
    })
    return (
        <div className="row ">
            <div className="col s12 ">
                <ul className="tabs tabs-fixed-width">
                    <li className="tab"><a className="active" href="#solicitations"><span>Solicitações de entrada</span></a></li>
                    <li className="tab"><a href="#invites"><span>Convites enviados</span></a></li>
                </ul>
            </div>
            <SolicitationsTab solicitations={solicitations} />
            <InvitesTab invites={invites} />
        </div>
    );
}

const SolicitationsTab = ({ solicitations }) => {
    return (
        <div id="solicitations" className="col s12">
            {solicitations ?
                Object.keys(solicitations.data.response).map(key => {
                    return <SolicitationsRow key={solicitations.data.response[key].Student.id} user={solicitations.data.response[key]} />
                })
                :
                <React.Fragment>
                    <br/>
                    <Loading />
                </React.Fragment>
            }

        </div>

    );
}

const InvitesTab = ({ invites }) => {
    return (
        <div id="invites" className="col s12">
            {invites ?
                Object.keys(invites.data.response).map(key => {
                    return <InvitesRow key={invites.data.response[key].Student.id} user={invites.data.response[key]} />
                })
                :
                <React.Fragment>
                    <br/>
                    <Loading />
                </React.Fragment>
            }
        </div>
    );
}

const InvitesRow = ({ user }) => {

    async function removerConvite(e){
        e.preventDefault();
        let studentId = e.target.studentId.value;
        let projectId = e.target.projectId.value;
        let response = await axios({method: "POST", url: apiUrl+"/project/cancel-invite", data: {studentId: studentId, projectId: projectId}});
        if(response.status === 201) {
            window.location.reload();
        } else {
            alert("Error "+response.status.error.toString());
        }
    }
    
    return (
        <div className="card darken-1">
            <div className="card-content">
                <b>{user.Student.name} </b>
                <br />
                <span>{user.email}</span>
            </div>
            <div className="card-action">
                <form onSubmit={removerConvite}>
                    <input type="hidden" value={user.Student.id} name="studentId" />
                    <input type="hidden" value={user.ProjectId} name="projectId" />
                    <button type="submit" className="btn-flat teal-text" > Remover </button>
                </form>
                {/* <a href={"/"}>Remover</a> */}
            </div>
        </div>
    )
}


const SolicitationsRow = ({ user }) => {
    async function aceitarSolicitacao(e){
        e.preventDefault();
        let studentId = e.target.studentId.value;
        let projectId = e.target.projectId.value;
        let response = await axios({method: "POST", url: apiUrl+"/project/agree-solicitation", data: {studentId: studentId, projectId: projectId}});
        if(response.status === 201) {
            window.location.reload();
        } else {
            alert("Error "+response.status.error.toString());
        }
    }

    return (
        <div className="card darken-1">
            <div className="card-content">
                <b>{user.Student.name} </b>
                <br />
                <span>{user.email}</span>
            </div>
            <div className="card-action">
                <form onSubmit={aceitarSolicitacao}>
                    <input type="hidden" value={user.Student.id} name="studentId" />
                    <input type="hidden" value={user.ProjectId} name="projectId" />
                    <button type="submit" className="btn-flat teal-text" > Aceitar </button>
                    {/* <button type="submit" className="btn-flat teal-text" > Remover </button> */}
                </form>
            </div>
        </div>
    )
}