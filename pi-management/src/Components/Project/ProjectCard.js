import React from 'react';
import { studentServices } from '../../_services';
import { toast } from '../../_helpers';
import { Link } from 'react-router-dom';

export default class ProjectCard extends React.Component {

    constructor(props) {
        super(props);
        this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : false;
        this.project = this.props.project;

        this.state = {
            userOnProject: false,
            solicitationInvited: false
        }

        this.solicitation = this.solicitation.bind(this);
    }


    componentDidMount() {
        if (this.user) {
            let userProjects = this.user["data"]["projects"];
            for (let key in userProjects) {

                if (userProjects[key]["id"] === this.project.id) {
                    if (userProjects[key].Members.isMember === 1)
                        this.setState({
                            userOnProject: true
                        });
                    else {
                        this.setState({
                            solicitationInvited: true
                        });
                    }
                    break
                }
            }
        }
    }

    async solicitation() {
        await studentServices.sendSolicitation(this.project.id, this.user ? this.user.data.id : undefined, toast);
        this.setState({
            solicitationInvited: true
        });
    }

    render() {
        const project = this.project;
        const { userOnProject, solicitationInvited } = this.state;
        return (
            <div className="card darken-1" key={project.id}>
                <Link to={"/projects/" + project.url} className="black-text">
                    <div className="card-content">
                        <span className="truncate"><b>{project.name} </b></span>
                        <span className="truncate">{project.description}</span>
                        <span className="truncate">{project.gitlab}</span>
                    </div>
                </Link>

                <div className="card-action">
                    <Link to={"/projects/" + project.url} className="btn-flat black white-text">Ver</Link>
                    {this.user ? userOnProject ? <span></span> :
                        <button type="submit" className="btn-flat" onClick={this.solicitation} disabled={solicitationInvited}>{!solicitationInvited ? "SOLICITAR ENTRADA NO PROJETO" : "SOLICITAÇÂO ENVIADA"}</button>
                        : <span></span>}
                </div>
            </div>
        )
    }
}