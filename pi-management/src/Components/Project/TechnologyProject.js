import React from 'react';
import { projectServices } from '../../_services';
import { toast } from '../../_helpers';

export const TechnologyProject = ({ technology, auth }) => {
    async function removeTechnology() {
        await projectServices.removeTechnology(technology.Projects_has_Technologies.ProjectId, technology.id, toast);
        document.querySelector(".tech"+technology.id).remove();
    }
    return (
        <div className={"col s12 l6 xl4 m6 tech"+technology.id}>
            <div className="card-panel">
                {auth && <button onClick={removeTechnology} className="btn-floating btn-small waves-effect waves-light red right"><i className="material-icons">clear</i></button>}
                <span >{technology.name}</span>
            </div>
        </div>
    )
}