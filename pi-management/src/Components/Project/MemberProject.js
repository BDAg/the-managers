import React from 'react';
import { jobs, toast } from '../../_helpers'
import './index.css';
import M from 'materialize-css';
import { Modal, Select, Button, Col, Row } from 'react-materialize';
import { projectServices } from '../../_services';
import { AlertSubmit } from '../General/AlertSubmit';
import { apiUrl } from '../../_helpers/api-url';

export const MemberProject = ({ memberData, authorizedUser = false, projectName }) => {
    let photoUrl = apiUrl+"/photos/";
    var elems = document.querySelectorAll('.collapsible');
    M.Collapsible.init(elems);
    let disabledButton = false;

    async function removeMember() {
        disabledButton = true;
        await projectServices.removeMember(memberData.id, memberData.Members.ProjectId, toast, projectName);
        disabledButton = false;
    }

    return (
        <li>
            <div className="collapsible-header">
                <div className="valign-wrapper">
                    <div>
                        {memberData.Photo ? <img className="responsive-img img-user circle" alt={`Foto de ${memberData.name}`} src={photoUrl + memberData.Photo.path}></img> : <i className="img-user material-icons">person_outline</i>}
                    </div>
                    <div className="name-member">
                        {memberData.name}
                    </div>
                </div>

                <span className="badge">{jobs[memberData.Members.job]}</span>
            </div>
            <div className="collapsible-body">
                <Row className="valign-wrapper">
                    <Col xl={3}>
                        <span>{memberData.semester}º TERMO</span>
                    </Col>
                    <Col xl={2}>
                        <a href={`/perfil/${memberData.email}`}>VER PERFIL</a>
                    </Col>
                    {
                        authorizedUser &&

                        <React.Fragment>
                            <Col xl={2}>
                                <a href={`#edit${memberData.id}`} className="modal-trigger">EDITAR CARGO</a>
                            </Col>
                            {memberData.Members.job === 1 && <Col xl={5} className="center">
                                <Button disabled={disabledButton} flat onClick={() => AlertSubmit(removeMember)} className="red-text">Remover membro</Button>
                            </Col>}
                        </React.Fragment>
                    }
                </Row>
                <br />
                <br />
            </div>
            <EditModal user={memberData} projectName={projectName} />
        </li>
    )
}



const SelectJob = ({ actualJob, name, handleChange }) => {
    return (
        <Select s={12} m={12} l={12} xl={12} label={`Editar o cargo de ${name}`} defaultValue={actualJob} required onChange={handleChange} name="job">
            <option value="" disabled> Escolha a função de {name}</option>
            <option value={1}>Time</option>
            <option value={2}>Scrum Master</option>
            <option value={3}>Product Owner</option>
        </Select>
    )
}


class EditModal extends React.Component {
    // let token = JSON.parse(localStorage.getItem("user")).token;

    constructor(props) {
        super(props);
        this.user = this.props.user;
        this.projectName = this.props.projectName;
        this.state = {
            job: this.user.Members.job || 1,
            submited: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    async handleSubmit(event) {
        event.preventDefault();

        this.setState({
            submited: true
        });

        const { job } = this.state;

        await projectServices.editMember(this.user.id, this.user.Members.ProjectId, job, toast, this.projectName)
        this.setState({
            submited: false
        });
    }

    handleChange(event) {
        this.setState({
            job: event.target.value
        });
    }

    render() {

        const user = this.user;
        const { submited } = this.state;
        let customSize = "s12 m12 l12 xl12"
        return (
            <Modal id={`edit${user.id}`}
                bottomSheet
                className="modal bottom-sheet"
                actions={[
                    <a href="#!" className="modal-close waves-effect waves-green btn-flat">Fechar</a>
                ]} >
                <div>
                    <div className={`row ${customSize} center`}>
                        <span>Editar {user.name}</span>
                    </div>
                    <div className={`row ${customSize} `} >
                        <form className={`row ${customSize} `} onSubmit={this.handleSubmit}>
                            <SelectJob handleChange={this.handleChange} name={user.name} actualJob={user.Members.job} />
                            <button className="btn-large waves-effect waves-light teal right" disabled={submited}>
                                <span>Salvar<i className="material-icons right">send</i></span>
                            </button>
                        </form>
                    </div>
                </div>
            </Modal>
        );
    }
}   