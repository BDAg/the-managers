import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
// import { PrivateRoute } from './PrivateRoute';
// import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { TopNavigator, NoMatch, PrivateRoute } from '..'
import M from 'materialize-css';
import { userActions, professorActions } from '../../_actions'
import { isMobile } from 'react-device-detect';
import {
    ListProjects,
    ViewProject,
    CreateProject,
    LoginStudent,
    EditProject,
    RegisterStudent,
    UserSettings,
    PasswordRedefine,
    ForgotPassword,
    UserPerfil,
    StudentPerfil,
    RegisterSwitch,
    HomePage,
    LoginAdmin,
    AdminArea,
    ListTutorials,
    RegisterTeacher,
    ForgotPasswordTeacher,
    LoginSwitch,
    LoginTeacher,
    ProfessorSettings,
    ProfessorPerfil,
    TeacherArea,
    ListarCronograma
    // HomePageTeacher
} from '../../Pages';

import { ViewTutorial } from '../Tutorials';
import { PasswordRedefineTeacher } from '../../Pages/PasswordRedefineTeacher';
import { ManipulateCronograma, GenerateReports } from '../Professor';
import { PrivateRouteProfessor, PrivateRouteStudent, PublicRoute } from './PrivateRoute';
import { NoMobileSupport } from './NoMobileSupport';
import { UnderConstruction } from './UnderConstruction';

class RouterComponent extends React.Component {
    constructor(props) {
        super(props);

        this.socket = this.props.socket;
        this.userRole = localStorage.getItem("professor") ? 2 : 1
    }

    componentDidMount() {
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.dropdown-trigger');
            M.Dropdown.init(elems);
        });
    }

    render() {
        return (
            <Router>
                <TopNavigator socket={this.socket} />
                <Switch>
                    <Route exact path="/" component={HomePage} />
                    <Route exact path="/projects" component={ListProjects} />
                    <Route exact path="/projects/:url" component={ViewProject} />
                    <PublicRoute exact path="/login" component={LoginSwitch} />
                    <PublicRoute exact path="/login/aluno" component={LoginStudent} />
                    <PublicRoute exact path="/login/professor" component={LoginTeacher} />
                    <PublicRoute exact path="/register" component={RegisterSwitch} />
                    <PublicRoute exact path="/register/aluno" component={RegisterStudent} />
                    <PublicRoute exact path="/register/professor" component={RegisterTeacher} />
                    <PublicRoute exact path="/password-redefine" component={PasswordRedefine} />
                    <PublicRoute exact path="/password-redefine/teacher" component={PasswordRedefineTeacher} />
                    <PublicRoute exact path="/forgot-password" component={ForgotPassword} />
                    <PublicRoute exact path="/forgot-password/teacher" component={ForgotPasswordTeacher} />
                    {/* <Route exact path="/home-page/teacher" component={HomePageTeacher} /> */}
                    <Route exact path="/logout" render={() => {
                        userActions.logout();
                        professorActions.logout();
                    }} />
                    <PrivateRoute path="/account" component={this.userRole === 1 ? UserSettings : ProfessorSettings} />
                    <PrivateRoute exact path="/perfil" component={this.userRole === 1 ? UserPerfil : ProfessorPerfil} />
                    <Route exact path="/perfil/:studentId" component={StudentPerfil} />
                    <Route exact path="/tutorials" component={ListTutorials} />
                    <Route exact path="/admin" component={(localStorage.getItem("admin") ? AdminArea : LoginAdmin)} />
                    <Route exact path="/tutorials/:url" component={ViewTutorial} />
                    <Route exact path="/under-construction" component={UnderConstruction} />
                    <PrivateRouteStudent exact path="/projects/:url/edit" component={EditProject} />
                    <PrivateRouteStudent exact path="/new-project" component={CreateProject} />
                    <PrivateRouteStudent exact path="/cronograma" component={ListarCronograma} />
                    <PrivateRouteProfessor exact path="/professor-area" component={TeacherArea}/>
                    <PrivateRouteProfessor exact path="/professor-area/cronograma" component={isMobile ? NoMobileSupport : ManipulateCronograma} />
                    <PrivateRouteProfessor exact path="/professor-area/relatorio" component={GenerateReports} />
                    <Route component={NoMatch} />
                </Switch>
            </Router>
        )
    }
}


function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}
const connectedRouterPage = connect(mapStateToProps)(RouterComponent);
export { connectedRouterPage as RouterComponent };