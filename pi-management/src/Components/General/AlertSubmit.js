import React from 'react';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { Button } from 'react-materialize';

export const AlertSubmit = (functionConfirm) => {
    confirmAlert({
        customUI: ({ onClose }) => {
            return (
                <div
                    key={Math.floor(100 + Math.random() * 900)}
                    className="card teal lighten-2 white-text"
                >
                    <div className="card-content">
                        <span className="card-title"><b>Confirmar</b></span>
                        <p className="white-text">Você tem certeza que deseja realizar essa ação?</p>
                    </div>
                    <div className="card-actions">
                        <Button
                            key={Math.floor(10 + Math.random() * 900)}
                            className="white-text" flat onClick={onClose}>Cancelar</Button>
                        <Button
                            key={Math.floor(10 + Math.random() * 900)}
                            className="white-text" flat onClick={() => {
                                functionConfirm();
                                onClose();
                            }}>Sim</Button>
                    </div>
                </div>
            )
        },
    })
};