import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('user') || localStorage.getItem('professor')
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login/aluno', state: { from: props.location } }} />
    )} />
)

export const PrivateRouteProfessor = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('professor')
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login/professor', state: { from: props.location } }} />
    )} />
)

export const PrivateRouteStudent = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('user')
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login/aluno', state: { from: props.location } }} />
    )} />
)

export const PublicRoute = ({ component: Component, ...rest}) => (
    <Route {...rest} render={props => (
        (localStorage.getItem('user') === null && localStorage.getItem('professor') === null)
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/', state: { from: props.location } }} />
    )} />
)