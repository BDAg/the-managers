import React from 'react';
import { Row } from 'react-materialize';
import { Button, Icon } from 'react-materialize';
import { history } from '../../_helpers';
export const UnderConstruction = () => {
    return (
        <div style={{ height: "87vh" }} className="valign-wrapper">
            <Button
                style={{top: "0", marginTop: "10vh" ,position:"fixed"}}
                className="teal lighten-2"
                floating
                icon={<Icon className="white-text">arrow_back</Icon>}
                onClick={() => history.goBack()}
            />
            <Row xl={12} l={12} m={12} s={12} className="center">
                <img src={"http://bestanimations.com/Science/Gears/loadinggears/loading-gears-animation-13-3.gif"} alt="under-construction" width="50px" height="50px"/>
                <br/>
                <span className="center flow-text">
                    Página em construção
                </span>
            </Row>
        </div>
    );
}