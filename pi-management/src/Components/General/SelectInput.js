import React, { Component } from 'react';
import { Select } from 'react-materialize';

export const SelectInputJob = ({ name, size, handleChange }) => {
    return (
        <div>
            <Select s={9} m={9} l={9} xl={9} label={name} defaultValue={3} required onChange={handleChange} name="job">
                <option value="" disabled> Escolha a sua {name.toLowerCase()}</option>
                <option value={3}>Product Owner</option>
                <option value={2}>Scrum Master</option>
            </Select>
        </div>
    )
}

export const SelectSemester = ({ handleChange, size, label, disabled = false, defaultV = 1 }) => {
    return (
        <div>
            <Select s={9} m={9} l={9} xl={9} label={label || "Semestre"} defaultValue={defaultV} required onChange={handleChange} name="semester" disabled={disabled} >
                <option value={1}> 1º Termo </option>
                <option value={2}> 2º Termo </option>
                <option value={3}> 3º Termo</option>
                <option value={4}> 4º Termo </option>
                <option value={5}> 5º Termo </option>
                <option value={6}> 6º Termo </option>
            </Select>
        </div>
    )
}

export const SelectNivelTecnology = ({ handleChange, size, helperText = true }) => {
    return (
        <div>
            <Select s={12} m={12} l={12} xl={12} label="Nivel das tecnologias selecionadas" defaultValue={1} required onChange={handleChange} name="level">
                <option value={1}> Ouvi falar </option>
                <option value={2}> Entendi </option>
                <option value={3}> Sei fazer com ajuda</option>
                <option value={4}> Sei fazer sozinho </option>
                <option value={5}> Sei ensinar </option>
                <option value={6}> Sei criar </option>
            </Select>
            {helperText && <span className="helper-text red-text"><b>Atenção</b>: esse nível valerá para todas as tecnologias selecionadas<br /> Limite de <b>6</b> tecnologias por vez</span>}
        </div>
    )
}

export const SelectFilterProjects = ({ handleChange }) => {
    return (
        <div>
            <Select s={12} m={12} l={12} xl={12} defaultValue="0" required onChange={handleChange} name="filterProject">
                <option value="0"> Todos </option>
                <option value="1"> Em Andamento </option>
                <option value="2"> Encerrados </option>
                <option value="3"> A -> Z </option>
                <option value="4"> Z -> A </option>
            </Select>
        </div>
    )
}

export class SelectInputCities extends Component {

    constructor(props) {
        super(props);
        this.handleChange = this.props.handleChange;
        this.cities = this.props.cities ? this.props.cities : [];
    }

    render() {

        let citiesElement = this.cities.length > 0 ? this.cities.map((item) => {
            return <option value={item.id} key={item.id}>{item.name}</option>;
        }) : NaN;

        return (
            <div>
                <div className={`input-field col s9 m9 l9 xl9`} >
                    <Select s={12} m={12} l={12} xl={12} label="Cidade" defaultValue={0} required onChange={this.handleChange} name="CityId">
                        <option value={0} disabled>Escolha a sua cidade</option>
                        {citiesElement}
                    </Select>
                </div>
            </div>
        );
    }
}
