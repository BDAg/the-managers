import React from 'react';
import './index.css';
import { Notifications } from '..';
import { Link } from 'react-router-dom';
import { Navbar, Icon, SideNavItem, SideNav, Button } from 'react-materialize';
import { apiUrl } from '../../_helpers/api-url';

export class TopNavigator extends React.Component {
    constructor(props) {
        super(props);
        this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : false;
        this.professor = localStorage.getItem('professor') ? JSON.parse(localStorage.getItem('professor')) : false;
        this.state = {
            notifications: localStorage.getItem("notifications") ? localStorage.getItem("notifications").length > 2 : false
        };
        this.socket = this.props.socket;
        if (this.user) {
            this.socket.on('notification', (notification) => {
            });
            this.socket.emit('subscriptNotification', 5000, this.user.data.id);
        }
    }

    render() {
        const { notifications } = this.state;
        return (
            <Navbar
                className=" teal lighten-2"
                brand={
                    <Link to="/" className="brand-logo logo left hide-on-med-and-down"><span className="flow-text">PI Management</span></Link>
                }
                menuIcon=
                {
                    <Icon center>menu</Icon>
                }
                alignLinks="right"
                sidenav={
                    <SideNavMenu user={this.user} professor={this.professor} notifications={notifications} />
                }
            >
                <ul id="dropdown-perfil" className="dropdown-content">
                    <li><Link to="/perfil">Ver perfil</Link></li>
                    <li><Link to="/account">Settings</Link></li>
                    <li className="divider"></li>
                    {
                        this.professor &&
                        <React.Fragment>
                            <li><Link to="/professor-area">Área do Professor</Link></li>
                            <li className="divider"></li>
                        </React.Fragment>
                    }
                    <li><Link to="/logout">Sair</Link></li>
                </ul>
                <ul id="dropdown-login" className="dropdown-content">
                    <li><Link to="/login/aluno">Aluno</Link></li>
                    <li className="divider"></li>
                    <li><Link to="/login/professor">Professor</Link></li>
                    <li className="divider"></li>
                    <li><Link to="/register">Cadastro</Link></li>
                </ul>
                <ul id="dropdown-notifications" className="dropdown-content">
                    {this.user && <Notifications />}
                </ul>
                {
                    this.user &&
                    <a className='dropdown-trigger' href='#!' data-target='dropdown-notifications'>
                        <i className="material-icons" style={{ marginLeft: "300px" }}>{notifications ? "notifications_active" : "notifications_off"}</i>
                    </a>
                }
                <Link to="/">
                    Início
                </Link>
                <Link to="/projects">
                    Projetos
                </Link>
                <Link to="/tutorials">
                    Tutoriais
                </Link>
                {this.user &&
                    <Link to="/cronograma">
                        Cronograma
                    </Link>
                }

                {
                    !this.user ?

                        !this.professor ?
                            <a className="dropdown-trigger" href="#!" data-target="dropdown-login">
                                Login
                        <i className="material-icons right">arrow_drop_down</i>
                            </a>
                            :
                            <a className="dropdown-trigger" href="#!" data-target="dropdown-perfil">
                                Perfil
                        <i className="material-icons right">arrow_drop_down</i>
                            </a>
                        :
                        <a className="dropdown-trigger" href="#!" data-target="dropdown-perfil">
                            Perfil
                        <i className="material-icons right">arrow_drop_down</i>
                        </a>
                }
                {localStorage.getItem("admin") &&
                    <Link to="/admin">
                        Área do administrador
                    </Link>
                }
            </Navbar>
        )
    }
}


const SideNavMenu = ({ user, professor, notifications }) => {
    let urlPhotos = apiUrl+"/photos/";
    return (
        <div>
            {user &&
                <SideNavItem userView user={{
                    background: 'https://i.ytimg.com/vi/12g2t7G54nA/maxresdefault.jpg',
                    image: user.data.photo && urlPhotos + user.data.photo.path,
                    name: user.data.name,
                    4: <SideNavNotifications notifications={notifications} />
                }} />
            }
            {professor &&
                <SideNavItem userView user={{
                    background: 'https://i.ytimg.com/vi/12g2t7G54nA/maxresdefault.jpg',
                    image: professor.data.photo && urlPhotos + professor.data.photo.path,
                    name: professor.data.name,
                }} />
            }
            {user && <SideNavNotifications notifications={notifications} />}
            <SideNavItem href="/">
                Início
            </SideNavItem>
            <SideNavItem href="/projects">
                Projetos
            </SideNavItem>
            <SideNavItem href="/tutorials">
                Tutoriais
            </SideNavItem>
            {
                localStorage.getItem("admin") && <SideNavItem href="/admin">
                    Área do administrador
                </SideNavItem>
            }
            {user
                ?
                <div>
                    <SideNavItem href="/perfil">
                        Ver Perfil
                    </SideNavItem>
                    <SideNavItem href="/account">
                        Settings
                    </SideNavItem>
                    <SideNavItem href="/logout">
                        Sair
                    </SideNavItem>
                </div>
                :
                professor ?
                    <div>
                        <SideNavItem href="/perfil">
                            Ver Perfil
                    </SideNavItem>
                        <SideNavItem href="/account">
                            Settings
                    </SideNavItem>
                        <SideNavItem href="/professor-area">
                            Área do professor
                    </SideNavItem>
                        <SideNavItem href="/logout">
                            Sair
                    </SideNavItem>
                    </div>
                    :
                    <div>
                        <SideNavItem href="/login/aluno">
                            Login como aluno
                        </SideNavItem>
                        <SideNavItem href="/login/professor">
                            Login como professor
                    </SideNavItem>
                        <SideNavItem href="/register">
                            Cadastrar
                    </SideNavItem>
                    </div>
            }
        </div>

    )
}

const SideNavNotifications = ({ notifications }) => {
    let isHidden = true;

    function showThis() {
        document.querySelector(".tab-notification-mobile").hidden = !isHidden;
        isHidden = !isHidden
    }

    return (
        <SideNav
            className="tab-notification-mobile"
            trigger={
                <Button flat onClick={showThis}>  <span><i className="material-icons teal-text valign-wrapper right">{notifications ? "notifications_active" : "notifications_off"}</i>Você {!notifications && "não"} possui notificações </span> </Button>
            }
        >
            <Button onClick={showThis} icon={<Icon>close</Icon>}></Button>
            <Notifications />
        </SideNav>
    )
}