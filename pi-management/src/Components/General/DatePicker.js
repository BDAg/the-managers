import React from 'react';

export const DatePicker = ({name, idField, size, value, handleChange}) => {
    return (
        <div className={`input-field col ${size}`}>
            <input name={idField} type="text" className="datepicker validate" value={value} onChange={handleChange} id={idField} required />
            <label htmlFor={idField}>{name}</label>
            <span className="helper-text grey-text">Clique para selecionar a data</span>
        </div>

    )
}