import React from 'react';
import { Row } from 'react-materialize';
import { Button, Icon } from 'react-materialize';
import { history } from '../../_helpers';
export const NoMatch = () => {
    return (
        <div style={{ height: "87vh" }} className="valign-wrapper">
            <Button
                style={{top: "0", marginTop: "10vh" ,position:"fixed"}}
                className="teal lighten-2"
                floating
                icon={<Icon className="white-text">arrow_back</Icon>}
                onClick={() => history.goBack()}
            />
            <Row xl={12} l={12} m={12} s={12}>
                <span className="center flow-text">
                    <h4>Erro - 404</h4>
                    Página não encontrada :(
                </span>
            </Row>
        </div>
    );
}