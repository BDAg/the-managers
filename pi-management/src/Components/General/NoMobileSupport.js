import React from 'react';
import { Row } from 'react-materialize';
export const NoMobileSupport = () => {
    return (
        <div style={{ height: "87vh" }} className="valign-wrapper">
            <Row xl={12} l={12} m={12} s={12}>
                <h4 className="center">Oh! Que pena!!!</h4>

                <p className="center flow-text">
                    Essa página ainda não suporta mobile :(
                    <br />
                    Tente mudar seu navegador para modo Desktop
                </p>
            </Row>
        </div>
    );
}