import React from 'react';
import { TextInput } from 'react-materialize';

export const InputField = ({ name, idField, type, size, value, handleChange, helperText, textHelper, required }) => {
    return (
        <div className={`input-field col ${size}`}>
            <input name={idField} value={value || ""} onChange={handleChange} id={idField} type={type} className='validate' required={required} />
            <label htmlFor={idField}>{name}</label>
            {helperText && <span className="helper-text grey-text">url do {textHelper}</span>}
        </div>
    )
}

export const CronoInputField = ({ name, idField, type, size, value, handleChange, helperText, textHelper, required }) => {
    return (
        <div className={`col ${size}`}>
            <TextInput
                label={name}
                l={12}
                xl={12}
                m={12}
                s={12}
                id={idField}
                type={type}
                onChange={handleChange}
                required={required}
                value={value}
            />
            {helperText && <span className="helper-text grey-text">url do {textHelper}</span>}
        </div>
    )
}