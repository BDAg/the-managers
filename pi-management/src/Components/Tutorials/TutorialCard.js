import React from 'react';
import { CardPanel, Icon, Col } from 'react-materialize';

export default class TutorialCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        const { name, description, url, video, content } = this.props;

        return (
            <a style={{ color: "black" }} href={`/tutorials/${url}`}>
                <CardPanel>
                    <Col>
                        <h5>{name}</h5>
                        <p>
                            {description}
                        </p>
                        <Col s={12} m={12} xl={12} l={12}>
                            <Icon className={video ? "green-text": "red-text" }>ondemand_video</Icon>
                            <Icon className={content ? "green-text": "red-text" }>description</Icon>
                        </Col>
                    </Col>
                </CardPanel>
            </a>
        )
    }
}