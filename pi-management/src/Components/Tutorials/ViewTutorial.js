import React from 'react';
import { Row, ProgressBar, Col } from 'react-materialize';
import { tutorialServices } from '../../_services';
import { Editor as EditorArea, EditorState, convertFromRaw } from 'draft-js';
import YouTube from 'react-youtube';
import Moment from 'moment';
import { isMobile } from 'react-device-detect';

class ViewTutorial extends React.Component {
    constructor(props) {
        super(props);
        this.urlProject = this.props.match.params.url;

        this.state = {
            dataProject: [],
            gotData: false,
        }
    }
    componentDidMount() {
        tutorialServices.view(this.urlProject).then(response => {
            this.setState({
                dataProject: response,
                gotData: true
            });
        });
    }

    render() {
        const { dataProject, gotData } = this.state;
        if (gotData) {
            Moment.locale("pt-br");
            let date = dataProject.createAt;
            let dataProjectContent = dataProject.content ? EditorState.createWithContent(convertFromRaw(JSON.parse(dataProject.content))) : false
            let videoId = dataProject.video ? dataProject.video.split("watch?v=")[1] : false

            return (
                <Row style={!isMobile ? { padding: "4%" } : { padding: "1%" }}>
                    <Col className="center" xl={12} m={12} l={12} s={12} >
                        <h4>{dataProject.title}</h4>
                        <p>{dataProject.description}</p>
                        {videoId &&
                            <YouTube
                                videoId={videoId}
                                origin={window.location.host}
                                opts={
                                    isMobile ? {
                                        width: "100%",
                                        origin: window.location.host,
                                        enablejsapi: 1
                                    } : {
                                            origin: window.location.host,
                                            enablejsapi: 1
                                        }
                                }
                            />
                        }
                    </Col>

                    {dataProjectContent &&
                        <Col xl={12} m={12} l={12} s={12}>
                            <EditorArea readOnly editorState={dataProjectContent} />
                        </Col>
                    }
                    <Row className="right">
                        <br />
                        <br />
                        <span>Postado{dataProject.Administrator && ` por ${dataProject.Administrator.name}`} em {Moment(date).format("D MMM YYYY")}</span>
                    </Row>
                </Row>
            )
        } else {
            return (
                <Col s={12}>
                    <ProgressBar />
                </Col>
            )
        }

    }
}

export default ViewTutorial;