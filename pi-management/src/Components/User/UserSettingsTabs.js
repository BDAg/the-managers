import React from 'react';
import './index.css';
import { Tab, Tabs, Row } from 'react-materialize';
import { UploadPhoto, MatrixSettings, ChangePassword, EditData } from '.';

export const UserSettingsTabs = ({ dispatch }) => {

    return (
        <div className="row ">
            <div className="col s12 ">
                <Tabs className="tabs-fixed-width">
                    <Tab title="Alterar senha">
                        <Row>
                            <div className="col s3 l3 xl3">

                            </div>
                            <ChangePassword />
                        </Row>
                    </Tab>

                    <Tab title="Alterar foto">
                        <Row />
                        <UploadPhoto />
                    </Tab>

                    <Tab title="Alterar dados">
                        <Row />
                        <EditData dispatch={dispatch} />
                    </Tab>

                    <Tab active={window.location.hash==="#editmatrix"} title="Matriz de Habilidade">
                        <Row />
                        <MatrixSettings />
                    </Tab>
                </Tabs>
            </div>
        </div>
    );
}