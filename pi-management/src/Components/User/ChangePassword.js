import React from 'react';
import { InputField } from '..'
import { toast } from '../../_helpers';
import { studentServices } from '../../_services'

class ChangePassword extends React.Component {
    constructor(props) {
        super(props);
        this.token = JSON.parse(localStorage.getItem("user")).token
        this.state = {
            oldPassword: '',
            password: '',
            passwordConfirm: '',
            validForm: false
        }
        this.onFormSubmt = this.onFormSubmt.bind(this);
        this.onChange = this.onChange.bind(this)
    }

    async onFormSubmt(e) {
        e.preventDefault();
        const { oldPassword, password, passwordConfirm } = this.state;

        if (password !== passwordConfirm) {
            toast("Senhas diferentes!");
        } else {
            this.setState({
                validForm: false
            });
            await studentServices.changePassword(oldPassword, password, this.token, toast);
        }
    }

    onChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;

        this.setState(data, () => {
            let { oldPassword, password, passwordConfirm } = this.state;

            let isValid = password.length >= 6 && passwordConfirm.length >= 6 && oldPassword.length >= 6;

            this.setState({
                validForm: isValid
            });
        });
    }

    render() {
        let customSize = 's9 m9 l9 xl9'
        return (
            <form className="col s9 m9 l9 xl9" onSubmit={this.onFormSubmt}>
                <InputField
                    name="Senha Atual"
                    type="password"
                    idField="oldPassword"
                    size={customSize}
                    value={this.state.oldPassword}
                    handleChange={this.onChange}
                    required={true}
                />

                <InputField
                    name="Nova Senha"
                    type="password"
                    idField="password"
                    size={customSize}
                    value={this.state.password}
                    handleChange={this.onChange}
                    required={true}
                />

                <InputField
                    name="Confirmar Nova Senha"
                    type="password"
                    idField="passwordConfirm"
                    size={customSize}
                    value={this.state.passwordConfirm}
                    handleChange={this.onChange}
                    required={true}
                />
                <div className={`col ${customSize}`}>
                    <button className="btn waves-effect waves-light" type="submit" disabled={!this.state.validForm}>Salvar
                                    <i className="material-icons right">send</i>
                    </button>
                </div>
            </form>
        )
    }
}


export default ChangePassword;
