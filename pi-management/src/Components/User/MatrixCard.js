import React from 'react';
import { levels, toast } from '../../_helpers';
import { studentServices } from '../../_services'
import { SelectNivelTecnology } from "..";
import { Modal, Row } from 'react-materialize';

export const MatrixCard = ({ data }) => {

    async function removeTechnology() {
        await studentServices.removeTechnology(data.SkillsMatrices.TechnologyId, data.name, toast);
        document.querySelector(".tech" + data.id).remove();
    }
    return (
        <div className={`col s12 l3 xl3 m6 tech${data.id}`}>
            <div className="card">
                <div className="card-content">
                    <span ><b>{data.name}</b></span>
                    <button onClick={removeTechnology} className="btn-floating btn-small waves-effect waves-light red right"><i className="material-icons">clear</i></button>
                    <br />
                    <span>{levels[data.SkillsMatrices.level]}</span>
                </div>
                <div className="card-action">
                    <Row s={12} m={12} xl={12} l={12} >
                        <a className="btn-flat waves-effect waves-light black white-text right modal-trigger" href={`#edit${data.id}`}>Editar</a>
                    </Row>
                </div>
            </div>
            <EditModal technology={data} />
        </div>
    );
}

class EditModal extends React.Component {
    // let token = JSON.parse(localStorage.getItem("user")).token;

    constructor(props) {
        super(props);
        this.technology = this.props.technology;
        this.state = {
            level: this.technology.level || 1,
            submited: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();

        this.setState({
            submited: true
        });

        const { level } = this.state;
        if (level !== this.technology.SkillsMatrices.level) {
            await studentServices.editTechnology(this.technology.SkillsMatrices.TechnologyId, level, toast);
        } else {
            toast("Você não alterou o nível da tecnologia");
        }

        window.location.reload();
    }

    handleChange(event) {
        this.setState({
            level: event.target.value
        });
    }

    render() {
        const technology = this.technology;
        const { submited } = this.state;
        let customSize = "s12 m12 l12 xl12"
        return (
            <Modal id={`edit${technology.id}`}
                bottomSheet
                className="modal bottom-sheet"
                actions={[
                    <a href="#!" className="modal-close waves-effect waves-green btn-flat">Fechar</a>

                ]} >
                <div>
                    <div className={`row form-edit ${customSize} center`}>
                        <span>Editar {technology.name}</span>
                    </div>
                    <div className={`row ${customSize} `} >
                        <form className={`row ${customSize} `} onSubmit={this.handleSubmit}>
                            <SelectNivelTecnology handleChange={this.handleChange} helperText={false} size={customSize} />
                            <button className="btn-large waves-effect waves-light teal right" disabled={submited}>
                                <span>Salvar<i className="material-icons right">send</i></span>
                            </button>
                        </form>
                    </div>
                </div>
            </Modal>
        );
    }
}    