import React from 'react';
import { MatrixCard } from '.';
import { SelectNivelTecnology } from '../General'
import { studentServices } from '../../_services';
import axios from 'axios';
import M from 'materialize-css';
import { connect } from 'react-redux';
import { toast } from '../../_helpers';
import { apiUrl } from '../../_helpers/api-url';

class MatrixSettings extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            matrix: JSON.parse(localStorage.getItem("user")).data.matrix,
            technolgies: [],
            level: 1,
            techLength: 0
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.mountChips = this.mountChips.bind(this);
    }

    async mountChips() {
        let technologiesRequest = await axios({ method: "GET", url: apiUrl+"/technology/list" });
        let technologies = technologiesRequest.data.response.data;
        let technologiesInfos = {}
        let chipsOption = { data: {} }

        Object.keys(technologies).forEach(key => {
            let inMatrix = false;

            Object.keys(this.state.matrix).forEach(keyM => {
                if (this.state.matrix[keyM].name === technologies[key].name) {
                    inMatrix = true;
                }
            });
            if (!inMatrix) {
                technologiesInfos[technologies[key]["name"]] = technologies[key]
                chipsOption["data"][technologies[key]["name"]] = null;
            }
            console.log(chipsOption);
        });

        var elems = document.querySelectorAll('.chips');
        this.chips = M.Chips.init(elems, {
            autocompleteOptions: chipsOption,
            limit: 6,
            placeholder: "Tecnologias",
            onChipAdd: (el, chip) => {
                let tech = chip.textContent.replace("close", "");
                if (technologiesInfos[tech]) {
                    this.state.technolgies.push(technologiesInfos[tech]);
                    this.setState({
                        techLength: this.state.technolgies.length
                    });
                }
                else {
                    chip.remove();
                    M.toast({
                        html: `
                                        <span>Tecnologia não encotrada</span>
                                        `
                        , displayLength: 5000
                    })
                }
            },
            onChipDelete: (el, chip) => {
                let tech = chip.textContent.replace("close", "");
                if (technologiesInfos[tech]) {
                    for (let each in this.state.technolgies) {
                        if (this.state.technolgies[each].id === technologiesInfos[tech].id) {
                            this.state.technolgies.splice(each, 1);
                            this.setState({
                                techLength: this.state.technolgies.length
                            });
                        }
                    }
                }
            }
        });
    }
    async componentDidMount() {
        this.mountChips();
    }

    handleChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;
        this.setState(data);
    }

    async handleSubmit(event) {
        this.setState({
            techLength: 0
        });

        let { technolgies, level } = this.state;

        event.preventDefault();

        for (let key in technolgies) {
            await studentServices.addTechnology(level, technolgies[key].id, technolgies[key].name, toast)
        }

        this.setState({
            matrix: JSON.parse(localStorage.getItem("user")).data.matrix,
            technolgies: []
        });

        this.mountChips();
    }

    render() {

        const { matrix } = this.state;
        return (
            <div className="row s12 m12 l12 xl12">
                <div className="row s12 m12 l12 xl12">
                    <div className="col s12 m12 l12 xl12 center-align">
                        <h5 >Adicionar tecnologias</h5>
                        <span> Você precisa adicionar ao menos 10 tecnologias para exibir sua matriz de habilidades em seu perfil </span>
                    </div>
                    <div className="col m1 s1 l3 xl3">

                    </div>
                    <div>
                        <form className="col m11 s11 l7 xl7 left-align" onSubmit={this.handleSubmit}>
                            <div className="chips chips-autocomplete m11 s11 l8 xl8"></div>
                            <SelectNivelTecnology handleChange={this.handleChange} size="m12 s12 l12 xl12" />
                            <button className="btn-large waves-effect waves-light teal" disabled={this.state.techLength < 1}>
                                <span>Adicionar<i className="material-icons right">send</i></span>
                            </button>
                        </form>
                    </div>

                </div >
                <br />
                <div className="row s12 m12 l12 xl12">
                    <h5>Matriz Atual</h5>
                    {matrix.length > 0 ?
                        Object.keys(matrix).map((key) => {
                            return (
                                <MatrixCard data={matrix[key]} key={key} />
                            )
                        }) : <h6 className="grey-text">
                            Você ainda não adicionou nenhuma Tecnologia :(
                    </h6>}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedRouterPage = connect(mapStateToProps)(MatrixSettings);
export { connectedRouterPage as MatrixSettings };