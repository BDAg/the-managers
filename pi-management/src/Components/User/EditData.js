import React from 'react';
import { userActions } from '../../_actions';

import {
    InputField,
} from '..';

import { toast } from '../../_helpers';

export default class UpdateData extends React.Component {
    constructor(props) {
        super(props);
        let userData = JSON.parse(localStorage.getItem("user"));
        this.token = userData.token;

        this.state = {
            name: userData.data.name || '',
            phone: userData.data.phone || '',
            semester: userData.data.semester || '',
            email: userData.data.email || '',
            validForm: true
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;

        this.setState(data, () => {
            let { email, phone, semester, name } = this.state;

            let isValid = email.length > 5 || semester.length >= 1 || name >= 4 || phone >= 8;

            this.setState({
                validForm: isValid
            });
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({
            validForm: false
        });

        let { email, phone, semester, name } = this.state;

        let data = {
            name: name,
            semester: semester,
            phone: phone,
            email: email,
        };

        this.props.dispatch(userActions.updateUser(data, this.token, toast));
    }

    render() {
        let customSize = 's9 m9 l9 xl9'

        return (
            <div id="editaccount" className="row s12 m12 l12 xl12">
                <div className="col s3 l3 xl3">

                </div>
                <form className="col s9 m9 l9 xl9" onSubmit={this.handleSubmit}>
                    <InputField
                        name="Nome"
                        size={customSize} idField="name"
                        type="text"
                        value={this.state.name}
                        handleChange={this.handleChange}
                        required={false} />

                    <InputField
                        name="E-mail"
                        type="text"
                        idField="email"
                        size={customSize}
                        value={this.state.email}
                        handleChange={this.handleChange}
                        required={false}
                    />

                    <InputField
                        name="Semestre"
                        size={customSize}
                        idField="semester" type="text"
                        value={this.state.semester}
                        handleChange={this.handleChange}
                        required={false} />

                    <InputField
                        name="Celular"
                        type="text"
                        idField="phone"
                        size={customSize}
                        value={this.state.phone}
                        handleChange={this.handleChange}
                        required={false}
                    />

                    <div className={`col ${customSize}`}>
                        <button className="btn waves-effect waves-light" type="submit" disabled={!this.state.validForm}>Salvar
                                    <i className="material-icons right">send</i>
                        </button>
                    </div>

                </form>
            </div>
        )
    }
}