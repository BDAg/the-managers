import React from 'react';
import FileBase64 from 'react-file-base64';
import { studentServices } from '../../_services';
import { toast } from '../../_helpers';
import { Row } from 'react-materialize';
import { apiUrl } from '../../_helpers/api-url';

class UploadPhoto extends React.Component {
    constructor(props) {
        super(props);
        this.user =  JSON.parse(localStorage.getItem("user"))
        this.token = this.user.token;
        this.state = {
            file: null,
            validForm: false
        }
        this.onFormSubmt = this.onFormSubmt.bind(this);
        this.onChange = this.onChange.bind(this)
    }

    onFormSubmt(e) {
        e.preventDefault();
        this.setState({
            validForm: false
        })

        const file = this.state.file;
        studentServices.changePhoto(file, this.token, toast);
    }

    onChange(file) {
        this.setState({ file: file, validForm: true });
    }

    render() {
        let urlPhotos = apiUrl+"/photos/"
        let customSize = 's9 m9 l9 xl9'

        return (
            <form onSubmit={this.onFormSubmt} className="col s12 m12 l12 xl12" >
               <Row className="center">
                    <FileBase64 multiple={false} onDone={this.onChange} />
               </Row>
                <div className={`col ${customSize} right`}>
                    <br />  <img className="center" style={{maxHeight: "40vh", width: "auto"}} alt="Nova foto" src={ this.state.file ? this.state.file.base64 : ( this.user.data.photo ? urlPhotos+this.user.data.photo.path : "https://i.stack.imgur.com/Pwbuz.png")}/>
                </div>
                <div className={`col ${customSize} right`}>
                    <br />
                    <button className="btn waves-effect waves-light center-align" type="submit" disabled={!this.state.validForm}>Enviar
                                    <i className="material-icons right">send</i>
                    </button>
                </div>
            </form>
        )
    }
}
export default UploadPhoto;
