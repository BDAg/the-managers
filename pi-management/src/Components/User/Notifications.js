import React from 'react';
import axios from 'axios';
import { Loading } from '..'
import { studentServices } from '../../_services';
import { notifications as notificationsHelper } from '../../_helpers';
import { apiUrl } from '../../_helpers/api-url';

class Notifications extends React.Component {
    constructor(props) {
        super(props);
        this.user = JSON.parse(localStorage.getItem('user'));
        this.professor = JSON.parse(localStorage.getItem('professor'));
        this.state = {
            notifications: localStorage.getItem("notifications") ? JSON.parse(localStorage.getItem("notifications")) : false,
            anyRequest: false
        }
        this.acceptInvite = this.acceptInvite.bind(this);
        this.denyInvite = this.denyInvite.bind(this);
        this.makeAsRead = this.makeAsRead.bind(this);

    }
    async componentDidMount() {
        if (!this.professor) {
            await this.getNotifications();
        }
    }

    async getNotifications() {
        await axios({ method: "GET", url: `${apiUrl}/notification/list`, headers: { authorization: this.user.token } }).then(res => {
            localStorage.setItem("notifications", JSON.stringify(res.data.response));
            this.setState({
                notifications: res.data.response,
                anyRequest: false
            });
        }).catch(err => {
            console.log(err);
        })
    }

    async denyInvite(e) {
        this.setState({
            anyRequest: true
        });
        e.preventDefault();
        let projectId = e.target.projectId.value;
        let studentId = e.target.studentId.value;
        let notificationId = e.target.notificationId.value;

        await studentServices.denyInvite(projectId, studentId, notificationId, this.user.token);
        await this.getNotifications();
    }

    async acceptInvite(e) {
        this.setState({
            anyRequest: true
        });
        e.preventDefault();
        let projectId = e.target.projectId.value;
        let studentId = e.target.studentId.value;
        let notificationId = e.target.notificationId.value;

        await studentServices.acceptInvite(projectId, studentId, notificationId, this.user.token);
        await this.getNotifications();
    }

    async makeAsRead(e){
        this.setState({
            anyRequest: true
        });
        e.preventDefault();
        let notificationId = e.target.notificationId.value;

        await studentServices.hideNotification(notificationId, this.user.token);
        await this.getNotifications();
    }
    render() {
        const { notifications } = this.state;
        if (notifications) {

            return (
                <div>
                    {notifications.length > 0 ?
                        Object.keys(notifications).map(key => {
                            let notification = notifications[key];
                            return (
                                <li key={notification.id}>
                                    <div>
                                        <div className="card">
                                            <div className="card-stacked">
                                                <div className="card-content">
                                                    <p>{notification.message}</p>
                                                </div>
                                                <div className="card-action row">
                                                    {notification.type === notificationsHelper.invite &&
                                                        <div>
                                                            <form onSubmit={this.acceptInvite} className="col l6 m6 xl6 l6">
                                                                <input type="hidden" name="projectId" value={notification.ProjectId} />
                                                                <input type="hidden" name="studentId" value={notification.StudentId} />
                                                                <input type="hidden" name="notificationId" value={notification.id} />
                                                                <button className="btn" name="aceitar" disabled={this.state.anyRequest} type="submit">Aceitar</button>
                                                            </form>
                                                            <form onSubmit={this.denyInvite} className="col l6 m6 xl6 l6">
                                                                <input type="hidden" name="projectId" value={notification.ProjectId} />
                                                                <input type="hidden" name="studentId" value={notification.StudentId} />
                                                                <input type="hidden" name="notificationId" value={notification.id} />
                                                                <button className="btn" type="submit" disabled={this.state.anyRequest}>Recusar</button>
                                                            </form>
                                                        </div>
                                                    }
                                                    {(notification.type === notificationsHelper.agree || notification.type === notificationsHelper.deny) &&
                                                        <form onSubmit={this.makeAsRead} className="col l12 m12 xl12 l12">
                                                            <input type="hidden" name="notificationId" value={notification.id} />
                                                            <button className="btn-flat l12 m12 xl12 l12" name="aceitar" disabled={this.state.anyRequest} type="submit"><span>Marcar como lido</span></button>
                                                        </form>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            )
                        })
                        : <div className="center">
                            <span className="grey-text center"><br /><b>Nada por aqui :)</b><br /></span>
                        </div>}
                </div>
            )
        } else {
            return (
                <Loading />
            )
        }

    }
}

export default Notifications;
