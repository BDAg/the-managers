import React from 'react';
import { CollapsibleItem, Col, Row, Switch, Select } from 'react-materialize';
import Divider from 'react-materialize/lib/Divider';
export default class CollapsibleStudents extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            usersFilter: [],
            query: '',
            selected: 0
        }
        this.handleChange = this.props.handleChange;
        this.changeQuery = this.changeQuery.bind(this);
        this.filterSelect = this.filterSelect.bind(this);
        this.filterSemester = this.filterSemester.bind(this);
        this.studentFilter = this.studentFilter.bind(this);
    }

    changeQuery(value) {
        this.setState({
            query: value.target.value
        }, () => {
            return this.studentFilter();
        });
    }


    async studentFilter() {

        let { users } = this.props;
        new Promise((resolve) => {

            let data = users.filter((value) => {
                if (value.name.toString().toLowerCase().includes(this.state.query.toLowerCase())) {
                    return value;
                }
                return null;
            });
            resolve(data);

        }).then((value) => {
            this.setState({
                usersFilter: value
            });
        });
    }

    filterSemester(semester) {

        let { users } = this.props;
        new Promise((resolve) => {

            let data = users.filter((value) => {
                if (value.semester === semester) {
                    return value;
                }
                return null;
            });
            resolve(data);

        }).then((value) => {
            this.setState({
                usersFilter: value,
                selected: semester
            });
        });

    }

    async filterSelect(event) {
        let value = Number(event.target.value);

        switch (value) {
            case 0:
                this.setState({
                    query: '',
                    selected: 0,
                    usersFilter: this.props.users
                });
                break;

            case 1:
                this.filterSemester(1);
                break;

            case 2:
                this.filterSemester(2);
                break;

            case 3:
                this.filterSemester(3);
                break;
        
            case 4:
                this.filterSemester(4);
                break;

            case 5:
                this.filterSemester(5);
                break;
            
            case 6:
                this.filterSemester(6);
                break;

            case 7:
                this.filterSemester(7);
                break;

            default:
                this.setState({
                    query: '',
                    selected: 0,
                    usersFilter: this.props.users
                });
                break;
        }

    }


    render() {
        const { usersFilter, query } = this.state;
        let { users } = this.props;
        let usersToMap = this.state.selected !== 0 || query !== '' ? usersFilter : usersFilter.length > 0 ? users : users;
        // this.state.selected !== 0 ? usersFilter : 
        return (
            <CollapsibleItem header="Estudantes" onSelect={() => { }} icon={<i className="material-icons">person_pin_circle</i>}>
                <input id="search" value={this.state.query} onChange={this.changeQuery} type="text" className={"validate"} placeholder={'Buscar estudante'} />
                <Switch offLabel="Sem projeto" onLabel="" onChange={this.handleChange} disabled={query.length > 0 || this.state.selected !== 0} />
                <br />
                <label htmlFor="semesterFilter">Selecionar Termo:</label>
                <Select s={12} m={12} l={12} xl={12} 
                    defaultValue="0" required name="semesterFilter" 
                    id="semesterFilter" onChange={this.filterSelect}>
                    <option value="0"> Todos </option>
                    <option value="1"> 1º Termo </option>
                    <option value="2"> 2º Termo </option>
                    <option value="3"> 3º Termo </option>
                    <option value="4"> 4º Termo </option>
                    <option value="5"> 5º Termo </option>
                    <option value="6"> 6º Termo </option>
                    <option value="7"> Formados </option>
                </Select>
                <Divider />
                <Row s={12} m={12} l={12} xl={12} style={{ overflow: "auto", height: "300px" }}>
                    <Col s={12} m={12} l={12} xl={12}>
                        {usersToMap.length > 0 ? Object.keys(usersToMap).map(user => {
                            return (
                                <div className="card darken-1" key={user}>
                                    <div className="card-content">
                                        <b>{usersToMap[user].name} </b>
                                        <br />
                                        <span>{usersToMap[user].email}</span>
                                        <br />
                                        <span>{usersToMap[user].semester}º Termo</span>
                                    </div>
                                    <div className="card-action">
                                        <a href={'/perfil/' + usersToMap[user].email} className="teal-text">Ver perfil</a>
                                    </div>
                                </div>
                            )
                        }) : <span className="grey-text"> <br /> Não há alunos para serem mostrados no momento</span>}
                    </Col>
                </Row>
            </CollapsibleItem>
        )
    }
}