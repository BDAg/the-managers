export * from './UserSettingsTabs';
export { default as UploadPhoto } from './UploadPhoto';
export { default as ChangePassword } from './ChangePassword';
export { default as Notifications } from './Notifications';
export { default as CollapsibleStudents } from './CollapsibleStudents';
export * from './MatrixSettings';
export * from './MatrixCard';
export { default as EditData } from './EditData';