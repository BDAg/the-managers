import React from 'react';
import { Row, Col, Button, Icon, DatePicker } from 'react-materialize';
import { cronogramaServices } from "../../_services/cronograma.service";
import { dateoptions, toast } from "../../_helpers";
import { AlertSubmit } from "../General/AlertSubmit";
import { CronoInputField } from '../General';
import M from 'materialize-css';

class ListarItemCronograma extends React.Component {
    constructor(props) {
        super(props);
        this.token = JSON.parse(localStorage.getItem("professor")).token;
        this.topicId = this.props.topicId;
        this.state = {
            deleting: false,
            open: false,
            itemToDelete: 0,
            editingItem: false,
            isMounted: false,
            startsAt: this.props.item.startsAt,
            expireAt: this.props.item.expireAt,
            description: this.props.item.description || ""
        }

        this.handleChange = this.handleChange.bind(this);
        this.submitEdit = this.submitEdit.bind(this);
        this.deletarItem = this.deletarItem.bind(this);
        this.handleDate = this.handleDate.bind(this);
        this.handleExpireAt = this.handleExpireAt.bind(this);
    }

    componentDidMount() {
        this.setState({
            isMounted: true
        })
    }

    componentWillUnmount() {
        this.setState({
            isMounted: false
        })
    }

    handleChange(e) {
        this.setState({
            description: e.target.value
        });
    }

    async deletarItem() {
        this.setState({ deleting: true, open: false });
        await cronogramaServices.deletarItem(this.state.itemToDelete, this.token).then(() => {
            this.setState({ deleting: false });
            this.props.loadCronograma()
        }).catch(() => {
            toast("Erro ao deletar tópico", "red")
        });
    }

    async submitEdit(e) {
        e.preventDefault();
        const { description, expireAt, startsAt } = this.state;
        if (new Date(startsAt) > new Date(expireAt)) {
            toast("A data de entrega deve ser maior do que data de ínicio", "orange");
        } else {
            this.setState({
                submitted: true
            });

            let data = {
                itemId: this.props.item.id,
                token: this.token,
                description: description,
                expireAt: expireAt,
                startsAt: startsAt,
            }

            cronogramaServices.editItem(data).then(() => {
                this.props.loadCronograma();
                this.setState({
                    editingItem: false,
                    submitted: false
                });
            }).catch(err => {
                toast("Ocorreu um erro ao editar item: " + err.message, "red");
            });
        }
    }

    handleDate(data) {
        let date = new Date(data);
        this.setState({
            startsAt: date.toISOString()
        });
    }

    handleExpireAt(data) {
        let date = new Date(data);
        this.setState({
            expireAt: date.toISOString()
        });
    }

    render() {
        dateoptions["minDate"] = new Date("2019-01-01");
        let itemCrono = this.props.item;
        const { deleting, editingItem } = this.state;
        let expireAt = new Date(itemCrono.expireAt);
        let expireAtDate = `${expireAt.getDate() < 10 ? "0" + expireAt.getDate().toString() : expireAt.getDate()}/${expireAt.getUTCMonth() + 1 < 10 ? "0" + (expireAt.getUTCMonth() + 1).toString() : expireAt.getUTCMonth() + 1}/${expireAt.getFullYear()}`
        // let startsAt = new Date(itemCrono.startsAt);
        // let startsAtDate = `${startsAt.getDate() < 10 ? "0" + startsAt.getDate().toString() : startsAt.getDate()}/${startsAt.getUTCMonth() + 1}/${startsAt.getFullYear()}`
        return (
            <div>
                <Col className="white z-depth-1 valign-wrapper" style={{ borderBottom: "solid 1px #4db6ac" }} s={12} m={12} l={12} xl={12}>

                    <Col s={7} m={7} xl={6} l={6}>
                        <span className="left truncate">{itemCrono.description}</span>
                    </Col>
                    <Col s={5} m={5} xl={6} l={6} className="valign-wrapper">
                        <Col s={9} m={9} xl={8} l={8}>
                            <span className="right">{expireAtDate}</span>
                        </Col>
                        <Col s={3} m={3} xl={4} l={4}>
                            <Button
                                disabled={deleting}
                                onClick={(() =>
                                    this.setState({
                                        itemToDelete: itemCrono.id
                                    }, AlertSubmit(this.deletarItem))
                                )}
                                className="white right" floating small icon={<Icon small className="red-text">delete</Icon>} />
                            <Button
                                disabled={deleting}
                                className="white right"
                                onClick={() => {
                                    this.setState({
                                        editingItem: !editingItem
                                    })
                                }}
                                floating small icon={<Icon small className="green-text">edit</Icon>} />
                        </Col>
                    </Col>
                </Col>
                {editingItem &&
                    <div>
                        <Col className="white z-depth-3 valign-wrapper" style={{ borderBottom: "solid 1px #4db6ac" }} s={12} m={12} l={12} xl={12}>


                            <Row className="valign-wrapper" s={12} m={12} xl={12} l={12}>
                                <form onSubmit={this.submitEdit}>

                                    <CronoInputField
                                        name="Nome"
                                        idField="description"
                                        value={this.state.description}
                                        handleChange={this.handleChange}
                                        size="xl4 l4"
                                        required
                                    />

                                    <DatePicker
                                        value={this.state.startsAt}
                                        options={dateoptions}
                                        onChange={this.handleDate}
                                        placeholder="Data de início"
                                        onFocus={(((it) => M.Datepicker.getInstance(it.target).open()))}
                                    />

                                    <DatePicker
                                        value={this.state.expireAt}
                                        options={dateoptions}
                                        onChange={this.handleExpireAt}
                                        placeholder="Data de entrega"
                                        onFocus={(((it) => M.Datepicker.getInstance(it.target).open()))}
                                    />

                                    <Col m={6} s={6}>
                                        <Button className="left" icon={<Icon className="left">save</Icon>} disabled={this.state.submitted}>Salvar</Button>
                                    </Col>
                                </form>
                            </Row>

                        </Col>
                        <Button
                            floating
                            onClick={() => {
                                this.setState({
                                    editingItem: !editingItem
                                })
                            }}
                            className="white"
                            icon={<Icon className="teal-text">cancel</Icon>} disabled={this.state.submitted} />
                    </div>
                }
            </div>
        )
    }
}


// function formatDate(date) {
//     var day = date.getDate();
//     var monthIndex = parseInt(date.getMonth());
//     var year = date.getFullYear();

//     return day < 10 ? "0" + day.toString() + '/' + (monthIndex + 1).toString() + '/' + year : day + '/' + monthIndex + '/' + year;
// }

export default ListarItemCronograma;