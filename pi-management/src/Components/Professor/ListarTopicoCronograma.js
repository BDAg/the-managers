import AdcionarItem from "./AdicionarItem";
import React from 'react';
import { Row, Col, Button, Icon, Divider } from 'react-materialize';
import { cronogramaServices } from "../../_services/cronograma.service";
import { toast } from "../../_helpers";
import { AlertSubmit } from "../General/AlertSubmit";
import { CronoInputField } from "../General";
import ListarItemCronograma from "./ListarItemCronograma";

class ListarTopicoCronograma extends React.Component {

    constructor(props) {
        super(props);
        this.token = JSON.parse(localStorage.getItem("professor")).token;
        this.topicId = this.props.topicId;
        this.state = {
            editThis: false,
            deleting: false,
            open: false,
            itemToDelete: 0,
            editingTopic: false,
            isMounted: false,
            description: this.props.item.description || ""
        }

        this.deletarTopico = this.deletarTopico.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.submitEdit = this.submitEdit.bind(this)
        this.setEditThis = this.setEditThis.bind(this);
    }

    componentDidMount() {
        this.setState({
            isMounted: true
        })
    }

    componentWillUnmount() {
        this.setState({
            isMounted: false
        })
    }

    async deletarTopico() {
        this.setState({ deleting: true, open: false });
        cronogramaServices.deletarTopico(this.topicId, this.token).then(() => {
            this.setState({ deleting: false })
            toast("Tópico deletado", null, () => {
                this.props.loadCronograma()
            })
        }).catch(() => {
            toast("Erro ao deletar tópico", "red");
        });
    }

    handleChange(e) {
        this.setState({
            description: e.target.value
        });
    }

    async submitEdit(e) {
        e.preventDefault();
        cronogramaServices.editTopic(this.props.item.id, this.token, this.state.description).then(() => {
            this.props.loadCronograma();
            this.setState({
                editingTopic: false
            });
        }).catch(err => {
            toast("Ocorreu um erro ao editar item: " + err.message, "red");
        });
    }

    setEditThis(){
        if(this.state.isMounted){
            this.setState({ editThis: !this.state.editThis })
        } else {
            this.setEditThis()
        }
    }
    render() {
        let item = this.props.item;
        const { editThis, deleting, editingTopic, isMounted } = this.state;

        return (
            <Row className="center-align" s={12} m={12} xl={12} l={12}>
                {editingTopic && <div><br /><br /></div>}
                <Col s={12} m={12} l={2} xl={2} className="teal lighten-1 z-depth-1">
                    <Button
                        onClick={
                            (() => AlertSubmit(this.deletarTopico))
                        }
                        disabled={deleting}
                        flat><span className="white-text"><b>Excluir</b> <Icon className="red-text right">delete</Icon></span></Button>
                    <Divider />
                    <Button
                        onClick={
                            (() => { this.setState({ editingTopic: !editingTopic }) })
                        }
                        disabled={deleting}
                        flat><span className="white-text"><b>{editingTopic ? "Cancelar" : "Editar"}</b> <Icon className="white-text right">edit</Icon></span></Button>
                </Col>
                <Col s={12} m={12} l={8} xl={8} className="teal lighten-2 z-depth-2">
                    <Col className="white-text valign-wrapper" s={12} m={12} l={12} xl={12} style={{ height: "5vh" }}>
                        {editingTopic ?

                            <Col className="white z-depth-2" style={{ marginBottom: "5%" }} s={12} m={12} l={12} xl={12}>
                                <form onSubmit={this.submitEdit} className="valign-wrapper s12 m12 l12 xl12">
                                    <Col m={6} s={6} >
                                        <CronoInputField
                                            value={this.state.description}
                                            name="Nome do tópico"
                                            idField="description"
                                            required
                                            size="s12 m12 l12 xl12"
                                            handleChange={this.handleChange}
                                        />
                                    </Col>
                                    <Col m={6} s={6} className="right">
                                        <Button className="left" icon={<Icon className="left">save</Icon>} disabled={this.state.submitted}>Salvar</Button>
                                    </Col>
                                </form>
                            </Col>
                            :

                            <Col s={7} m={7} xl={6} l={6}>

                                <span className="left truncate"><b>{item.description}</b></span>
                            </Col>
                        }
                        <Col s={5} m={5} xl={6} l={6} className="valign-wrapper">
                            <Col s={9} m={9} xl={8} l={8}>
                                <span className="right"><b>Data de entrega</b></span>
                            </Col>
                            <Col s={3} m={3} xl={4} l={4}>
                                <span className="right"> <b>Ações</b></span>
                            </Col>
                        </Col>
                    </Col>
                    {isMounted && Object.keys(item.Cronogramas).map(keyCrono => {
                        let itemCrono = item.Cronogramas[keyCrono];
                        
                        return (
                            <ListarItemCronograma loadCronograma={this.props.loadCronograma} deletarItem={this.deletarItem} key={keyCrono} item={itemCrono}/>
                        )
                    })}

                    <Col className="white " style={{ borderBottom: "solid 1px teal lighten-2" }} s={12} m={12} l={12} xl={12}>
                        {isMounted && editThis &&
                            <AdcionarItem editThis={this.setEditThis} topicId={item.id} loadCronograma={this.props.loadCronograma} semester={this.props.semester} />
                        }
                        <Button waves={"green"} onClick={this.setEditThis} flat>{!editThis ? "Adicionar item" : "Cancelar"}</Button>
                    </Col>

                </Col>
            </Row>
        )
    }
}

export default ListarTopicoCronograma;