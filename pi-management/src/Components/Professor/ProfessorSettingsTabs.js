import React from 'react';
import { Tabs, Tab, Row } from 'react-materialize';
import ChangePasswordProfessor from './ChangePasswordProfessor';
import UploadPhotoProfessor from './UploadPhotoProfessor';
import EditDataProfessor from './EditDataProfessor';

export const ProfessorSettingsTabs = ({ dispatch }) => {

    return (
        <div>
            <div>
                <Tabs className="tabs-fixed-width">
                    <Tab title="Alterar senha">
                        <Row>
                            <div className="col s3 l3 xl3">

                            </div>
                            <ChangePasswordProfessor />
                        </Row>
                    </Tab>

                    <Tab title="Alterar foto">
                        <Row />
                        <UploadPhotoProfessor />
                    </Tab>

                    <Tab title="Alterar dados">
                        <Row />
                        <EditDataProfessor dispatch={dispatch} />
                    </Tab>
                </Tabs>
            </div>
        </div>
    )
}