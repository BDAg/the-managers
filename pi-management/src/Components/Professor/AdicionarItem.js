import React from 'react';
import { Row, Col, Button, Icon, DatePicker } from 'react-materialize';
import {
    dateoptions, toast,
} from '../../_helpers';
import M from 'materialize-css';
import { cronogramaServices } from '../../_services/cronograma.service';
import { CronoInputField } from '../General';

class AdcionarItem extends React.Component {

    constructor(props) {
        super(props);

        this.topicId = this.props.topicId;
        this.state = {
            editThis: false,
            description: "",
            startsAt: "",
            expireAt: "",
            submitted: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDate = this.handleDate.bind(this);
        this.handleExpireAt = this.handleExpireAt.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    async handleSubmit(e) {
        e.preventDefault()

        const { description, startsAt, expireAt } = this.state;
        if (new Date(startsAt) > new Date(expireAt)) {
            toast("A data de entrega deve ser maior do que data de ínicio", "orange");

        } else {
            this.setState({
                submitted: true
            });
            let data = {
                description: description,
                expireAt: expireAt,
                startsAt: startsAt,
                semester: this.props.semester,
                TopicsCronogramaId: this.topicId,
                token: JSON.parse(localStorage.getItem("professor")).token
            }

            await cronogramaServices.criarCronograma(data).then(async response => {
                if (response.status === 201) {
                    toast("Item adicionado")
                    await this.props.loadCronograma();
                }
            });
            this.setState({
                submitted: false
            });
            this.props.editThis();
        }

    }

    handleChange(e) {
        this.setState({
            description: e.target.value
        });
    }

    handleDate(data) {
        let date = new Date(data);
        this.setState({
            startsAt: date.toISOString()
        });
    }

    handleExpireAt(data) {
        let date = new Date(data);
        this.setState({
            expireAt: date.toISOString()
        });
    }

    render() {
        dateoptions["minDate"] = new Date("2019-01-01");

        return (
            <Row className="valign-wrapper" s={12} m={12} xl={12} l={12}>
                <form onSubmit={this.handleSubmit}>

                    <CronoInputField
                        name="Nome"
                        idField="description"
                        value={this.state.description}
                        handleChange={this.handleChange}
                        size="xl4 l4"
                        required
                    />

                    <DatePicker
                        options={dateoptions}
                        onChange={this.handleDate}
                        placeholder="Data de início"
                        onFocus={(((it) => M.Datepicker.getInstance(it.target).open()))}
                    />

                    <DatePicker
                        options={dateoptions}
                        onChange={this.handleExpireAt}
                        placeholder="Data de entrega"
                        onFocus={(((it) => M.Datepicker.getInstance(it.target).open()))}
                    />

                    <Col m={6} s={6}>
                        <Button className="left" icon={<Icon className="left">save</Icon>} disabled={this.state.submitted}>Salvar</Button>
                    </Col>
                </form>
            </Row>
        )
    }
}

export default AdcionarItem;