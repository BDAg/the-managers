export * from './ProfessorSettingsTabs';
export * from './ChangePasswordProfessor';
export { default as ManipulateCronograma } from './ManipulateCronograma';
export { default as GenerateReports } from './GenerateReports';