import React from 'react';
import { SelectSemester, CronoInputField } from '../General';
import { Row, Col, ProgressBar, Button, Icon } from 'react-materialize';
import { cronogramaServices } from '../../_services/cronograma.service';
import { toast } from '../../_helpers';
import ListarTopicoCronograma from './ListarTopicoCronograma';

class ManipulateCronograma extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            semester: 1,
            loadingCronograma: true,
            cronograma: [],
            topicName: "",
            creatingTopic: false,
            submitted: false
        }

        this.handleSemesterChange = this.handleSemesterChange.bind(this);
        this.listCronograma = this.listCronograma.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentDidMount() {
        this.setState({
            isMounted: true
        }, (() => this.listCronograma()))
    }

    componentWillUnmount() {
        this.setState({
            isMounted: false
        })
    }

    listCronograma() {
        if (this.state.isMounted) {
            this.setState(
                {
                    loadingCronograma: true
                },
            )
            cronogramaServices.listar(this.state.semester).then(
                (
                    response => this.setState(
                        {
                            cronograma: response,
                            loadingCronograma: false,
                            submitted: false,
                            creatingTopic: false,
                            topicName: ""
                        },
                    )
                )
            ).catch((err => toast("Erro ao listar cronograma " + err.toString(), "red")));
        }
    }

    async handleSemesterChange(e) {
        this.setState({
            semester: e.target.value,
            loadingCronograma: true
        }, () => this.listCronograma());
    }

    async handleSubmit(e) {
        e.preventDefault();

        const { semester, topicName } = this.state;
        this.setState({
            submitted: true
        })
        let data = {
            description: topicName,
            semester: semester,
            token: JSON.parse(localStorage.getItem("professor")).token
        }

        cronogramaServices.criarTopico(data).then(() => {
            this.listCronograma();
        }).catch(() => {
            toast("Erro ao adicionar tópico, tente novamente.", "red")
        });
    }

    render() {
        const { loadingCronograma, cronograma, semester, creatingTopic, isMounted } = this.state;

        return (
            <Row s={12} m={12} l={12} xl={12}>
                <br />
                <Col s={1} m={1} xl={1} l={1}>
                </Col>
                <SelectSemester
                    label="Selecione o termo"
                    handleChange={this.handleSemesterChange}
                    disabled={this.state.loadingCronograma}
                />
                <Col s={12} m={12} l={12} xl={12}>
                    {loadingCronograma && <ProgressBar indeterminate />}
                    {(isMounted) &&
                        Object.keys(cronograma).map(key => {
                            let item = cronograma[key];
                            return (
                                <ListarTopicoCronograma topicId={item.id} key={key} item={item} semester={semester} loadCronograma={this.listCronograma} />
                            )
                        })}
                    {
                        !loadingCronograma && cronograma.length === 0 ? <Col className="center" s={12} m={12} l={12} xl={12}><span className="grey-text center">O cronograma desse termo ainda não foi definido<br /><br /></span></Col>
                            :
                            !creatingTopic && <Col s={1} m={1} l={2} xl={2} className="teal lighten-2">
                            </Col>
                    }
                    {(!loadingCronograma && cronograma.length === 0) && <Col s={1} m={1} l={2} xl={2}></Col>}
                    {creatingTopic &&
                        <Row s={12} m={12} l={12} xl={12}>
                            <Col s={1} m={1} l={2} xl={2}>
                            </Col>
                            <Col s={11} m={11} l={8} xl={8}>
                                <form className="s12 m12 l12 xl12" onSubmit={this.handleSubmit}>
                                    <div className="row">
                                        <CronoInputField
                                            name="Nome do tópico"
                                            idField="topicName"
                                            size="s12 m12 l12 xl12"
                                            value={this.state.topicName}
                                            handleChange={(e) => this.setState({ topicName: e.target.value })}
                                            required
                                        />
                                        <Col s={12} m={12} l={12} xl={12}>
                                            <Button className="right" icon={<Icon className="left">save</Icon>} disabled={this.state.submitted}>Salvar</Button>
                                        </Col>
                                    </div>
                                </form>
                            </Col>

                        </Row>
                    }
                    {
                        creatingTopic && <Col s={1} m={1} l={2} xl={2} className="teal lighten-2">
                        </Col>
                    }
                    <Col className="teal lighten-2 center" style={{ borderBottom: "solid 1px teal lighten-2" }} s={11} m={11} l={8} xl={8}>
                        <Button waves={"green"} onClick={(() => this.setState({ creatingTopic: !creatingTopic }))} flat><span className="white-text">{creatingTopic ? "Cancelar" : "Adicionar tópico"}</span></Button>
                    </Col>
                </Col>
            </Row>
        )
    }
}

export default ManipulateCronograma;