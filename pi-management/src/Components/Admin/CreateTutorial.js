import React from 'react';
import { Col, Row, Button, Icon } from 'react-materialize';
import { Editor as EditorArea, EditorState, RichUtils, convertToRaw } from 'draft-js';
import { InputField } from '../General/InputField';
import './index.css';
import { tutorialServices } from '../../_services/tutorial.service';

export default class CreateTutorial extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            content: EditorState.createEmpty(),
            title: "",
            video: "",
            description: "",
            url: "",
            validForm: false,
        };
        this.onChange = (content) => this.setState({ content });
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    _onBoldClick(e) {
        let property;
        switch (e.target.textContent) {
            case "format_bold":
                property = "BOLD";
                break;

            case "format_italic":
                property = "ITALIC";
                break;

            case "format_underlined":
                property = "UNDERLINE";
                break;

            default:
                property = false;

        }
        if (property)
            this.onChange(RichUtils.toggleInlineStyle(this.state.content, property));
    }

    handleChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;

        if (event.target.name === "title") {
            let url = generateUrl(event.target.value);

            this.setState({
                url: url,
            })
        }

        this.setState(data, () => {
            const { content, url, title, video } = this.state;
            let isValid = title.length > 5 && (convertToRaw(content.getCurrentContent()).blocks[0].text.length > 5 || video.length > 5) && url;

            this.setState({
                validForm: isValid
            });
        });
    }

    async handleSubmit(e) {
        e.preventDefault();

        this.setState({
            validForm: false
        });

        const { content, description, url, title, video } = this.state;

        let raw = convertToRaw(content.getCurrentContent());
        let data = {
            content: raw.blocks[0].text !== "" ? JSON.stringify(raw) : null,
            title: title,
            video: video,
            description: description,
            url: url,
            token: JSON.parse(localStorage.getItem("admin")).token
        }

        await tutorialServices.publish(data);

        data = {
            content: EditorState.createEmpty(),
            title: "",
            video: "",
            description: "",
            url: ""
        }

        this.setState(data);
    }

    render() {
        return (
            <Row>
                <Col xl={2} l={2} s={2} m={2}>
                </Col>
                <Col xl={8} l={8} m={12} s={12}>
                    <span className="flow-text">Criar Tutorial</span>
                    <br />
                    <br />
                    <form onSubmit={this.handleSubmit}>
                        <Row className="center">
                            <InputField
                                name="Título"
                                helperText
                                textHelper={`tutorial: ${this.state.url}`}
                                size="s12 m12 l12 xl12"
                                idField="title"
                                type="text"
                                value={this.state.title}
                                handleChange={this.handleChange} />

                            <InputField
                                name="Descrição do tutorial"
                                size="s12 m12 l12 xl12"
                                idField="description"
                                type="text"
                                value={this.state.description}
                                handleChange={this.handleChange} />

                            <InputField
                                name="Link do video"
                                size="s12 m12 l12 xl12"
                                idField="video"
                                type="text"
                                value={this.state.video}
                                handleChange={this.handleChange} />


                        </Row>

                        <span className="flow-text">Conteúdo do tutorial</span>
                        <Row xl={12} l={12} s={12} m={12} style={{ border: "1px grey solid", width: "98%", marginLeft: "10px" }}>
                            <Col xl={12} l={12} s={12} className="center" style={{ padding: "1%", borderBottom: "1px grey solid", width: "99%", marginLeft: "5px" }}>
                                <Button small name="bold" type="button" floating onClick={this._onBoldClick.bind(this)} className="black"><Icon name="bold">format_bold</Icon ></Button>
                                <Button small name="bold" type="button" floating onClick={this._onBoldClick.bind(this)} className="black"><Icon name="bold">format_italic</Icon ></Button>
                                <Button small name="bold" type="button" floating onClick={this._onBoldClick.bind(this)} className="black"><Icon name="bold">format_underlined</Icon ></Button>
                            </Col>
                            <EditorArea placeholder=" Conteúdo" ariaExpanded editorState={this.state.content} onChange={this.onChange} />
                        </Row>
                        <Button type="submit" className="right" waves={"red"} disabled={!this.state.validForm}><span>Publicar <Icon className="right">send</Icon></span></Button>
                    </form>
                </Col>
            </Row>
        )
    }
}

function generateUrl(string) {

    let url = string.replace(/[^\w]/gi, ' ').split(" ").join("-").toLowerCase();

    while (url.includes("--")) {
        url = url.split("--").join("-");
    }

    if (url[url.length - 1] === "-") {
        url = url.slice(0, url.length - 1);
    }

    if (url[0] === "-") {
        url = url.slice(1, url.length);
    }
    return url;
}
