import { projectServices } from '../_services';
import { projectConstants } from '../_constants';
// import { alertActions } from '.';
// import { history } from '../_helpers';

export const projectActions = {
    getAll,
    viewPerfil: getProject,
    createProject,
    editProject
}

function getAll() {
    return dispatch => {
        dispatch(request())
        projectServices.getAll().then(
            response => {
                dispatch(success(response));
            },
            error => dispatch(failure(error))
        ).catch(err => {
            dispatch(failure(err));
        });
    }

    function request() { return { type: projectConstants.GETALL_REQUEST } }
    function success(projects) { return { type: projectConstants.GETALL_SUCCESS, projects } }
    function failure(error) { return { type: projectConstants.GETALL_FAILURE, error } }
}

function getProject(urlProject) {
    return dispatch => {
        dispatch(request())

        projectServices.viewPerfil(urlProject).then(
            response => {
                dispatch(success(response));
            },
            error => dispatch(failure(error))
        ).catch(err => {
            dispatch(failure(err));
        });
    }

    function request() { return { type: projectConstants.GETONE_REQUEST } }
    function success(projects) { return { type: projectConstants.GETONE_SUCCESS, projects } }
    function failure(error) { return { type: projectConstants.GETONE_FAILURE, error } }
}

function createProject(data, toast) {
    return dispatch => {
        dispatch(request());

        projectServices.createProject(data).then(
            response => {
                if (response.error) {
                    dispatch(failure(response.error))
                    toast(response.error, () => {

                    });
                } else {
                    localStorage.removeItem("projects");
                    dispatch(success(response));
                    toast("Projeto criado com sucesso!", null, () => {
                        window.location.href = "/projects"
                    });
                }
            },
            error => {
                dispatch(failure(error))
                toast(error,"red");
            }
        ).catch(err => {
            dispatch(failure(err))
            toast(err, "red");
        });
    }

    function request() { return { type: projectConstants.CREATE_REQUEST } }
    function success() { return { type: projectConstants.CREATE_SUCCESS } }
    function failure(error) { return { type: projectConstants.CREATE_FAILURE, error } }
}

function editProject(data, toast) {
    return dispatch => {
        dispatch(request());

        projectServices.editProject(data).then(
            response => {
                if (response.error) {
                    dispatch(failure(response.error));
                } else {
                    dispatch(success(response));
                    toast("Projeto editado com sucesso!",null, () => {
                        window.location.href = `/projects/${data.urlProject}`
                    });
                }
            },
            error => {
                dispatch(failure(error))
                toast(error, "red");
            }
        ).catch(err => {
            dispatch(failure(err))
            toast(err, "red");
        });

    }

    function request() { return { type: projectConstants.EDIT_REQUEST } }
    function success() { return { type: projectConstants.EDIT_SUCCESS } }
    function failure(error) { return { type: projectConstants.EDIT_FAILURE, error } }
}