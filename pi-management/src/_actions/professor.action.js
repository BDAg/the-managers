import { professorConstants } from '../_constants';
import { professorServices } from '../_services';
import { history } from '../_helpers';

export const professorActions = {
    login,
    logout,
    register,
    viewProfessorPerfil,
    getAll,
    updateUser,
    delete: _delete
};

function login(username, password, toast) {
    return dispatch => {
        dispatch(request({ username }));

        professorServices.login(username, password)
            .then(
                user => {
                    let msg = user.response;
                    if (msg) {
                        if (msg.message === "Not authenticated!" || msg.message === "Unknown error!") {
                            dispatch(failure("ALERT_ERROR"));
                            toast("Não foi possível conectar, tente novamente!");
                            window.location.reload();
                        }
                        else if (msg.message === "Not Autorized!") {
                            dispatch(failure("NOT_AUTORIZED"));
                            toast("Not Autorized!");
                            window.location.reload();
                        }
                    }
                    else {
                        dispatch(success(user));
                        window.location.replace('/')
                        history.push('/');
                    }

                },
                error => {
                    dispatch(failure(error));
                }
            );
    };

    function request(user) { return { type: professorConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: professorConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: professorConstants.LOGIN_FAILURE, error } }
}

function viewProfessorPerfil(email, toast) {
    return dispatch => {
        dispatch(request(email));

        professorServices.viewProfessorPerfil(email, toast).then(
            res => {
                if (res) {
                    if (res.response) {
                        dispatch(success(res));
                        return res
                    } else {
                        dispatch(failure(res.error));
                    }
                }
            },
            err => {
                dispatch(failure(err));
            }
        );
    }

    function request(user) { return { type: professorConstants.GETONE_REQUEST, user } }
    function success(user) { return { type: professorConstants.GETONE_SUCCESS, user } }
    function failure(error) { return { type: professorConstants.GETONE_FAILURE, error } }
}

function logout() {
    professorServices.logout();
    return { type: professorConstants.LOGOUT };
}

function register(userData, toast) {
    return dispatch => {
        dispatch(request(userData));

        professorServices.register(userData, toast)
            .then(
                user => {
                    dispatch(success());
                },
                error => {
                    dispatch(failure(error));
                    alert("Usuário não criado " + error);
                }
            );
    };

    function request(user) { return { type: professorConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: professorConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: professorConstants.REGISTER_FAILURE, error } }
}

function updateUser(data, token, toast) {
    return dispatch => {
        let message = ""
        dispatch(request());
        professorServices.updateUser(data, token).then(
            response => {
                if (response.error) {
                    dispatch(failure(response.error))
                    message = response.error;
                } else {
                    dispatch(success());
                    message = "Editado com sucesso"
                }
                toast(message)
            },
            error => {
                dispatch(failure(error));
                message = error
                toast(message)

            },
        )
    }

    function request() { return { type: professorConstants.EDIT_REQUEST } }
    function success() { return { type: professorConstants.EDIT_SUCCESS } }
    function failure(error) { return { type: professorConstants.EDIT_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());
        professorServices.getAll()
            .then(
                users => {
                    dispatch(success(users));
                    window.location.reload();
                },
                error => dispatch(failure(error))
            );
    };

    function request() { return { type: professorConstants.GETALL_REQUEST } }
    function success(users) { return { type: professorConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: professorConstants.GETALL_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        professorServices.delete(id, professorServices.header)
            .then(
                user => {
                    dispatch(success(id));
                },
                error => {
                    dispatch(failure(id, error));
                }
            );
    };

    function request(id) { return { type: professorConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: professorConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: professorConstants.DELETE_FAILURE, id, error } }
}