import { utilConstants } from '../_constants';
import { utilServices } from '../_services';
import { alertActions } from '.';

export const utilActions = {
    states,
    cities
};

function states() {
    return dispatch => {
        dispatch(request());

        utilServices.states()
            .then(
                response => {
                    if (response.error) {
                        dispatch(failure("ALERT_ERROR"));
                        alert("Não foi possível conectar, tente novamente!");
                        window.location.reload();
                    }
                    else {
                        dispatch(success());
                        // window.location.replace('/')
                        // history.push('/');
                    }

                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: utilConstants.REQUEST } }
    function success() { return { type: utilConstants.SUCCESS } }
    function failure(error) { return { type: utilConstants.FAILURE, error } }
}

function cities() {
    return dispatch => {
        dispatch(request());

        utilServices.cities()
            .then(
                response => {
                    if (response.error) {
                        dispatch(failure("ALERT_ERROR"));
                        alert("Não foi possível conectar, tente novamente!");
                        window.location.reload();
                    }
                    else {
                        dispatch(success());
                        // window.location.replace('/')
                        // history.push('/');
                    }

                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request() { return { type: utilConstants.REQUEST } }
    function success() { return { type: utilConstants.SUCCESS } }
    function failure(error) { return { type: utilConstants.FAILURE, error } }
}