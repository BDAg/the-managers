import { userConstants } from '../_constants';
import { studentServices } from '../_services';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    register,
    getAll,
    viewStudentPerfil,
    updateUser,
    delete: _delete
};

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));

        studentServices.login(username, password)
            .then(
                user => {
                    let msg = user.response;
                    if (msg) {
                        if (msg.message === "Not authenticated!" || msg.message === "Unknown error!") {
                            dispatch(failure("ALERT_ERROR"));
                            alert("Não foi possível conectar, tente novamente!");
                            window.location.reload();
                        }
                    }
                    else {
                        dispatch(success(user));
                        window.location.replace('/')
                        history.push('/');
                    }

                },
                error => {
                    dispatch(failure(error));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function viewStudentPerfil(email, toast) {
    return dispatch => {
        dispatch(request(email));

        studentServices.viewStudentPerfil(email, toast).then(
            res => {
                if (res) {
                    if (res.response) {
                        dispatch(success(res));
                        return res
                    } else {
                        dispatch(failure(res.error));
                    }
                }
            },
            err => {
                dispatch(failure(err));
            }
        );
    }

    function request(user) { return { type: userConstants.GETONE_REQUEST, user } }
    function success(user) { return { type: userConstants.GETONE_SUCCESS, user } }
    function failure(error) { return { type: userConstants.GETONE_FAILURE, error } }
}

function logout() {
    studentServices.logout();
    return { type: userConstants.LOGOUT };
}

function register(userData, toast) {
    return dispatch => {
        dispatch(request(userData));

        studentServices.register(userData, toast)
            .then(
                user => {
                    dispatch(success());
                },
                error => {
                    dispatch(failure(error));
                    alert("Usuário não criado " + error);
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function updateUser(data, token, toast) {
    return dispatch => {
        let message = ""
        dispatch(request());
        studentServices.updateUser(data, token).then(
            response => {
                if (response.error) {
                    dispatch(failure(response.error))
                    message = response.error;
                } else {
                    dispatch(success());
                    message = "Editado com sucesso"
                }
                toast(message)
            },
            error => {
                dispatch(failure(error));
                message = error
                toast(message)

            },
        )
    }

    function request() { return { type: userConstants.EDIT_REQUEST } }
    function success() { return { type: userConstants.EDIT_SUCCESS } }
    function failure(error) { return { type: userConstants.EDIT_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());
        studentServices.getAll()
            .then(
                users => {
                    dispatch(success(users));
                    window.location.reload();
                },
                error => dispatch(failure(error))
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        studentServices.delete(id, studentServices.header)
            .then(
                user => {
                    dispatch(success(id));
                },
                error => {
                    dispatch(failure(id, error));
                }
            );
    };

    function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
}