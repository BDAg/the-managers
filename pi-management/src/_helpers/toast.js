import M from 'materialize-css';

export const toast = function (message, color, callback = null) {
    return M.toast({
        html: `
        <div class="white-text">
            <span><b>${message}</br></span>
        </div>
        `,
        classes: `${color || "teal"}`,
        displayLength: 4000,
        completeCallback: callback,
    });
}