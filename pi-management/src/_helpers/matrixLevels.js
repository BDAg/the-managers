export const levels = {
    1: "Ouvi falar",
    2: "Entendi",
    3: "Sei fazer com ajuda",
    4: "Sei fazer sozinho",
    5: "Sei ensinar",
    6: "Sei criar"
}