import { levels } from '.';

export const matrixData = {
    options: {
        dataLabels: {
            style: {
                fontSize: '12px',
                fontFamily: 'Helvetica, Arial, sans-serif',
                colors: ["rgba(0,0,0,1)"]
            },
        },
        chart: {
            toolbar: {
                show: false,
            },
        },
        fill: {
            colors: ['rgba(0, 128, 128, 0.6)']
        },
        sparkline: {
            enabled: true,
        },
        labels: [],
        plotOptions: {
            radar: {
                size: 100,
                polygons: {
                    strokeColor: '#2983FF',
                    fill: {
                        colors: ['rgba(77, 227, 130, 0.8)', 'rgba(69, 255, 122, 0.8)',  'rgba(46, 191, 80, 0.8)', 'rgba(175, 219, 29, 0.8)', 'rgba(189, 116, 109, 0.8)', 'rgba(235, 99, 89, 0.8)']
                    }
                }
            }
        },
        // colors: ['rgba(57, 144, 237, 0.5)'],
        markers: {
            size: 4,
            colors: ['rgba(0,128,128, 0.8)'],
            strokeColor: 'rgb(0, 0, 0)',
            strokeWidth: 2,
        },
        yaxis: {
            forceNiceScale: true,
            tickAmount: 6,
            min: 0,
            max: 6,
            labels: {
                formatter: function (val, index) {
                    return levels[val];
                },
                minWidth: 0,
                maxWidth: 20,
                style: {
                    fontSize: '9px',
                    color: 'rgba(0, 0, 0, 0)'
                }
            },
        },
        stroke: {
            show: true,
            width: 3,
            colors: ['rgba(0,128,128, 0.8)'],
            dashArray: 0
        },
        tooltip: {
            enabled: true,
            theme: "dark",
            style: {
                fontSize: '12px'
            }
        },
    },
    series: [{
        name: '',
        data: []
    }]
}