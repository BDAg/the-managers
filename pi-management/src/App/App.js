import React from 'react';
import './App.css';

import openSocket from 'socket.io-client';

import { connect } from 'react-redux';
import { history } from '../_helpers';
import { alertActions } from '../_actions';

import { RouterComponent } from '../Components';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.socket = openSocket("http://35.231.220.66:4555");
    
    const { dispatch } = this.props;

    history.listen((location, action) => {
      dispatch(alertActions.clear());
    });
  }

  render() {
    return (
      <RouterComponent history={history} socket={this.socket}/>
    )
  }
}

function mapStateToProps(state) {
  const { alert } = state;

  return {
    alert
  }
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };