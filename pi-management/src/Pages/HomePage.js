import React from 'react';
import { studentServices, postServices } from '../_services';
import {
    CollapsibleStudents,
    NewPost,
    ListPost,
    ProjectCard
} from '../Components';

import { CollapsibleItem, Collapsible, Row, Col, Divider } from 'react-materialize';

import Button from 'react-materialize/lib/Button';

class HomePage extends React.Component {

    constructor(props) {
        super(props);

        this.user = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : false;
        let posts = localStorage.getItem("posts") ? JSON.parse(localStorage.getItem("posts")) : false;
        let postNumberShow = 10;
        this.professor = localStorage.getItem("professor") ? JSON.parse(localStorage.getItem("professor")) : false;
        this.state = {
            usersWithoutProject: localStorage.getItem("usersWProject") ? JSON.parse(localStorage.getItem("usersWProject")) : [],
            allUsers: localStorage.getItem("allUsers") ? JSON.parse(localStorage.getItem("allUsers")) : [],
            posts: posts,
            filter: false,
            haveNewsPosts: false,
            postNumberShow: postNumberShow,
            postsToView: posts ? posts.slice(0, postNumberShow) : [],
            newProjects: localStorage.getItem("projects") ? JSON.parse(localStorage.getItem("projects")).splice(0, 3) : [],
        }

        this.changeFilter = this.changeFilter.bind(this);
        this.getData = this.getData.bind(this);
        this.Poll = this.Poll.bind(this);
        this.viewMorePosts = this.viewMorePosts.bind(this);

    }

    async getData() {
        if (!this.state.posts) {
            await studentServices.listUsersWithoutProjects();
            this.setState({
                usersWithoutProject: JSON.parse(localStorage.getItem("usersWProject")),
                posts: JSON.parse(localStorage.getItem("posts")),
                postsToView: localStorage.getItem("posts") ? JSON.parse(localStorage.getItem("posts")).slice(0, this.state.postNumberShow) : []
            });
        } else {
            if(!localStorage.getItem("allUsers"))
                await studentServices.getAllStudents();

            this.setState({
                allUsers: JSON.parse(localStorage.getItem("allUsers"))
            });
            clearInterval(this.getDataIntervail);
        }

    }

    async viewMorePosts() {
        const { postNumberShow, posts } = this.state;

        this.setState({
            postsToView: posts.slice(0, postNumberShow + 10),
            postNumberShow: postNumberShow + 10
        });
    }

    async Poll() {
        const { haveNewsPosts } = this.state;
        if (!haveNewsPosts) {
            let have = await postServices.Polling();

            this.setState({
                haveNewsPosts: have
            });
        }
    }

    componentWillUnmount() {
        clearInterval(this.PollIntervail);
        postServices.getAll().then(posts => {
        }).catch(err => {
        });
    }

    async componentDidMount() {
        this.getDataIntervail = setInterval(this.getData, 500);
        this.PollIntervail = setInterval(this.Poll, 30000);
        await postServices.getAll().then(posts => {
            this.setState({
                posts: JSON.parse(localStorage.getItem("posts")),
                postsToView: JSON.parse(localStorage.getItem("posts")).slice(0, 10)
            });
        }).catch(err => {
        });

    }


    async reloadThis(justReaload = false) {
        if (!justReaload) {
            this.setState({
                posts: JSON.parse(localStorage.getItem("posts"))
            })
        }
        window.location.reload();
    }

    changeFilter(e) {
        this.setState({
            filter: !this.state.filter
        });
    }

    render() {
        const { usersWithoutProject, allUsers, filter, posts, haveNewsPosts, postsToView, newProjects } = this.state;
        const users = filter ? usersWithoutProject : allUsers;

        if (posts) {
            return (
                <Row s={12} m={12} l={12} xl={12}>
                    <h3 className="center green-text flow-text row">
                        Bem vindo ao PI MANAGEMENT
                    </h3>

                    <Col s={12} m={12} l={4} xl={4}>
                        <Collapsible>

                            <CollapsibleItem header="Últimos projetos" icon={<i className="material-icons">lightbulb_outline</i>}>

                                {Object.keys(newProjects).map(key => {
                                    let project = newProjects[key];

                                    return <ProjectCard
                                        key={key}
                                        project={project}
                                    />
                                })}
                            </CollapsibleItem>
                            {
                                this.user &&
                                <CollapsibleStudents users={users || []} handleChange={this.changeFilter} />
                            }
                            {
                                this.professor &&
                                <CollapsibleStudents users={users || []} handleChange={this.changeFilter} />
                            }
                        </Collapsible>
                    </Col>
                    <Col s={12} m={12} l={8} xl={8} className="right">
                        <Col s={12} m={12} l={12} xl={12} className="center">
                            {(this.user || this.professor) && <NewPost reloadThis={this.reloadThis} />}
                            {haveNewsPosts && <Row s={12} m={12} l={12} xl={12} className="z-depth-1 center-algin"><Button s={12} m={12} l={12} xl={12} flat onClick={this.reloadThis}> <span> Há novos posts </span></Button></Row>}
                            {postsToView && Object.keys(postsToView).map((key) => <ListPost postData={postsToView[key]} key={key} />)}
                            <Divider />
                            {(posts.length > 9) && posts.length > postsToView.length && <Row s={12} m={12} l={12} xl={12} className="z-depth-1 center-algin"><Button waves={"teal"} s={12} m={12} l={12} xl={12} flat onClick={this.viewMorePosts}> <span> Ver mais </span></Button></Row>}
                        </Col>
                    </Col>
                </Row>
            )
        } else {
            return (
                <Col className="center">
                    <br />
                    <Col className="progress">
                        <Col className="indeterminate"></Col>
                    </Col>
                </Col>
            )
        }
    }
}


export default HomePage;