import React from 'react';
import { connect } from 'react-redux';
import { toast } from '../_helpers';

import {
    InputField,
} from '../Components';

import { studentServices } from '../_services';

class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            isValid: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;

        this.setState(data, () => {
            let { email } = this.state;

            let isValid = email.length > 5;

            this.setState({
                isValid: isValid
            });
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            isValid: false
        });
        let { email } = this.state;

        studentServices.forgotPassowrd(email, toast);
    }

    render() {
        let customSize = 's6 m6 l6 xl6';

        const { isValid } = this.state
        return (
            <div>
                <br />
                <div className="center">
                    <span className="flow-text"> Esqueci minha senha</span>
                </div>

                <div className="row s12 m12 l12 xl12">
                    <div className="col s3 l3 xl3"></div>

                    <form className="center" onSubmit={this.handleSubmit}>
                        <div className="row">
                            <InputField
                                name="Digite seu e-mail"
                                size={customSize}
                                idField="email"
                                value={this.state.email}
                                type="text"
                                handleChange={this.handleChange}
                                required={true} />
                        </div>

                        <div className="center">
                            <button className="btn waves-effect waves-light" type="submit" disabled={!isValid}>Enviar
                        <i className="material-icons right">send</i>
                            </button>
                        </div>
                    </form>

                </div>
                <div className="row s12 m12 l12 xl12">
                    <div className="col s3 l3 xl3">
                    </div>
                    <div className="center">
                        <a className="text-teal" href="/password-redefine" >Já tenho um código</a>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {};
}

const connectedRouterPage = connect(mapStateToProps)(ForgotPassword);
export { connectedRouterPage as ForgotPassword };