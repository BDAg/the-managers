import React from 'react';
import { connect } from 'react-redux';
import { ProjectCard, ListPost, NewPost } from '../Components';
import ReactApexChart from 'react-apexcharts';
import { matrixData } from '../_helpers';
import { Tab, Tabs, Col, Row } from 'react-materialize';
import { postServices } from '../_services';
import { apiUrl } from '../_helpers/api-url';

class UserPerfil extends React.Component {
    constructor(props) {
        super(props);
        this.photoUrl = apiUrl+"/photos/"
        this.user = this.props.userData || JSON.parse(localStorage.getItem("user"));
        this.matrix = this.user.data.matrix;
        let posts = localStorage.getItem("posts") ? JSON.parse(localStorage.getItem("posts")) : false
        this.state = {
            posts: posts,
            postsFiltered: []
        }
        this.getPosts = this.getPosts.bind(this);
    }

    async getPosts() {
        if (!this.state.posts) {
            await postServices.getAll().then(posts => {
                this.setState({
                    posts: JSON.stringify(posts.data.response)
                });
            }).catch(err => {
            });
        } else {
            clearInterval(this.getPostsInterval);

            const { posts } = this.state;
            new Promise((resolve) => {

                let data = posts.filter((value) => {
                    if (value.Student) {
                        if(value.Student.id === this.user.data.id)
                            return value;
                    }
                    return null;
                });
                resolve(data);

            }).then((value) => {
                this.setState({
                    postsFiltered: value
                });
            });

            
        }
    }
    componentDidMount() {
        this.getPostsInterval = setInterval(this.getPosts, 500);
    }

    async reloadThis(justReaload = false) {
        if (!justReaload) {
            await this.getPosts();
        }
        window.location.reload();
    }

    render() {
        let userProjects = this.user.data.projects;
        let user = this.user;
        let labels = [];
        let data = [];
        let currentLoggedUser = localStorage.getItem("user");
        const { posts, postsFiltered } = this.state;
        for (let key in this.matrix) {
            labels.push(this.matrix[key].name);
            data.push(this.matrix[key].SkillsMatrices.level)
        }

        matrixData.options.labels = labels.slice(0, 30);
        matrixData.series[0].data = data.slice(0, 30);

        return (
            <div className="row">
                <br />
                <div className="col m12 s12 l4 xl4 left">
                    <div className="row">
                        <div className="col s12 m12 l12 xl12">
                            <div className="card" >
                                <div className="card-image">
                                    {user.data.photo && <Row style={{backgroundColor: "grey"}} className="center" s={12} m={12} l={12} xl={12}><img alt="Foto de perfil" className="center" style={{maxHeight: "50vh",  marginLeft: "auto", marginRight: "auto", width: "auto"}} src={`${this.photoUrl + user.data.photo.path}`} /></Row>}
                                </div>
                                <div className="card-content">

                                    <h5>{user.data.name}</h5>
                                    <br />
                                    {user.data.city && <span>{user.data.city.name} - {user.data.city.State.name}<br /></span>}
                                    <span>{user.data.email}</span>
                                    <br />
                                    {user.data.phone && <span>{user.data.phone} <br /></span>}
                                    <span>{user.data.semester}</span>
                                    <h6><b>Matriz de Habilidades</b></h6>
                                    <div className="row s12 m12 l12 xl12">
                                        {
                                            this.matrix.length > 9 ?
                                                <div>
                                                    <ReactApexChart options={matrixData.options} series={matrixData.series} type="radar" height="300" />
                                                </div>
                                                :
                                                <div className="center grey-text">{
                                                    currentLoggedUser ? this.user.data.id === JSON.parse(currentLoggedUser).data.id ?
                                                        <span>Você ainda não criou sua matriz <a href="/account#editmatrix">clique aqui</a> para editá-la</span> :
                                                        <span>{user.data.name} ainda não possui uma matriz de habilidades</span> : <span>{user.data.name} ainda não possui uma matriz de habilidades</span>
                                                }</div>
                                        }
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col m12 s12 l8 xl8 right">
                    <Col s={12} m={12} l={12} xl={12}>
                        {currentLoggedUser && this.user.data.id === JSON.parse(currentLoggedUser).data.id  && <NewPost reloadThis={this.reloadThis} />}
                    </Col>
                    <Tabs className="tab-demo z-depth-1" >
                        <Tab title="Projetos">
                            {userProjects.length > 0 ?
                                Object.keys(userProjects).map((key) => {
                                    if (userProjects[key].Members.isMember === 1) {
                                        return <ProjectCard project={userProjects[key]} key={key} />
                                    }
                                    return <b key={key}></b>
                                }) :
                                <div className="center grey-text"><span>{currentLoggedUser ? this.user.data.id === JSON.parse(currentLoggedUser).data.id ? "Vocễ " : user.data.name : user.data.name} ainda não possui projetos</span></div>}
                        </Tab>
                        <Tab active={window.location.hash==="#posts"} title="Publicações">
                            <Col s={12} m={12} l={12} xl={12}>
                                <br/>
                                {posts && postsFiltered.length > 0 ? Object.keys(postsFiltered).map((key) => <ListPost postData={postsFiltered[key]} key={key} />) : <span>Esse usuário não possui publicações</span>}
                            </Col>
                        </Tab>
                    </Tabs>

                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedRouterPage = connect(mapStateToProps)(UserPerfil);
export { connectedRouterPage as UserPerfil };