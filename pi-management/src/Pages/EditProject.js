import React from 'react';
import M from 'materialize-css';
import { connect } from 'react-redux';

import {
    InputField,
    DatePicker,
} from '../Components';

import {
    dateoptions, 
    toast,
} from '../_helpers';

import { projectActions } from '../_actions';


class EditProject extends React.Component {

    constructor(props) {
        super(props);
        const { match } = this.props;
        this.project = match.params.url;
        let projectData = localStorage.getItem("projectsPerfil") ? JSON.parse(localStorage.getItem("projectsPerfil"))[this.project] : null
        this.state = {
            projectData: projectData,
            name: projectData ? projectData.name : '',
            description: projectData ? projectData.description : '',
            expireAt: projectData ? new Date(projectData.expireAt) : '',
            gitlab: projectData ? projectData.gitlab : '',
            validForm: projectData ? true : false,
            StudentId: JSON.parse(localStorage.getItem("user"))["data"]["id"]
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDate = this.handleDate.bind(this);
    }

    handleDate(data) {
        let date = new Date(data);
        this.setState({
            expireAt: date.toISOString()
        });
    }

    componentDidMount() {
        dateoptions['onSelect'] = this.handleDate;
        dateoptions['defaultTime'] = new Date(this.state.expireAt);
        document.addEventListener('DOMContentLoaded', function () {
            let elems = document.querySelectorAll('.datepicker');
            M.Datepicker.init(elems, dateoptions);
        });
    }

    handleChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;

        this.setState(data, () => {
            let { name, description } = this.state;

            let isValid = name.length > 5 && description.length >= 5;

            this.setState({
                validForm: isValid
            });
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            validForm: false
        });
        let { name, gitlab, description, expireAt } = this.state;
        let data = {
            name: name,
            gitlab: gitlab,
            description: description,
            expireAt: expireAt,
            idProject: this.state.projectData.id,
            urlProject: this.project
        }
        
        this.props.dispatch(projectActions.editProject(data, toast));
    }

    render() {
        let customSize = 's9 m9 l9 xl9'
        if (this.state.projectData) {
            return (
                <div>
                    <br />
                    <div className="center">
                        <span className="flow-text"> Editar {this.state.projectData.name || "projeto"}</span>
                    </div>

                    <div className="row s12 m12 l12 xl12">
                        <div className="col s3 l3 xl3">

                        </div>

                        <form className="col s9 m9 l9 xl9" onSubmit={this.handleSubmit}>
                            <div className="row">
                                <InputField
                                    name="Nome do projeto"
                                    size={customSize} idField="name"
                                    type="text"
                                    value={this.state.name}
                                    handleChange={this.handleChange}
                                    helperText={true}
                                    textHelper={this.state.url}
                                    required={true} />

                                <InputField
                                    name="Descrição do Projeto"
                                    size={customSize}
                                    idField="description" type="text"
                                    value={this.state.description}
                                    handleChange={this.handleChange}
                                    required={true} />

                                <InputField
                                    name="GitLab do projeto"
                                    type="text"
                                    idField="gitlab"
                                    size={customSize}
                                    value={this.state.gitlab}
                                    handleChange={this.handleChange}
                                    required={true}
                                />

                                <DatePicker
                                    name="Data de encerramento do projeto"
                                    size={customSize}
                                    idField="expire"
                                    value={this.state.expireAt}
                                    handleChange={this.handleChange}
                                    required={true}
                                />

                                <div className={`col ${customSize}`}>
                                    <button className="btn waves-effect waves-light" type="submit" disabled={!this.state.validForm}>Editar Projeto
                                        <i className="material-icons right">create</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            )
        } else {
            window.location.href = "/projects"

            return (
                <div>
                </div>
            )
        }

    }
}

function mapStateToProps(state) {
    const { projectsPerfil, projects } = state;
    return (
        projectsPerfil,
        projects
    );
}
const connectedEditProject = connect(mapStateToProps)(EditProject);
export { connectedEditProject as EditProject }