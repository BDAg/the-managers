import React from 'react';
import { projectServices } from '../_services';
import { connect } from 'react-redux';
import { ProjectCard } from '../Components';
import './index.css';

import { Col, Row, TextInput } from 'react-materialize';

import {
    SelectFilterProjects
  } from '../Components';

class ListProjects extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            projects: localStorage.getItem("projects") ? JSON.parse(localStorage.getItem("projects")) : false,
            user: localStorage.getItem("user"),
            gotData: localStorage.getItem("projects") ? true : false,
            query: '',
            projectsFilter: []
        }


        this.getData = this.getData.bind(this);
        this.projectFilter = this.projectFilter.bind(this);
        this.changeQuery = this.changeQuery.bind(this);
        this.filterSelect = this.filterSelect.bind(this);
        this.filterExpiredOrAvailable = this.filterExpiredOrAvailable.bind(this);
        this.order = this.order.bind(this);
    }

    async getData() {
        if (!this.state.projects) {
            projectServices.getAll().then(data => {
                this.setState({
                    projects: JSON.parse(localStorage.getItem("projects"))
                });
            }).catch(()=>{

            });
            
            this.setState({
                gotData: true,
            }, () => {
                clearInterval(this.getDataInteval);
            });
        }
    }

    async componentDidMount() {
        this.getDataInteval = setInterval(this.getData, 500);
    }

    componentWillUnmount(){
        projectServices.getAll().then(() => {
            this.setState({
                projects: localStorage.getItem("projects") ? JSON.parse(localStorage.getItem("projects")) : false,
            })
        }).catch(()=>{
        });
    }
    
    changeQuery(value) {
        this.setState({
            query: value.target.value
        }, ()=>{
            return this.projectFilter();
        });
    }

    async projectFilter() {

        const { projects } = this.state;
        new Promise((resolve) => {
            
            let data = projects.filter((value) => {
                if (value.name.toString().toLowerCase().includes(this.state.query)) {
                    return value;
                }
                return null;
            });
            resolve(data);

        }).then((value) => {
            this.setState({
                projectsFilter: value
            });
        });
    }

    async filterExpiredOrAvailable(what = '') {
        const { projects } = this.state;

        new Promise((resolve) => {

            let today = new Date().getTime();
            let expireProject;

            let data = projects.filter((project) => {
                expireProject = new Date(project["expireAt"]).getTime();
                if (what === 'expired') {
                    if (today > expireProject) {
                        return project
                    }
                } else {
                    if (today < expireProject) {
                        return project
                    }
                }
                return null;
            });

            resolve(data);

        }).then((value) => {
            this.setState({
                query: '',
                projectsFilter: value
            });
        });
    }

    async order(order) {

        let numOrderA = order === 'asc' ? -1 : 1;
        let numOrderB = order === 'asc' ? 1: -1;
        
        new Promise((resolve) => {
            let orderAsc = Array.from(this.state.projects).sort((a, b) => {
                if (a.name < b.name) return numOrderA;
                if (a.name > b.name) return numOrderB;
                return 0;
            });
            resolve(orderAsc);
        }).then((value) => {
            this.setState({
                query: '',
                projectsFilter: value
            });
        });

    }

    async filterSelect(event) {
        let value = Number(event.target.value);

        switch (value) {
            case 0:
                this.setState({
                    query: '',
                    projectsFilter: this.state.projects
                });
                break;

            case 1:
                this.filterExpiredOrAvailable('available');
                break;

            case 2:
                this.filterExpiredOrAvailable('expired');
                break;

            case 3:
                this.order('asc');
                break;
        
            case 4:
                this.order('desc');
                break;

            default:
                this.setState({
                    query: '',
                    projectsFilter: this.state.projects
                });
                break;
        }

    }

    render() {
        const { projectsFilter, projects, gotData, user } = this.state;
        let projectsToMap = projectsFilter.length > 0 ? projectsFilter : projects

        if (!gotData) {
            return (
                <Col className="center">
                    <br />
                    <Col className="progress">
                        <Col className="indeterminate"></Col>
                    </Col>
                </Col>
            )
        } else {
            return (

                <Col className={'input-search'}>
                    <br />
                    
                    {user && <Row className={"right"} style={{ marginRight: "15px" }}>
                        <a className="btn waves-effect teal lighten-2 waves-light" href="/new-project" >novo projeto
                        <i className="material-icons left">add</i>
                        </a>
                    </Row>
                    }
                    <Col s={12} m={12} l={12} lx={12}>
                        <TextInput id="search" value={this.state.query} onChange={this.changeQuery} type="text" className={"validate"} placeholder={'Buscar projeto'} />
                        <SelectFilterProjects
                            handleChange={this.filterSelect}
                            size={"s12 m12 l12 xl12"}
                        />
                    </Col>
                    <br />
                    <Row s={12} m={12} l={12} lx={12}>
                        {
                            projectsToMap && projectsToMap.map(project => (
                            
                                <ProjectCard key={project.id} project={project} />
                            ))
                        }
                    </Row>
                </Col>
            )
        }
    }
}
function mapStateToProps(state) {
    const { projects } = state;
    return (
        projects
    );
}

const connectedListPage = connect(mapStateToProps)(ListProjects);
export { connectedListPage as ListProjects };