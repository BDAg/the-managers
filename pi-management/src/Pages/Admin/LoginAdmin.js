import React, { Component } from 'react';

// import { userActions } from '../_actions';
import { connect } from 'react-redux';

import {
  InputField
} from '../../Components';
import { AdminServices } from '../../_services/admin.service';

class LoginAdmin extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      validForm: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    let data = {};
    data[event.target.name] = event.target.value;

    this.setState(data, () => {
      let { email, password } = this.state;

      let isValid = email.length > 5 && password.length >= 5;

      this.setState({
        validForm: isValid
      });
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.setState({
      validForm: false
    });

    let { email, password } = this.state;

    let data = {
      email: email,
      password: password
    };
    
    AdminServices.login(data.email, data.password);
  }

  render() {
    let customSize = 's9 m9 l9 xl9'
    return (
      <div>
        <br />
        <div className="center">
          <span className="flow-text">Área do administrador</span>
        </div>

        <div className="row s12 m12 l12 xl12">
          <div className="col s3 l3 xl3">

          </div>

          <form className="col s9 m9 l9 xl9" onSubmit={this.handleSubmit}>
            <div className="row">

              <InputField
                name="email"
                size={customSize} idField="email"
                type="email"
                value={this.state.email}
                handleChange={this.handleChange}
                required={true} />

              <InputField
                name="password"
                size={customSize} idField="password"
                type="password"
                value={this.state.password}
                handleChange={this.handleChange}
                required={true} />

              <div className={`col ${customSize}`}>
                <button className="btn waves-effect waves-light" type="submit" disabled={!this.state.validForm}>Login
                              <i className="material-icons right">send</i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

function mapStateToProps() {
  return {
  };
}

const connectedRouterPage = connect(mapStateToProps)(LoginAdmin);
export { connectedRouterPage as LoginAdmin };