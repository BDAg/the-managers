import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Collapsible, CollapsibleItem, Row } from 'react-materialize';
import { CreateTutorial } from '../../Components';

class AdminArea extends Component {

    constructor(props) {
        super(props);
        this.admin = localStorage.getItem("admin") ? JSON.parse(localStorage.getItem("admin")) : false;
        this.authorized = this.admin ? this.admin.data.role : false
    }


    render() {
        if (this.authorized) {
            return (
                <Row style={{padding: "2%"}}>
                    <br />
                    <div className="center">
                        <span className="flow-text">Área do administrador</span>
                    </div>

                    <h6>Bem vindo {this.admin.data.name}</h6>
                    <Collapsible>
                        <CollapsibleItem header="Criar tutorial">
                            <CreateTutorial />
                        </CollapsibleItem>
                        <CollapsibleItem header="Ativar contas de professores">
                            <Row s={12} m={12} l={12} xl={12}>
                                <span className="grey-text">Nada por aqui :)</span>
                            </Row>
                        </CollapsibleItem>
                    </Collapsible>


                    <Button className="right" flat waves={"red"} onClick={() => {
                        localStorage.removeItem("admin");
                        window.location.reload();
                    }}>Sair</Button>
                </Row>
            );
        } else {
            localStorage.removeItem("admin");
            window.location.reload();
            return (
                <div>
                    Não Autorizado
                </div>
            )
        }
    }
}

function mapStateToProps() {
    return {
    };
}

const connectedRouterPage = connect(mapStateToProps)(AdminArea);
export { connectedRouterPage as AdminArea };