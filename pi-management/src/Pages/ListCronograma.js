import React from 'react';
import { cronogramaServices } from '../_services/cronograma.service';
import { Col, Row, ProgressBar } from 'react-materialize';
import { SelectSemester } from '../Components';
import { toast } from '../_helpers';
class ListCronograma extends React.Component {
    constructor(props) {
        super(props);
        if (localStorage.getItem("user")) {
            this.user = JSON.parse(localStorage.getItem("user"));
        } else {
            window.location.href = "/";
        }

        this.state = {
            cronograma: localStorage.getItem("cronograma") ? JSON.parse(localStorage.getItem("cronograma")) : false,
            semester: this.user.data.semester,
            disabled: false,
            loadingCronograma: false
        }
        this.handleSemesterChange = this.handleSemesterChange.bind(this);
    }

    componentDidMount() {
        if (!localStorage.getItem("cronograma")) {
            cronogramaServices.listar(this.user.data.semester).then(response => {
                localStorage.setItem("cronograma", JSON.stringify(response));
                this.setState({
                    cronograma: response
                });
            });
        }
    }

    listCronograma() {
        this.setState(
            {
                loadingCronograma: true
            },
        )
        cronogramaServices.listar(this.state.semester).then(
            (
                response => this.setState(
                    {
                        cronograma: response,
                        loadingCronograma: false,
                    },
                )
            )
        ).catch((err => toast("Erro ao listar cronograma " + err.toString(), "red")));
    }

    async handleSemesterChange(e) {
        this.setState({
            semester: e.target.value,
            loadingCronograma: true
        }, () => this.listCronograma());
    }

    render() {
        const { cronograma, loadingCronograma } = this.state;
        return (
            <div className="center">
                <br />
                <SelectSemester
                    label="Selecione o termo"
                    handleChange={this.handleSemesterChange}
                    disabled={this.state.loadingCronograma}
                    defaultV={this.user.data.semester}
                />
                <br />
                {loadingCronograma && <ProgressBar indeterminate />}
                {Object.keys(cronograma).length === 0 && <span className="center grey-text">O cronograma desse termo ainda não foi definido, consulte seu professor para mais informações</span>}
                {Object.keys(cronograma).map((key) => {
                    let item = cronograma[key]
                    return (
                        <Row s={12} m={12} l={12} xl={12} key={key} className="center">
                            <Col s={12} m={12} l={2} xl={2} >
                            </Col>
                            <Col s={12} m={12} l={8} xl={8} className="teal lighten-2 z-depth-2">
                                <Col className="white-text valign-wrapper" s={12} m={12} l={12} xl={12} style={{ height: "5vh" }}>
                                    <Col s={7} m={7} xl={6} l={6}>
                                        <span className="left truncate"><b>{item.description}</b></span>
                                    </Col>
                                    <Col s={5} m={5} xl={6} l={6} className="valign-wrapper">
                                        <Col s={6} m={6} xl={6} l={6}>
                                            <span className="right">Data de Início</span>
                                        </Col>
                                        <Col s={6} m={6} xl={6} l={6}>
                                            <span className="right">Data de Entrega</span>
                                        </Col>
                                    </Col>
                                </Col>
                                {Object.keys(item.Cronogramas).map(keyCrono => {
                                    let itemCrono = item.Cronogramas[keyCrono];
                                    let expireAt = new Date(itemCrono.expireAt);
                                    let expireAtDate = `${expireAt.getDate() < 10 ? "0" + expireAt.getDate().toString() : expireAt.getDate()}/${expireAt.getUTCMonth() + 1 < 10 ? "0" + (expireAt.getUTCMonth() + 1).toString() : expireAt.getUTCMonth() + 1}/${expireAt.getFullYear()}`
                                    let startsAt = new Date(itemCrono.startsAt);
                                    let startsAtDate = `${startsAt.getDate() < 10 ? "0" + startsAt.getDate().toString() : startsAt.getDate()}/${startsAt.getUTCMonth() + 1 < 10 ? "0" + (startsAt.getUTCMonth() + 1).toString() : startsAt.getUTCMonth() + 1}/${startsAt.getFullYear()}`
                                    return (
                                        <Col key={keyCrono} className="white z-depth-1 valign-wrapper" style={{ borderBottom: "solid 1px #4db6ac" }} s={12} m={12} l={12} xl={12}>

                                            <Col s={7} m={7} xl={6} l={6}>
                                                <span className="left truncate">{itemCrono.description}</span>
                                            </Col>
                                            <Col s={5} m={5} xl={6} l={6} className="valign-wrapper">
                                                <Col s={6} m={6} xl={6} l={6}>
                                                    <span className="right">{startsAtDate}</span>
                                                </Col>
                                                <Col s={6} m={6} xl={6} l={6}>
                                                    <span className="right">{expireAtDate}</span>
                                                </Col>
                                            </Col>
                                        </Col>
                                    )

                                })}

                            </Col>
                        </Row>
                    )
                })}
            </div>
        )
    }
}

export default ListCronograma;