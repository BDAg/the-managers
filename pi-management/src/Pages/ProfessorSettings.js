import React from 'react';
import { connect } from 'react-redux';

import {
    ProfessorSettingsTabs,
} from '../Components';

class ProfessorSettings extends React.Component {

    constructor(props){
        super(props);
        this.dispatch = this.props.dispatch;

    }
    render() {

        return (
            <div className="row center">
                <h3>Editar dados</h3>
                <ProfessorSettingsTabs dispatch={this.dispatch}/>
            </div >
        )
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedRouterPage = connect(mapStateToProps)(ProfessorSettings);
export { connectedRouterPage as ProfessorSettings };