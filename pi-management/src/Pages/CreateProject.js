import React from 'react';
import M from 'materialize-css';
import { connect } from 'react-redux';
import { toast } from '../_helpers'

import {
    InputField,
    DatePicker,
    SelectInputJob
} from '../Components';

import {
    dateoptions,
} from '../_helpers';

import { projectActions } from '../_actions';


class CreateProject extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            gitlab: 'gitlab.com/BDAg/',
            url: '',
            expireAt: '',
            job: 3,
            validForm: false,
            StudentId: JSON.parse(localStorage.getItem("user"))["data"]["id"]
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDate = this.handleDate.bind(this);
    }

    handleDate(data) {
        let date = new Date(data);
        console.log(date);
        this.setState({
            expireAt: date.toISOString()
        }, (() => {
            let { name, gitlab, description, expireAt } = this.state;

            let isValid = name.length > 3 && validateGitlab(gitlab) && description.length >= 5 && expireAt.length > 10;
            this.setState({
                validForm: isValid
            });
        }))
    }

    componentDidMount() {
        dateoptions['onSelect'] = this.handleDate;

        document.addEventListener('DOMContentLoaded', function () {
            let elems = document.querySelectorAll('.datepicker');
            M.Datepicker.init(elems, dateoptions);
        });
    }

    handleChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;

        if (event.target.name === "name") {
            let url = generateUrl(event.target.value);

            this.setState({
                url: url,
                gitlab: 'gitlab.com/BDAg/' + url
            });
        }

        this.setState(data, () => {
            let { name, gitlab, description, expireAt } = this.state;

            let isValid = name.length > 3 && validateGitlab(gitlab) && description.length >= 5 && expireAt.length > 10;

            this.setState({
                validForm: isValid
            });
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            validForm: false
        });
        let { name, gitlab, description, job, expireAt, url, StudentId } = this.state;
        let data = {
            name: name,
            gitlab: gitlab,
            description: description,
            job: job,
            expireAt: expireAt,
            url: url,
            StudentId: StudentId
        }

        this.props.dispatch(projectActions.createProject(data, toast));
    }

    render() {
        let customSize = 's9 m9 l9 xl9'
        return (
            <div>
                <br />
                <div className="center">
                    <span className="flow-text"> Criar novo projeto</span>
                </div>

                <div className="row s12 m12 l12 xl12">
                    <div className="col s3 l3 xl3">

                    </div>

                    <form className="col s9 m9 l9 xl9" onSubmit={this.handleSubmit}>
                        <div className="row">
                            <InputField
                                name="Nome do projeto"
                                size={customSize} idField="name"
                                type="text"
                                value={this.state.name}
                                handleChange={this.handleChange}
                                helperText={true}
                                textHelper={` projeto: ${this.state.url}`}
                                required={true} />

                            <InputField
                                name="Descrição do Projeto"
                                size={customSize}
                                idField="description" type="text"
                                value={this.state.description}
                                handleChange={this.handleChange}
                                required={true} />

                            <DatePicker
                                name="Data de encerramento do projeto"
                                size={customSize}
                                idField="expire"
                                handleChange={this.handleChange} required={true} />

                            <InputField
                                name="GitLab do projeto"
                                type="text"
                                idField="gitlab"
                                size={customSize}
                                value={this.state.gitlab}
                                handleChange={this.handleChange}
                                required={true}
                            />

                            <SelectInputJob
                                name="Função no projeto"
                                size={customSize}
                                handleChange={this.handleChange}
                            />

                            <div className={`col ${customSize}`}>
                                <button className="btn waves-effect waves-light" type="submit" disabled={!this.state.validForm}>Criar Projeto
                                    <i className="material-icons right">{this.state.anyRequest ? "cached" : "send"}</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

function generateUrl(string) {

    let url = string.replace(/[^\w]/gi, ' ').split(" ").join("-").toLowerCase();

    while (url.includes("--")) {
        url = url.split("--").join("-");
    }

    if (url[url.length - 1] === "-") {
        url = url.slice(0, url.length - 1);
    }

    if (url[0] === "-") {
        url = url.slice(1, url.length);
    }
    return url;
}

function validateGitlab(gitlab) {
    gitlab = gitlab.toLowerCase();
    return gitlab.indexOf("gitlab.com/bdag/") !== -1 && gitlab.split("gitlab.com/bdag/").join("").split("www").join("").length > 4;
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}
const connectedRouterPage = connect(mapStateToProps)(CreateProject);
export { connectedRouterPage as CreateProject };