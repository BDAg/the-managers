import React from 'react';
import { Col, ProgressBar } from 'react-materialize';
import { TutorialCard } from '../Components';
import { tutorialServices } from '../_services/tutorial.service';

class ListTutorials extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tutorials: [],
            gotData: false
        }
    }

    componentDidMount() {
        tutorialServices.list().then(response => {
            this.setState({
                tutorials: response,
                gotData: true
            });
        });
    }
    
    render() {
        const { tutorials, gotData } = this.state
        if (gotData) {
            return (
                <div>
                    {
                        Object.keys(tutorials).map(key => {
                            let tutorial = tutorials[key];

                            return <TutorialCard
                                key={key}
                                video={tutorial.video !== ""}
                                name={tutorial.title}
                                url={tutorial.url}
                                description={tutorial.description}
                                content={tutorial.content !== null}
                            />
                        })
                    }
                </div>
            )
        } else {
            return (
                <Col s={12}>
                    <ProgressBar />
                </Col>
            )
        }
    }
}

export default ListTutorials;