import React, { Component } from 'react';

import { professorActions } from '../_actions';
import { connect } from 'react-redux';

import { toast } from '../_helpers/toast';

import {
  InputField
} from '../Components';

class LoginTeacher extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      validForm: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    let data = {};
    data[event.target.name] = event.target.value;

    this.setState(data, () => {
      let { email, password } = this.state;

      let isValid = email.length > 5 && password.length >= 5;

      this.setState({
        validForm: isValid
      });
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.setState({
      validForm: false
    });

    let { email, password } = this.state;

    let data = {
      email: email,
      password: password
    };

    this.props.dispatch(professorActions.login(data.email, data.password, toast));
  }

  render() {
    let customSize = 's9 m9 l9 xl9'
    return (
      <div>
        <br />
        <div className="center">
          <span className="flow-text">Entrar como professor</span>
        </div>

        <div className="row s12 m12 l12 xl12">
          <div className="col s3 l3 xl3">

          </div>

          <form className="col s9 m9 l9 xl9" onSubmit={this.handleSubmit}>
            <div className="row">

              <InputField
                name="email"
                size={customSize} idField="email"
                type="email"
                value={this.state.email}
                handleChange={this.handleChange}
                required={true} />

              <InputField
                name="password"
                size={customSize} idField="password"
                type="password"
                value={this.state.password}
                handleChange={this.handleChange}
                required={true} />

              <div className={`col ${customSize}`}>
                <button className="btn waves-effect waves-light" type="submit" disabled={!this.state.validForm}>Login
                              <i className="material-icons right">send</i>
                </button>
              </div>
            </div>
          </form>
        </div>
        <div className="row s12 m12 l12 xl12">
          <div className="col s3 l3 xl3">
          </div>
          <div className="center">
            <a className="text-teal" href="/forgot-password/teacher" >Esqueci minha senha</a>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return {
    user,
    users
  };
}

const connectedRouterPage = connect(mapStateToProps)(LoginTeacher);
export { connectedRouterPage as LoginTeacher };