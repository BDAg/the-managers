import React from 'react';
import { connect } from 'react-redux';
import { ListPost, NewPost } from '../Components';
import { Col } from 'react-materialize';
import { postServices } from '../_services';
import ReactTooltip from 'react-tooltip';
import { apiUrl } from '../_helpers/api-url';

class ProfessorPerfil extends React.Component {
    constructor(props) {
        super(props);
        this.photoUrl = apiUrl+"/photos/"
        this.user = this.props.userData || JSON.parse(localStorage.getItem("professor"));
        let posts = localStorage.getItem("posts") ? JSON.parse(localStorage.getItem("posts")) : false
        this.state = {
            posts: posts,
            postsFiltered: []
        }
        this.getPosts = this.getPosts.bind(this);
        this.reloadThis = this.reloadThis.bind(this);
    }

    async getPosts() {
        if (!this.state.posts) {
            await postServices.getAll();
        } else {
            clearInterval(this.getPostsInterval);

            const { posts } = this.state;
            new Promise((resolve) => {

                let data = posts.filter((value) => {
                    if (value.Professor) {
                        if (value.Professor.id === this.user.data.id) {
                            return value;
                        }
                    }
                    return null;
                });
                resolve(data);

            }).then((value) => {
                this.setState({
                    postsFiltered: value
                });
            });
        }
    }
    componentDidMount() {
        this.getPostsInterval = setInterval(this.getPosts, 500);
    }

    async reloadThis() {
        await postServices.getAll().then(() => {
            this.setState({ posts: localStorage.getItem("posts") }, window.location.reload())
        });
    }

    render() {
        let user = this.user;
        let currentLoggedUser = localStorage.getItem("professor");
        const { posts, postsFiltered } = this.state;


        return (
            <div className="row">
                <div>
                    <br />
                    <div className="col m12 s12 l4 xl4 left">
                        <div className="row">
                            <div className="col s12 m12 l12 xl12">
                                <div className="card" >
                                    <div className="card-image">
                                        {user.data.photo && <img data-tip="Professor" alt="Foto de perfil" style={{ border: "2px gold solid", boxShadow: "0 0 10px black" }} src={`${this.photoUrl + user.data.photo.path}`} />}
                                        <ReactTooltip />

                                    </div>
                                    <div className="card-content">

                                        <h5>{user.data.name}</h5>
                                        <br />
                                        {user.data.city && <span>{user.data.city.name} - {user.data.city.State.name}<br /></span>}
                                        <span>{user.data.email}</span>
                                        <br />
                                        {user.data.phone && <span>{user.data.phone} <br /></span>}
                                        <span>{user.data.semester}</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col m12 s12 l8 xl8 right">
                    <Col s={12} m={12} l={12} xl={12}>
                        {currentLoggedUser && this.user.data.id === JSON.parse(currentLoggedUser).data.id && <NewPost reloadThis={this.reloadThis} />}
                    </Col>
                    {/* <Tabs className="tab-demo z-depth-1" > */}
                    {/* <Tab active={window.location.hash === "#posts"} title="Publicações"> */}
                    <Col s={12} m={12} l={12} xl={12}>
                        <br />
                        {posts && postsFiltered.length > 0 ? Object.keys(postsFiltered).map((key) => <ListPost postData={postsFiltered[key]} key={key} />) : <span>O professor ainda não possui publicações</span>}
                    </Col>
                    {/* </Tab> */}
                    {/* </Tabs> */}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedRouterPage = connect(mapStateToProps)(ProfessorPerfil);
export { connectedRouterPage as ProfessorPerfil };