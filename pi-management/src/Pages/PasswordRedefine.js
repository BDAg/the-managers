import React from 'react';
// import M from 'materialize-css';
import { connect } from 'react-redux';
import { studentServices } from '../_services';
import {
    InputField
} from '../Components';
import { toast } from '../_helpers';

// import { userActions } from '../_actions';

class PasswordRedefine extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            email: localStorage.getItem("emailForgot") || "",
            password: "",
            passwordConfirm: "",
            code: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        let data = {};
        data[event.target.name] = event.target.value;

        this.setState(data, () => {
            let { email } = this.state;

            let isValid = email.length > 5;

            this.setState({
                isValid: isValid
            });
        });
    }

    handleSubmit(event){
        event.preventDefault();
        this.setState({
            isValid: false
        });
        let { email, password, passwordConfirm, code } = this.state;

        if(password === passwordConfirm){
            studentServices.changePasswordWithCode(email, code, password, toast);
        } else {
            toast("Senhas não batem!", "red");
        }

    }

    render() {
        let customSize = 's9 m9 l9 xl9'
        return (
            <div>
                <br />
                <div className="center">
                    <span className="flow-text">Esqueci minha senha</span>
                </div>
                <div className="row s12 m12 l12 xl12">
                    <div className="col s3 m3 l3 xl3">

                    </div>
                    <form className="col s9 m9 l9 xl9" onSubmit={this.handleSubmit}>
                        <div className="row">
                            <InputField
                                name="Email"
                                size={customSize} idField="email"
                                type="text"
                                value={this.state.email}
                                handleChange={this.handleChange}
                                required={true} />

                            <InputField
                                name="Digite Código de Confirmação"
                                size={customSize}
                                idField="code" type="text"
                                value={this.state.code}
                                handleChange={this.handleChange}
                                required={true} />

                            <InputField
                                name="Nova senha"
                                size={customSize}
                                idField="password" type="password"
                                value={this.state.password}
                                handleChange={this.handleChange}
                                required={true} />
                            
                            <InputField
                                name="Confirmar nova senha"
                                size={customSize}
                                idField="passwordConfirm" type="password"
                                value={this.state.passwordConfirm}
                                handleChange={this.handleChange}
                                required={true} />


                            <div className={`col ${customSize}`}>
                                <button className="btn waves-effect waves-light" type="submit" disabled={!this.state.isValid}>Alterar senha
                    <i className="material-icons right">{this.state.anyRequest ? "cached" : "send"}</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {};
}
const connectedRouterPage = connect(mapStateToProps)(PasswordRedefine);
export { connectedRouterPage as PasswordRedefine };