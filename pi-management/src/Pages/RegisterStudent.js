import React, { Component } from 'react';
import { userActions } from '../_actions';
import { connect } from 'react-redux';
import { utilServices } from '../_services';
import M from 'materialize-css';
import './index.css';

import {
  InputField,
  SelectSemester
} from '../Components';
import { toast } from '../_helpers';

class RegisterStudent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            birthday: '',
            semester: '1',
            phone: '',
            email: '',
            password: '',
            CityId: 0,
            StateId: 0,
            validForm: false,
            loading: false,
            cities: []
        }
    
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.mountChips = this.mountChips.bind(this);
    }

    componentDidMount() {
        document.addEventListener('DOMContentLoaded', () => {
            this.mountChips();
        });
    }

    handleChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;
    
        this.setState(data, () => {
            let { email, password, CityId } = this.state;

            let isValid = email.length > 5 && password.length >= 5 && CityId !== 0;

            this.setState({
                validForm: isValid
            });
        });
    }

    handleChangeState(data) {

        let date = new Date(data);
        this.setState({
            expireAt: date.toISOString()
        });
    }
    
    handleSubmit(event) {
        event.preventDefault();
        
        this.setState({
            validForm: false
        });

        let { name, birthday, semester, phone, email, password, CityId } = this.state;

        if(birthday === ''){
            birthday = null;
        }
        
        let data = {
            name: name,
            birthday: birthday,
            semester: semester,
            phone: phone,
            email: email,
            password: password,
            CityId: CityId
        };

        this.props.dispatch(userActions.register(data, toast));
    }

    changeValue = (value) => {
        this.setState({
            CityId: value
        })
    }

    async mountChips() {
        let citiesOption = { data: {} }
        let citiesInfo = {}
        await utilServices.citiesByState(26).then(value => {
            JSON.parse(value).map(function (city) {
                citiesOption.data[city["name"] + ", São Paulo, Brasil"] = null;
                citiesInfo[city["name"] + ", São Paulo, Brasil"] = city["id"];
                return null;
            });
        });

        var elems = document.querySelectorAll('.chips');
        this.chips = M.Chips.init(elems, {
            autocompleteOptions: citiesOption,
            limit: 1,
            onChipAdd: (el, chip) => {
                let label = chip.textContent.replace("close", "");
                if (citiesInfo[label]) {
                    this.setState({
                        CityId: citiesInfo[label]
                    });
                }
                else {
                    chip.remove();
                    M.toast({
                        html: `
                                        <span>Cidade não encotrada</span>
                                        `
                        , displayLength: 4000
                    })
                }
            },
            onChipDelete: () => {
                this.setState({
                    CityId: 0
                });
            }
        });
    }

    render() {
        let customSize = 's9 m9 l9 xl9'
        return (
            <div>
                <br />
                <div className="center">
                    <span className="flow-text"> Criar Conta</span>
                </div>

                <div className="row s12 m12 l12 xl12">
                    <div className="col s3 l3 xl3">

                    </div>

                    <form className="col s9 m9 l9 xl9" onSubmit={this.handleSubmit}>
                        <div className="row">
                            <InputField
                                name="Nome"
                                size={customSize} idField="name"
                                type="text"
                                value={this.state.name}
                                handleChange={this.handleChange}
                                required={true} />

                            <InputField
                                name="E-mail"
                                type="text"
                                idField="email"
                                size={customSize}
                                value={this.state.email}
                                handleChange={this.handleChange}
                                required={true}
                            />
                            
                            <InputField
                                name="Password"
                                type="password"
                                idField="password"
                                size={customSize}
                                value={this.state.password}
                                handleChange={this.handleChange}
                                required={true}
                            />

                            <SelectSemester 
                                handleChange={this.handleChange}
                                size={customSize}
                            />

                            <InputField
                                name="Celular"
                                type="text"
                                idField="phone"
                                size={customSize}
                                value={this.state.phone}
                                handleChange={this.handleChange}
                                required={true}
                            />
                            
                            <div className={`input-field col s9 m9 l9 xl9`}>
                                <div id="cidade" className="chips chips-autocomplete s12 m12 l12 xl12"></div>
                                {this.state.CityId === 0 && <label htmlFor="cidade">Cidade</label>}
                            </div>

                            <div className={`col button-register-student ${customSize}`}>
                                <br/>
                                <button className="btn waves-effect waves-light" type="submit" disabled={!this.state.validForm}>Criar
                                    <i className="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}
  
const connectedRouterPage = connect(mapStateToProps)(RegisterStudent);
export { connectedRouterPage as RegisterStudent };