import React from 'react';
import { Row, CardPanel, Col, Icon } from 'react-materialize';
import { Link } from 'react-router-dom';

class TeacherArea extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
        this.actions = [
            { name: "Definir ou editar cronograma", link: "/professor-area/cronograma" },
            { name: "Gerar relatório", link: "/under-construction" },
            { name: "Entregas de alunos", link: "/under-construction" }
            // { name: "", link: "" },
            // { name: "", link: "" },
            // { name: "", link: "" },
        ]
    }

    render() {
        return (
            <Row className="center">
                <h4>Área do professor</h4>

                {
                    Object.keys(this.actions).map(key => {
                        return (
                            <Col key={key} s={12} m={6} l={6} xl={6}>
                                <Link to={`${this.actions[key].link}`}>
                                    <CardPanel s={12} m={12} l={12} xl={12} className="teal valign-wrapper lighten-2" style={{ height: "14vh" }}>
                                        <span className="flow-text white-text">{this.actions[key].name}</span>
                                        <Icon className="white-text right">arrow_forward</Icon>
                                    </CardPanel>
                                </Link>
                            </Col>
                        )
                    })
                }
            </Row>
        )
    }
}

export default TeacherArea;