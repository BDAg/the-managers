import React from 'react';
import { projectActions } from '../_actions';
import { studentServices, projectServices, postServices } from '../_services';
import { connect } from 'react-redux';
import M from 'materialize-css';
import axios from 'axios';
import { toast } from '../_helpers';
import { isMobile } from 'react-device-detect';
import {
    MemberProject,
    TechnologyProject,
    ProjectsTabs,
    SearchUsers,
    NewPost,
    ListPost
} from '../Components';
import { Divider, Col } from 'react-materialize';
import { apiUrl } from '../_helpers/api-url';

class ViewProject extends React.Component {
    constructor(props) {
        super(props);
        const { match } = this.props;
        this.project = match.params.url;

        this.state = {
            projectData: null,
            gotData: false,
            solicitacions: null,
            invites: null,
            user: JSON.parse(localStorage.getItem("user")),
            authorizedUser: false,
            idProject: null,
            posts: localStorage.getItem("posts") ? JSON.parse(localStorage.getItem("posts")) : false,
            isOnProject: false
        }
        this.getData = this.getData.bind(this);
        this.reloadThis = this.reloadThis.bind(this);
    }

    componentDidMount() {
        this.intervalData = setInterval(
            () => this.getData(), 500
        )
    }
    async reloadThis() {
        await postServices.getAll().then(() => {
            this.setState({ posts: JSON.parse(localStorage.getItem("posts")) }, window.location.reload())
        })
    }
    async getData() {
        if (!this.state.posts) {
            await postServices.getAll().then(() => {
                this.setState({
                    posts: JSON.parse(localStorage.getItem("posts"))
                });
            }).catch(err => {
            });
        } else {

            const { posts } = this.state;
            new Promise((resolve) => {

                let data = posts.filter((value) => {
                    if (value.Project) {
                        if (value.Project.url === this.project) {
                            return value;
                        }
                    }
                    return null;
                });
                resolve(data);

            }).then((value) => {
                this.setState({
                    posts: value
                });
            });
        }

        if (!this.state.projectData) {
            await this.props.dispatch(projectActions.viewPerfil(this.project));
            this.setState({
                projectData: localStorage.getItem("projectsPerfil") ? JSON.parse(localStorage.getItem("projectsPerfil"))[this.project] : null
            });
        } else {
            this.setState({
                gotData: true,
                idProject: this.state.projectData["id"],

            });
            let user = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : false

            let idProject = this.state.idProject;

            if (user && idProject) {
                let authorizedUser = false;
                let userProjects = user["data"]["projects"];
                let userId = user.data.id
                let job;
                let students = this.state.projectData.Students;
                for (let key in students) {
                    if (students[key].id === userId) {
                        job = students[key].Members.job;
                        break
                    }
                }

                for (let key in userProjects) {
                    if (userProjects[key]["id"] === idProject) {
                        this.setState({
                            isOnProject: true
                        });
                        if (authorize(job)) {
                            authorizedUser = true;
                        }
                        break
                    }
                }


                this.setState({
                    authorizedUser: authorizedUser
                });
                if (authorizedUser) {
                    studentServices.listUsersWithoutProjects();
                    let technologiesRequest = await axios({ method: "GET", url: apiUrl+"/technology/list" });
                    let technologies = technologiesRequest.data.response.data;
                    let technologiesInfos = {}
                    let chipsOption = { data: {} }

                    var el = document.querySelectorAll('.tabs')
                    M.Tabs.init(el);

                    Object.keys(technologies).forEach(key => {
                        technologiesInfos[technologies[key]["name"]] = technologies[key]
                        chipsOption["data"][technologies[key]["name"]] = null;
                    });

                    var elems = document.querySelectorAll('.chips');
                    M.Chips.init(elems, {
                        autocompleteOptions: chipsOption,
                        placeholder: "Adicionar",
                        onChipAdd: async (el, chip) => {
                            let tech = chip.textContent.replace("close", "");
                            if (technologiesInfos[tech]) {
                                await projectServices.addTechnology(idProject, technologiesInfos[tech]["id"], technologiesInfos[tech]["name"], toast);
                                this.setState({
                                    projectData: null
                                });
                                this.intervalData = setInterval(
                                    () => this.getData(), 500
                                )
                            }
                            else {
                                chip.remove();
                                M.toast({
                                    html: `
                                        <span>Tecnologia não encotrada</span>
                                        `
                                    , displayLength: 5000
                                })
                            }
                        },
                        onChipDelete: (el, chip) => {
                            let tech = chip.textContent.replace("close", "");
                            if (technologiesInfos[tech])
                                projectServices.removeTechnology(idProject, technologiesInfos[tech]["id"]);
                        }
                    });

                    let invites = await axios({ method: "POST", url: apiUrl+"/project/list-invite/1", data: { token: user["token"], idProject: idProject } });
                    let solicitacions = await axios({ method: "POST", url: apiUrl+"/project/list-invite/0", data: { token: user["token"], idProject: idProject } });

                    if (invites && solicitacions) {
                        this.setState({
                            invites: invites,
                            solicitacions: solicitacions
                        });
                    }

                    var modals = document.querySelectorAll('.modal');
                    M.Modal.init(modals);
                    clearInterval(this.intervalData);
                }
            }
        }
    }

    render() {
        const { projectData, gotData, authorizedUser, posts, isOnProject } = this.state;

        if (gotData && projectData) {
            let members = projectData.Students;
            let technologies = projectData.Technologies;

            return (
                <div>
                    <div className="row center s12 m12 l12 xl12">
                        <div>
                            <div className="col s10 m10 l10 xl10">
                                <h3 className="flow-text">{projectData.name}</h3>
                            </div>
                            <div className="col right s2 m2 l2 xl2">
                                <a href={`https://www.${projectData.gitlab.split("www.").join("").split("https://").join("")}`} target="_blank" rel="noopener noreferrer">
                                    <img alt="GitLab do projeto" className="img-gitlab circle responsive-img" src={'https://static.concrete.com.br/uploads/2018/02/logo.png'} />
                                </a>
                            </div>
                        </div>
                        <div className="col s10 m10 l10 xl10">
                            <h6>{projectData.description}</h6>
                            <br />
                        </div>
                        {authorizedUser && <div className="row">
                            <div className="col s12 m12 l6 xl6" style={{ marginBottom: "5px" }}>
                                <a href={`/projects/${this.project}/edit`} className="waves-effect waves-light btn">Editar projeto</a>
                            </div>

                            <div className="col s12 m12 l6 xl6">
                                <a className="waves-effect waves-light btn modal-trigger" href="#searchUsers">Convidar Membros</a>
                                <SearchUsers idProject={this.state.projectData["id"]} />
                            </div>
                        </div>}
                    </div>

                    <div className="row s12 m12 l12 xl12">

                        <div className={`col s12 m6 l5 xl5 center`}>

                            {authorizedUser && <ProjectsTabs
                                solicitations={this.state.solicitacions}
                                invites={this.state.invites}
                            />}

                            <span><b>Membros do projeto</b></span>
                            {
                                members.length > 0 ?
                                    <ul className="collapsible">
                                        {members.map((member => {
                                            return <MemberProject authorizedUser={authorizedUser} projectName={this.project} key={member.email} memberData={member} />
                                        }))}
                                    </ul>
                                    :
                                    <div>
                                        <span>Esse projeto ainda não possui membros</span>
                                    </div>
                            }

                            <br />
                            <div className="divider"></div>
                            <span><b>Tecnologias do Projeto</b></span>
                            <br />
                            {
                                technologies.length > 0 ?
                                    technologies.map((technology) => {
                                        return <TechnologyProject key={technology.name} technology={technology} auth={authorizedUser} />
                                    })
                                    :
                                    <div>
                                        <span>Esse projeto ainda não possui tecnologias</span>
                                    </div>
                            }

                            {
                                authorizedUser &&
                                <div className="col s12 l12 xl12 m12 left-align" style={{ marginTop: "10px" }}>
                                    <div className="chips chips-autocomplete"></div>
                                </div>
                            }
                        </div>
                        <div className={`col ${isMobile ? "right" : "left"} s12 m6 l7 xl7 center`}>
                            {isMobile && <div><Divider /><br /> </div>}
                            {isOnProject && <span><b>Publicar no projeto</b></span>}
                            {isOnProject && <NewPost reloadThis={this.reloadThis} project={projectData} />}
                            <span><b>Publicações do projeto</b></span>
                            <br />
                            <br />
                            <Col s={12} m={12} l={12} xl={12}>
                                {posts && posts.length > 0 ? Object.keys(posts).map((key) => <ListPost postData={posts[key]} key={key} />) : <span>Esse projeto não possui publicações</span>}
                            </Col>
                        </div>
                    </div>
                </div>
            )
        }

        else
            return (
                <div className="center">
                    <br />
                    <div className="progress">
                        <div className="indeterminate"></div>
                    </div>
                </div>
            )
    }
}

function authorize(job) {
    return job === 2 || job === 3
}

function mapStateToProps(state) {
    const { projectsPerfil, projects } = state;
    return (
        projectsPerfil,
        projects
    );
}
const connectedViewProject = connect(mapStateToProps)(ViewProject);
export { connectedViewProject as ViewProject }