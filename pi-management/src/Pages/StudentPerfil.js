import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { toast } from '../_helpers';
import { UserPerfil} from '.';

class StudentPerfil extends React.Component {
    constructor(props) {
        super(props);
        this.student = this.props.match.params.studentId;
        this.state = {
            studentData: localStorage.getItem("users") ? JSON.parse(localStorage.getItem("users"))[this.student] : null
        }
    }

    getData() {
        this.setState({
            studentData: localStorage.getItem("users") ? JSON.parse(localStorage.getItem("users"))[this.student] : null
        });
    }
    async componentDidMount() {
        await this.props.dispatch(userActions.viewStudentPerfil(this.student, toast));
        this.intervalGetData = setInterval(() => {
            this.getData()
        }, 500);
    }

    render() {
        const { studentData } = this.state;

        if (studentData) {
            clearInterval(this.intervalGetData);

            return (
                <UserPerfil userData={studentData} />
            );
        } else {
            return (
                <div className="center">
                    <br />
                    <div className="progress">
                        <div className="indeterminate"></div>
                    </div>
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedStudentPerfil = connect(mapStateToProps)(StudentPerfil);

export { connectedStudentPerfil as StudentPerfil }