import React from 'react';

export default class LoginSwitch extends React.Component {
    render() {
        return (
            <div className="col center s12 m12 l12 xl12">
                <h5>Logar-se como</h5>
                <div className="col s12 m12 l12 xl12 ">

                    <div className="row">
                        <div className="col s0 m0 l2 xl2">

                        </div>
                        <div className="col s12 m12 l8 xl8">
                            <a href="/login/aluno">

                                <div className="card-panel teal lighten-2">
                                    <span className="white-text">
                                        <b>Aluno</b>
                                    </span>
                                    <i className="material-icons right white-text">
                                        arrow_forward
                                    </i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div className="col s12 m12 l12 xl12 ">

                    <div className="row">
                        <div className="col s0 m0 l2 xl2">

                        </div>
                        <div className="col s12 m12 l8 xl8">
                            <a href="/login/professor">

                                <div className="card-panel teal lighten-2">
                                    <span className="white-text">
                                        <b>Professor</b>
                                    </span>
                                    <i className="material-icons right white-text">
                                        arrow_forward
                                    </i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div >

        )
    }
}