import { projectConstants } from '../_constants';

export function projectsPerfil(state = {}, action) {

    switch (action.type) {
        case projectConstants.GETONE_REQUEST:
            return {
                loading: true
            };
        case projectConstants.GETONE_SUCCESS:
            let nameUrl = action.projects.url || "unknown";
            let data = {}
            data[nameUrl] = action.projects
            return {
                items: data
            };
        case projectConstants.GETONE_FAILURE:
            return {
                error: action.error
            };
        default:
            return state
    }
}