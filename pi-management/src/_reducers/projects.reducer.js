import { projectConstants } from '../_constants';

export function projects(state = {}, action) {
  switch (action.type) {

    case projectConstants.CREATE_REQUEST:
      return {
        creating: true
      };
    case projectConstants.CREATE_SUCCESS:
      return {};
      
    case projectConstants.CREATE_FAILURE:
      return {error: action.error};
    case projectConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case projectConstants.GETALL_SUCCESS:
      return {
        items: action.projects
      };
    case projectConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case projectConstants.EDIT_REQUEST:
      return {

      }
    case projectConstants.EDIT_SUCCESS:
      return {

      }
    case projectConstants.EDIT_FAILURE:
      return {

      }

    default:
      return state
  }
}