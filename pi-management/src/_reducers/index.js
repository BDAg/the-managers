import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { projects } from './projects.reducer';
import { projectsPerfil } from './project-perfil.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  projects,
  projectsPerfil
});

export default rootReducer;