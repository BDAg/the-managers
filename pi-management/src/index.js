import React from 'react';
import './index.css';
import { App } from './App/index';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { store } from './_helpers';
import { render } from 'react-dom';
import 'materialize-css/dist/css/materialize.min.css';

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
