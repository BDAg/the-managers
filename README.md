# The Managers

## Tecnologias Utilizadas

- [ReactJS](https://pt-br.reactjs.org)
- [Express](https://expressjs.com/pt-br/)
- [MySQL](https://www.mysql.com)
- [Sequelize](https://sequelize.org)
- [MaterializeJS](https://materializecss.com)

### Para conhecer a equipe [clique aqui.](https://gitlab.com/BDAg/the-managers/wikis/team)